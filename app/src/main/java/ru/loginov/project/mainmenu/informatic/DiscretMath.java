package ru.loginov.project.mainmenu.informatic;

import android.app.Activity;
import android.graphics.Color;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import ru.loginov.project.mainmenu.IListMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.InputProcess;
import ru.loginov.project.otherclass.MyToast;
import ru.loginov.project.otherclass.Stoh;
import ru.loginov.project.preference.PreferenceValue;

/**
 * Created by Loginov Ilya on 13.03.2018.
 */

public class DiscretMath extends IListMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Бинормализованная стохастичность", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditText editText = new EditText(activity);
                        editText.setHint("Целое число");
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                        final TextView tv = new TextView(activity);
                        tv.setTextSize(PreferenceValue.textsizeformul);
                        tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                        InputProcess process = new InputProcess(new EditText[]{editText}, tv, new InputProcess.OnClear() {
                            @Override
                            public void onClear(View view) {
                                tv.setText("Введите целое число");
                                editText.setText("");
                            }
                        }, new InputProcess.OnExecute() {
                            @Override
                            public void onExecute(String... data) {
                                try{
                                    int n = Integer.valueOf(data[0]);
                                    final double ret = Stoh.func(n);
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText("s = " + String.valueOf(ret));
                                        }
                                    });
                                }catch (Exception e){
                                    MyToast.show("Неправильно введены данные");
                                }
                            }
                        });
                        process.reloadProcess(activity);
                    }
                })
        });
    }
}
