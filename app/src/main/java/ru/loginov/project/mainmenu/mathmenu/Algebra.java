package ru.loginov.project.mainmenu.mathmenu;

import android.app.Activity;
import android.graphics.Color;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import ru.loginov.project.R;
import ru.loginov.project.S;
import ru.loginov.project.calculator.Eq;
import ru.loginov.project.calculator.YObject;
import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.InputProcess;
import ru.loginov.project.otherclass.MyToast;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 01.11.2016.
 */
public class Algebra extends IMenu{
    @Override
    public Menu getMenu(final Activity activity) {
        return new Menu(new ButtonMenu[]{new ButtonMenu("Вычисление корней уравнения", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final YEdit editText = new YEdit(activity);
                editText.setHint("Уравнение");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{editText},tv,new InputProcess.OnClear(){
                    @Override
                    public void onClear(View view) {
                        tv.setText("");
                        editText.setText("");
                    }
                }, new InputProcess.OnExecute() {
                    @Override
                    public void onExecute(String... data) {
                        if (!data[0].equals("")){
                            YObject y = Eq.getY(data[0]);
                            String ret = "";
                            List<String> l = y.getX();
                            if (l.size() > 0) {
                                for (int i = 0; i < l.size(); i++) {
                                    ret += "Корень № " + (i + 1) + " = " + l.get(i) + "\n";
                                }
                            }else ret = "Корней нет";
                            final String finalRet = ret;
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    tv.setText(finalRet);
                                }
                            });
                        }else{
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    tv.setText("Введите выражение");
                                }
                            });

                        }
                    }
                });

                inputProcess.reloadProcess(activity);
                final RelativeLayout layout = (RelativeLayout)editText.getKeyBoard(activity, R.layout.l_keyboard);
                layout.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layout.setLayoutParams(layoutParams);
                layout.setGravity(Gravity.BOTTOM);
                InitView.add(layout);
                editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if (b) layout.setVisibility(View.VISIBLE);
                        else layout.setVisibility(View.GONE);
                    }
                });

            }
        }),
                new ButtonMenu("Нахождение делителей числа", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditText editText = new EditText(activity);
                        final TextView tv = new TextView(activity);
                        tv.setTextSize(PreferenceValue.textsizeformul);
                        tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                        editText.setHint("Целое число");
                        tv.setText("Введите число");
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                        InputProcess inputProcess = new InputProcess(new EditText[]{editText}, tv, new InputProcess.OnClear() {
                            @Override
                            public void onClear(View view) {
                                editText.setText("");
                                tv.setText("Введите число");
                            }
                        }, new InputProcess.OnExecute() {
                            @Override
                            public void onExecute(final String... data) {
                                if (!data[0].equals("")){
                                            try{
                                                long a = Long.valueOf(data[0]);
                                                List<Long> b = S.delitel(a);
                                                if (b != null && b.size() > 0){
                                                    Collections.sort(b);
                                                    final String str = S.write(tv,b);
                                                    activity.runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            tv.setText(str);
                                                        }
                                                    });
                                                }else{
                                                    activity.runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            tv.setText("У этого числа нету делителей, кроме 1 и его самого");
                                                        }
                                                    });
                                                }
                                            }catch (Exception e) {
                                                MyToast.show("Неправильно введены данные");
                                                e.printStackTrace();
                                            }
                                }
                            }
                        });
                        inputProcess.reloadProcess(activity);
                    }
                }),
                new ButtonMenu("Нахождение НОД и НОК чисел", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditText editText = new EditText(activity);
                        final TextView textView = new TextView(activity);
                        textView.setTextSize(PreferenceValue.textsizeformul);
                        textView.setTextColor(Color.parseColor(PreferenceValue.textcolorinput));
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        textView.setLayoutParams(layoutParams);
                        editText.setHint("Целые числа через запятую");
                        textView.setText("Введите числа");
                        InputProcess inputProcess = new InputProcess(new EditText[]{editText}, textView, new InputProcess.OnClear() {
                            @Override
                            public void onClear(View view) {
                                editText.setText("");
                                textView.setText("Введите числа");
                            }
                        }, new InputProcess.OnExecute() {
                            @Override
                            public void onExecute(String... data) {
                                if (!data[0].equals("")){
                                    try {
                                        Long[] longs = S.getNumbers(data[0]);
                                        long[] a = new long[longs.length];
                                        for (int i = 0;i<longs.length;i++){
                                            a[i] = longs[i];
                                        }
                                        final long gcd = S.gcd(a);
                                        final long lcm = S.lcm(a);
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                textView.setText("НОД = " + String.valueOf(gcd) + "\n" + "НОК = " +String.valueOf(lcm));
                                            }
                                        });
                                    }catch (Exception e){
                                        MyToast.show("Неправильно введены данные");
                                    }
                                }
                            }
                        });
                        inputProcess.reloadProcess(activity);
                    }
                }),
                new ButtonMenu("Формулы",new Algebra_Sub().tothis(activity))

























        });
    }
}
