package ru.loginov.project.mainmenu.mathmenu;

import android.app.Activity;
import android.graphics.Color;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import ru.loginov.project.R;
import ru.loginov.project.S;
import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.InputProcess;
import ru.loginov.project.otherclass.MyToast;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.resourse.MyResourse;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 01.11.2016.
 */
public class Trigonometria extends IMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Числовая окружность", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        InitView.initImage(activity,R.drawable.trigonometria);
                    }
                }),
                new ButtonMenu("Вычисление тригонометрических функций", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        Menu m = new Menu(new ButtonMenu[]{
                                new ButtonMenu("Вычисление значений через угол", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        final EditText editText = new EditText(activity);
                                        editText.setHint("Угол в градусах");
                                        editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                                        final TextView tv = new TextView(activity);
                                        tv.setTextSize(PreferenceValue.textsizeformul);
                                        tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                                        InputProcess inputProcess = new InputProcess(new EditText[]{editText}, tv, new InputProcess.OnClear() {
                                            @Override
                                            public void onClear(View view) {
                                                editText.setText("");
                                                tv.setText("");
                                            }
                                        }, new InputProcess.OnExecute() {
                                            @Override
                                            public void onExecute(String... data) {
                                                if (data.length > 0){
                                                    if(!data[0].equals("")){
                                                        try {
                                                            Double d = Double.valueOf(data[0]);
                                                            String ret = "Угол " + d + "\n";
                                                            d = Math.toRadians(d);
                                                            ret += "sin = " + Math.sin(d) + "\n";
                                                            ret += "cos = " + Math.cos(d) + "\n";
                                                            ret += "tan = " + Math.tan(d) + "\n";
                                                            ret += "ctan = " + (1/(Math.tan(d))) + "\n";
                                                            final String finalRet = ret;
                                                            activity.runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    tv.setText(finalRet);
                                                                }
                                                            });
                                                        }catch (Exception e){
                                                            MyToast.show("Неправильно указаны данные");
                                                        }
                                                    }else{
                                                        MyToast.show("Укажите угол");
                                                    }
                                                }
                                            }
                                        });
                                        inputProcess.reloadProcess(activity);
                                    }
                                }),
                                new ButtonMenu("Вычисление значений через известные значения (WIP)", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        final EditText editText = new EditText(activity);
                                        editText.setHint("Выражение");
                                        final TextView textView = new TextView(activity);
                                        textView.setTextSize(PreferenceValue.textsizeformul);
                                        textView.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                                        InputProcess inputProcess = new InputProcess(new EditText[]{editText}, textView, new InputProcess.OnClear() {
                                            @Override
                                            public void onClear(View view) {
                                                editText.setText("");
                                                textView.setText("");
                                            }
                                        }, new InputProcess.OnExecute() {
                                            @Override
                                            public void onExecute(String... data) {
                                                try {
                                                    final String str= S.getsctcin(editText.getText().toString());
                                                    activity.runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            textView.setText(str);
                                                        }
                                                    });
                                                }catch (Exception e){
                                                    activity.runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            textView.setText("Неправильно введено выражение");
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                        inputProcess.reloadProcess(activity);
                                    }
                                })
                        });
                        InitView.initMenu(activity,m,InitView.getNowMenuItem());
                    }
                }),
                new ButtonMenu("Уравнения", S.getClick(activity,true,"Уравнения",MyResourse.strar32,MyResourse.strar33,null)),
                new ButtonMenu("Формулы",S.getClick(activity,true,"Формулы",MyResourse.strar30,MyResourse.strar31,null))
        });
    }
}
