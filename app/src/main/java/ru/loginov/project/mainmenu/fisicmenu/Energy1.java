package ru.loginov.project.mainmenu.fisicmenu;

import android.app.Activity;

import ru.loginov.project.mainmenu.IListMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;

/**
 * Created by user on 06.11.2016.
 */
public class Energy1 extends IListMenu {
    @Override
    public Menu getMenu(Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Электростатика",new Energy2().tothis(activity)),
                new ButtonMenu("Конденсаторы",new Energy3().tothis(activity)),
                new ButtonMenu("Законы постоянного тока",new Energy4().tothis(activity)),
                new ButtonMenu("Электрический ток в различных средах",new Energy5().tothis(activity))
        });
    }
}
