package ru.loginov.project.mainmenu.chemistry;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.R;
import ru.loginov.project.S;
import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.resourse.MyResourse;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 12.11.2016.
 */
public class ChemistryMain extends IMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Таблица Менделеева", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        InitView.initImage(activity, R.drawable.tablmend);
                    }
                }),
                new ButtonMenu("Таблица растворимости", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        InitView.createFormulView(activity,MyResourse.str3,InitView.getNowMenuItem(),null);
                    }
                }),
                new ButtonMenu("Формулы (WIP)", S.getClick(activity,true,"Формулы", MyResourse.strar38,MyResourse.strar39,MyResourse.strar40))




        });
    }

    @Override
    public OnClickMenu tothis(final Activity activity){
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                NavigationView navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
                InitView.initMenu(activity,getMenu(activity),navigationView.getMenu().getItem(0).getSubMenu().getItem(5));
            }
        };
    };
}
