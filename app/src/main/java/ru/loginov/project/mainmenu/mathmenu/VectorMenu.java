package ru.loginov.project.mainmenu.mathmenu;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import ru.loginov.project.S;
import ru.loginov.project.calculator.Vector;
import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.InputProcess;
import ru.loginov.project.preference.PreferenceValue;

/**
 * Created by Loginov Ilya on 13.10.2017.
 */

public class VectorMenu  extends IMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Скалярное произведение векторов", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final YEdit a = new YEdit(activity);
                        final YEdit b = new YEdit(activity);
                        a.setHint("Вектор №1");
                        b.setHint("Вектор №2");
                        final TextView tv = new TextView(activity);
                        tv.setTextSize(PreferenceValue.textsizeformul);
                        tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                        InputProcess inputProcess = new InputProcess(new EditText[]{a, b}, tv, new InputProcess.OnClear() {
                            @Override
                            public void onClear(View view) {
                                a.setText("");
                                b.setText("");
                                tv.setText("");
                            }
                        },
                                new InputProcess.OnExecute() {
                                    @Override
                                    public void onExecute(String... data) {
                                        if (!data[0].equals("") && !data[1].equals("")) {
                                            Vector a = Vector.fromString(data[0]);
                                            Vector b = Vector.fromString(data[1]);
                                            final double c = a.smult(b);
                                            activity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    tv.setText(String.valueOf(c));
                                                }
                                            });
                                        }else{
                                            activity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    tv.setText("Неправильно введены данные");
                                                }
                                            });
                                        }
                                    }
                                });
                        inputProcess.reloadProcess(activity);
                        S.setCustomKeyBoard2(a,activity);
                        S.setCustomKeyBoard2(b,activity);
                    }
                }), new ButtonMenu("Векторное произведение векторов", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final YEdit a = new YEdit(activity);
                final YEdit b = new YEdit(activity);
                a.setHint("Вектор №1");
                b.setHint("Вектор №2");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{a, b}, tv, new InputProcess.OnClear() {
                    @Override
                    public void onClear(View view) {
                        a.setText("");
                        b.setText("");
                        tv.setText("");
                    }
                },
                        new InputProcess.OnExecute() {
                            @Override
                            public void onExecute(String... data) {
                                if (!data[0].equals("") && !data[1].equals("")) {
                                    Vector a = Vector.fromString(data[0]);
                                    Vector b = Vector.fromString(data[1]);
                                    final Vector c = a.mult(b);
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText(c.toString());
                                        }
                                    });
                                }else{
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText("Неправильно введены данные");
                                        }
                                    });
                                }
                            }
                        });
                inputProcess.reloadProcess(activity);
                S.setCustomKeyBoard2(a,activity);
                S.setCustomKeyBoard2(b,activity);
            }
        }),
                new ButtonMenu("Смешаное произведение векторов", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final YEdit a = new YEdit(activity);
                        final YEdit b = new YEdit(activity);
                        final YEdit c = new YEdit(activity);
                        a.setHint("Вектор №1");
                        b.setHint("Вектор №2");
                        c.setHint("Вектор №3");
                        final TextView tv = new TextView(activity);
                        tv.setTextSize(PreferenceValue.textsizeformul);
                        tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                        InputProcess inputProcess = new InputProcess(new EditText[]{a, b,c}, tv, new InputProcess.OnClear() {
                            @Override
                            public void onClear(View view) {
                                a.setText("");
                                b.setText("");
                                c.setText("");
                                tv.setText("");
                            }
                        },
                                new InputProcess.OnExecute() {
                                    @Override
                                    public void onExecute(String... data) {
                                        if (!data[0].equals("") && !data[1].equals("") && !data[2].equals("")) {
                                            Vector a = Vector.fromString(data[0]);
                                            Vector b = Vector.fromString(data[1]);
                                            Vector c = Vector.fromString(data[2]);
                                            final double d = a.mult(b,c);
                                            activity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    tv.setText(String.valueOf(d));
                                                }
                                            });
                                        }else{
                                            activity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    tv.setText("Неправильно введены данные");
                                                }
                                            });
                                        }
                                    }
                                });
                        inputProcess.reloadProcess(activity);
                        S.setCustomKeyBoard2(a,activity);
                        S.setCustomKeyBoard2(b,activity);
                        S.setCustomKeyBoard2(c,activity);
                    }
                }), new ButtonMenu("Площадь параллелограма", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final YEdit a = new YEdit(activity);
                final YEdit b = new YEdit(activity);
                a.setHint("Вектор №1");
                b.setHint("Вектор №2");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{a, b}, tv, new InputProcess.OnClear() {
                    @Override
                    public void onClear(View view) {
                        a.setText("");
                        b.setText("");
                        tv.setText("");
                    }
                },
                        new InputProcess.OnExecute() {
                            @Override
                            public void onExecute(String... data) {
                                if (!data[0].equals("") && !data[1].equals("")) {
                                    Vector a = Vector.fromString(data[0]);
                                    Vector b = Vector.fromString(data[1]);
                                    final double c = Vector.squareRect(a,b);
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText(String.valueOf(c));
                                        }
                                    });
                                }else{
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText("Неправильно введены данные");
                                        }
                                    });
                                }
                            }
                        });
                inputProcess.reloadProcess(activity);
                S.setCustomKeyBoard2(a,activity);
                S.setCustomKeyBoard2(b,activity);
            }
        }), new ButtonMenu("Площадь треугольника", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final YEdit a = new YEdit(activity);
                final YEdit b = new YEdit(activity);
                a.setHint("Вектор №1");
                b.setHint("Вектор №2");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{a, b}, tv, new InputProcess.OnClear() {
                    @Override
                    public void onClear(View view) {
                        a.setText("");
                        b.setText("");
                        tv.setText("");
                    }
                },
                        new InputProcess.OnExecute() {
                            @Override
                            public void onExecute(String... data) {
                                if (!data[0].equals("") && !data[1].equals("")) {
                                    Vector a = Vector.fromString(data[0]);
                                    Vector b = Vector.fromString(data[1]);
                                    final double c = Vector.squareTriangle(a,b);
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText(String.valueOf(c));
                                        }
                                    });
                                }else{
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText("Неправильно введены данные");
                                        }
                                    });
                                }
                            }
                        });
                inputProcess.reloadProcess(activity);
                S.setCustomKeyBoard2(a,activity);
                S.setCustomKeyBoard2(b,activity);
            }
        }), new ButtonMenu("Объём параллелепипеда", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final YEdit a = new YEdit(activity);
                final YEdit b = new YEdit(activity);
                final YEdit c = new YEdit(activity);
                a.setHint("Вектор №1");
                b.setHint("Вектор №2");
                c.setHint("Вектор №3");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{a, b,c}, tv, new InputProcess.OnClear() {
                    @Override
                    public void onClear(View view) {
                        a.setText("");
                        b.setText("");
                        c.setText("");
                        tv.setText("");
                    }
                },
                        new InputProcess.OnExecute() {
                            @Override
                            public void onExecute(String... data) {
                                if (!data[0].equals("") && !data[1].equals("") && !data[2].equals("")) {
                                    Vector a = Vector.fromString(data[0]);
                                    Vector b = Vector.fromString(data[1]);
                                    Vector c = Vector.fromString(data[2]);
                                    final double d = Vector.volumeRect(a,b,c);
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText(String.valueOf(d));
                                        }
                                    });
                                }else{
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText("Неправильно введены данные");
                                        }
                                    });
                                }
                            }
                        });
                inputProcess.reloadProcess(activity);
                S.setCustomKeyBoard2(a,activity);
                S.setCustomKeyBoard2(b,activity);
                S.setCustomKeyBoard2(c,activity);
            }
        }),new ButtonMenu("Объём пирамиды", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final YEdit a = new YEdit(activity);
                final YEdit b = new YEdit(activity);
                final YEdit c = new YEdit(activity);
                a.setHint("Вектор №1");
                b.setHint("Вектор №2");
                c.setHint("Вектор №3");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{a, b,c}, tv, new InputProcess.OnClear() {
                    @Override
                    public void onClear(View view) {
                        a.setText("");
                        b.setText("");
                        c.setText("");
                        tv.setText("");
                    }
                },
                        new InputProcess.OnExecute() {
                            @Override
                            public void onExecute(String... data) {
                                if (!data[0].equals("") && !data[1].equals("") && !data[2].equals("")) {
                                    Vector a = Vector.fromString(data[0]);
                                    Vector b = Vector.fromString(data[1]);
                                    Vector c = Vector.fromString(data[2]);
                                    final double d = Vector.volumePrism(a,b,c);
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText(String.valueOf(d));
                                        }
                                    });
                                }else{
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText("Неправильно введены данные");
                                        }
                                    });
                                }
                            }
                        });
                inputProcess.reloadProcess(activity);
                S.setCustomKeyBoard2(a,activity);
                S.setCustomKeyBoard2(b,activity);
                S.setCustomKeyBoard2(c,activity);
            }
        })
        });
    }
}
