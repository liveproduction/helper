package ru.loginov.project.mainmenu.mathmenu;

import android.app.Activity;

import ru.loginov.project.S;
import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.resourse.MyResourse;

/**
 * Created by user on 01.11.2016.
 */
public class Geometria extends IMenu {
    @Override
    public Menu getMenu(Activity activity) {
       return S.getMENU(activity, MyResourse.strar50,MyResourse.strar51,MyResourse.strar52);
    }
}
