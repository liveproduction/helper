package ru.loginov.project.mainmenu.fisicmenu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import ru.loginov.project.mainmenu.IListMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.EditDialog;
import ru.loginov.project.otherclass.ListDialog;
import ru.loginov.project.resourse.MyResourse;
import ru.loginov.project.table.TableView;
import ru.loginov.project.workspace.InitView;
import ru.loginov.project.workspace.MainView;

/**
 * Created by user on 06.11.2016.
 */
public class Konstant extends IListMenu{
    Activity activity;
    @Override
    public Menu getMenu(Activity activity) {
        this.activity = activity;
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Плотность веществ", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        new InitTable().execute(InitView.getNowMenuItem());
                    }
                }),
                new ButtonMenu("Абсолютный показатель преломления среды", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        new InitTable2().execute(InitView.getNowMenuItem());
                    }
                }),
                new ButtonMenu("Физические постоянные", InitView.createFormul(activity,MyResourse.str2,InitView.getNowMenuItem(),null))



        });
    }

    class InitTable2 extends AsyncTask<MenuItem,Void,Void>{

        @Override
        protected Void doInBackground(final MenuItem... menuItems) {
            final TableView tableView = new TableView("Вещество","",MyResourse.itemar2);
            tableView.setOnmenu( new MainView.OnInitMenu() {
                @Override
                public void onOpenMenu(final Activity ac) {
                    ListDialog d = new ListDialog("Меню",new Menu(new ButtonMenu[]{
                            new ButtonMenu("Поиск", new OnClickMenu() {
                                @Override
                                public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                    final EditDialog dialog = new EditDialog("Поиск","Поиск","Строка поиска","");
                                    dialog.setRunnable(new EditDialog.onEnded() {
                                        @Override
                                        public void run(Activity activity) {
                                            tableView.search(dialog.getOutput());
                                            tableView.reloadTable(activity,menuItems[0]);
                                        }
                                    });
                                    dialog.getDialog(ac).show();
                                }
                            })
                    }));
                    d.getDialog(ac).show();
                }
            });
            tableView.visibleTable(activity,menuItems[0]);
            return null;
        }
    }

    class InitTable extends AsyncTask<MenuItem,Void,Void> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            pd = InitView.showLoadMessage();
        }

        @Override
        protected Void doInBackground(final MenuItem... voids) {
            final TableView tableView = new TableView("Вещество", "кг/м^3", MyResourse.itemar1);
            tableView.setOnmenu( new MainView.OnInitMenu() {
                @Override
                public void onOpenMenu(final Activity ac) {
                    ListDialog d = new ListDialog("Меню",new Menu(new ButtonMenu[]{
                            new ButtonMenu("Поиск", new OnClickMenu() {
                                @Override
                                public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                    final EditDialog dialog = new EditDialog("Поиск","Поиск","Строка поиска","");
                                    dialog.setRunnable(new EditDialog.onEnded() {
                                        @Override
                                        public void run(Activity activity) {
                                            tableView.search(dialog.getOutput());
                                            tableView.reloadTable(activity,voids[0]);
                                        }
                                    });
                                    dialog.getDialog(ac).show();
                                }
                            }),
                            new ButtonMenu("Сортировка", new OnClickMenu() {
                                @Override
                                public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                    ListDialog dialog = new ListDialog("Сортировка",new Menu(new ButtonMenu[]{new ButtonMenu("Сортировка по названию (по убыванию)", new OnClickMenu() {
                                        @Override
                                        public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                            tableView.sortNameDown();
                                            tableView.reloadTable(ac,voids[0]);
                                        }
                                    }),new ButtonMenu("Сортировка по названию (по возрастанию)", new OnClickMenu() {
                                        @Override
                                        public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                            tableView.sortNameUp();
                                            tableView.reloadTable(ac,voids[0]);
                                        }
                                    }),new ButtonMenu("Сортировка по плотности (по возрастанию)", new OnClickMenu() {
                                        @Override
                                        public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                            tableView.sortValueDown();
                                            tableView.reloadTable(ac,voids[0]);
                                        }
                                    }),new ButtonMenu("Сортировка по плотности (по убыванию)", new OnClickMenu() {
                                        @Override
                                        public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                            tableView.sortValueUp();
                                            tableView.reloadTable(ac,voids[0]);
                                        }
                                    }),}));

                                    dialog.getDialog(ac).show();
                                }
                            }),
                            new ButtonMenu("Помощь", new OnClickMenu() {
                                @Override
                                public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                    AlertDialog.Builder adb = new AlertDialog.Builder(ac);
                                    adb.setTitle("Помощь");
                                    adb.setMessage("Если над название вещества стоит знак '*', значит это газ. Если над названием вещества стоит знак '^', значит это жидкость. Если над название вещества стоит знак '~', значит это сплав металов. Если нет никаких знаков, значит это чистое вещество. Плотность даётся при нормальных условиях");
                                    adb.create().show();
                                }
                            }), new ButtonMenu("Полный список", new OnClickMenu() {
                        @Override
                        public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                            tableView.setItems(MyResourse.itemar1);
                            tableView.reloadTable(ac,voids[0]);
                        }
                    })



                    }));
                    d.getDialog(ac).show();
                }
            });
            tableView.visibleTable(activity,voids[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            pd.dismiss();
            super.onPostExecute(aVoid);

        }
    }
}
