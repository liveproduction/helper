package ru.loginov.project.mainmenu.fisicmenu;

import android.app.Activity;

import ru.loginov.project.S;
import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.resourse.MyResourse;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 12.12.2016.
 */

public class Optic extends IMenu {
    @Override
    public Menu getMenu(Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Линзы", InitView.createFormul(activity, MyResourse.strar27[2],InitView.getNowMenuItem(),MyResourse.strar46[2])),
                new ButtonMenu("Формулы", S.getMENU(activity, MyResourse.strar47, MyResourse.strar48, MyResourse.strar49).tothis(activity))


        });
    }
}
