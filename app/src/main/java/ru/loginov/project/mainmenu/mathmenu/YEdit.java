package ru.loginov.project.mainmenu.mathmenu;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

import ru.loginov.project.R;

/**
 * Created by masa0 on 27.07.2017.
 */

public class YEdit extends android.support.v7.widget.AppCompatEditText {

    public YEdit(Activity activity) {
        super(activity);
    }

    @Override
    public boolean getDefaultEditable() {
        return false;
    }

    public ViewGroup getKeyBoard(Activity activity,int id){
        final YEdit edit = this;
        RelativeLayout relativeLayout = new RelativeLayout(activity);
            TableLayout layout = (TableLayout) activity.getLayoutInflater().inflate(id,null,false);
            int childCount = layout.getChildCount();
            for (int i = 0;i<childCount;i++) {
                TableRow t = (TableRow) layout.getChildAt(i);
                int child = t.getChildCount();
                for (int j = 0; j < child; j++) {
                    final View button = t.getChildAt(j);
                    if (button.getId() == R.id.buttonremove) {
                        button.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String str = edit.getText().toString();
                                if (str.length() > 0) {
                                    edit.setText(str.substring(0, str.length() - 1));
                                }
                            }
                        });
                        button.setOnLongClickListener(new OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View view) {
                                edit.setText("");
                                return true;
                            }
                        });
                    } else {
                        button.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                edit.setText(edit.getText() + button.getTag().toString());
                            }
                        });
                    }
                }
            }
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
            layout.setLayoutParams(layoutParams);
            relativeLayout.addView(layout);
            return relativeLayout;
    }
}
