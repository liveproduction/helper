package ru.loginov.project.mainmenu.fisicmenu;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.S;
import ru.loginov.project.mainmenu.IListMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.resourse.MyResourse;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 06.11.2016.
 */
public class Magnit extends IListMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        return S.getMENU(activity, MyResourse.strar24,MyResourse.strar25,MyResourse.strar26,new int[]{3},new OnClickMenu[]{new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                InitView.initText(activity,MyResourse.str1);
            }
        }});
    }
}
