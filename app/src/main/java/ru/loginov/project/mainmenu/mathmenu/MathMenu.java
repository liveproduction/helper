package ru.loginov.project.mainmenu.mathmenu;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.R;
import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 01.11.2016.
 */
public class MathMenu extends IMenu {
    @Override
    public Menu getMenu(Activity activity) {
        return new Menu(new ButtonMenu[]{new ButtonMenu("Алгебра",new Algebra().tothis(activity)),
        new ButtonMenu("Тригонометрия",new Trigonometria().tothis(activity)),
                new ButtonMenu("Планиметрия",new Geometria().tothis(activity)),
                new ButtonMenu("Стериометрия",new Ctereometria().tothis(activity)),
                new ButtonMenu("Матрицы",new MatrixMenu().tothis(activity)),
                new ButtonMenu("Вектора",new VectorMenu().tothis(activity))
        });
    }

    @Override
    public OnClickMenu tothis(final Activity activity){
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                NavigationView navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
                InitView.initMenu(activity,getMenu(activity),navigationView.getMenu().getItem(0).getSubMenu().getItem(0));
            }
        };
    };
}
