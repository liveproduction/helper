package ru.loginov.project.mainmenu.shiphr;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Vibrator;
import android.support.design.widget.NavigationView;
import android.text.ClipboardManager;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;

import ru.loginov.project.R;
import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.Enigma;
import ru.loginov.project.otherclass.InputProcess;
import ru.loginov.project.otherclass.InputProcess.OnClear;
import ru.loginov.project.otherclass.InputProcess.OnExecute;
import ru.loginov.project.otherclass.MyException;
import ru.loginov.project.otherclass.MyToast;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

public class ShiphrMain
  extends IMenu
{
  public ru.loginov.project.menuclass.Menu getMenu(final Activity paramActivity)
  {
    return new ru.loginov.project.menuclass.Menu(new ButtonMenu[] { new ButtonMenu("Зашифровать сообщение", new OnClickMenu()
    {
      public void run(final AdapterView<?> paramAnonymousAdapterView, final View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong, final ButtonMenu paramAnonymousButtonMenu)
      {
        final EditText edittext1 = new EditText(paramActivity);
        final EditText edittext2 = new EditText(paramActivity);
        edittext1.setHint("Ключ");
        edittext2.setHint("Коммутаторы");

        edittext1.setOnLongClickListener(new OnLongClickListener() {
          @Override
          public boolean onLongClick(View view) {
            if (edittext1.getText().toString().length() > 0){
              if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) paramActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Shiphr",edittext1.getText().toString());
                clipboard.setPrimaryClip(clip);
              }else{
                ClipboardManager clipboard = (ClipboardManager) paramActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setText(edittext1.getText().toString());
              }
              Vibrator vibrator = (Vibrator)paramActivity.getSystemService(Context.VIBRATOR_SERVICE);
              try {
                vibrator.vibrate(100);
              }catch (Exception e){
              }
              MyToast.show("Текст скопирован");
            }else{
              try {
                edittext1.setText(Enigma.getRandomKey());
              } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
              }
            }
            return true;
          }
        });
        edittext2.setOnLongClickListener(new OnLongClickListener() {
          @Override
          public boolean onLongClick(View view) {
            if (edittext2.getText().toString().length() > 0){
              if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) paramActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Shiphr",edittext2.getText().toString());
                clipboard.setPrimaryClip(clip);
              }else{
                ClipboardManager clipboard = (ClipboardManager) paramActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setText(edittext2.getText().toString());
              }
              Vibrator vibrator = (Vibrator)paramActivity.getSystemService(Context.VIBRATOR_SERVICE);
              try {
                vibrator.vibrate(100);
              }catch (Exception e){
              }
              MyToast.show("Текст скопирован");
            }else{
              edittext2.setText(Enigma.getRandomreflectors());
            }
            return true;
          }
        });
        final EditText localEditText = new EditText(paramActivity);
        localEditText.setHint("Строка");
        final TextView localTextView = new TextView(paramActivity);
        localTextView.setOnLongClickListener(new OnLongClickListener()
        {
          public boolean onLongClick(View paramAnonymous2View)
          {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
              android.content.ClipboardManager clipboard = (android.content.ClipboardManager) paramActivity.getSystemService(Context.CLIPBOARD_SERVICE);
              ClipData clip = ClipData.newPlainText("Shiphr",localTextView.getText().toString());
              clipboard.setPrimaryClip(clip);
            }else{
              ClipboardManager clipboard = (ClipboardManager) paramActivity.getSystemService(Context.CLIPBOARD_SERVICE);
              clipboard.setText(localTextView.getText().toString());
            }
            Vibrator vibrator = (Vibrator)paramActivity.getSystemService(Context.VIBRATOR_SERVICE);
            try {
              vibrator.vibrate(100);
            }catch (Exception e){
            }
            MyToast.show("Текст скопирован");
            return false;
          }
        });
        localTextView.setTextSize(PreferenceValue.textsizeformul);
        localTextView.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
        localTextView.setText("Введите строку,ключ и по желанию коммутаторы");
        OnClear local2 = new OnClear()
        {
          public void onClear(View paramAnonymous2View)
          {
            edittext1.setText("");
            edittext2.setText("");
            localEditText.setText("");
            localTextView.setText("Введите строку,ключ и по желанию коммутаторы");
          }
        };
        OnExecute local3 = new OnExecute()
        {
          public void onExecute(String... data) {
            try {
              final String str = Enigma.encode(data[2],data[0],data[1]);
              paramActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                  localTextView.setText(str);
                }
              });
            } catch (MyException e) {
              if (e.getID() == 1){
                MyToast.show("Ключ не может быть больше 10 символов");
              }else{
                MyToast.show("Введите ключ");
              }
            }catch (Exception e){
              MyToast.show("Неправильно введены данные");
              e.printStackTrace();
            }
          }
        };
        new InputProcess(new EditText[] { edittext1, edittext2, localEditText }, localTextView, local2, local3).reloadProcess(paramActivity);
      }
    }), new ButtonMenu("Дешифровать сообщение", new OnClickMenu()
    {
      public void run(final AdapterView<?> paramAnonymousAdapterView, final View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong, final ButtonMenu paramAnonymousButtonMenu)
      {
        final EditText edittext1 = new EditText(paramActivity);
        final EditText edittext2 = new EditText(paramActivity);
        final EditText localEditText = new EditText(paramActivity);
        edittext1.setHint("Ключ");
        edittext2.setHint("Коммутаторы");
        localEditText.setHint("Строка");
        final TextView localTextView = new TextView(paramActivity);
        localTextView.setOnLongClickListener(new OnLongClickListener()
        {
          public boolean onLongClick(View paramAnonymous2View)
          {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
              android.content.ClipboardManager clipboard = (android.content.ClipboardManager) paramActivity.getSystemService(Context.CLIPBOARD_SERVICE);
              ClipData clip = ClipData.newPlainText("Shiphr",localTextView.getText().toString());
              clipboard.setPrimaryClip(clip);
            }else{
              ClipboardManager clipboard = (ClipboardManager) paramActivity.getSystemService(Context.CLIPBOARD_SERVICE);
              clipboard.setText(localTextView.getText().toString());
            }
            Vibrator vibrator = (Vibrator)paramActivity.getSystemService(Context.VIBRATOR_SERVICE);
            try {
              vibrator.vibrate(100);
            }catch (Exception e){
            }
            MyToast.show("Текст скопирован");
            return false;
          }
        });
        localTextView.setTextSize(PreferenceValue.textsizeformul);
        localTextView.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
        localTextView.setText("Введите строку,ключ и по желанию коммутаторы");
        OnClear local2 = new OnClear()
        {
          public void onClear(View paramAnonymous2View)
          {
            edittext1.setText("");
            edittext2.setText("");
            localEditText.setText("");
            localTextView.setText("Введите строку,ключ и по желанию коммутаторы");
          }
        };
        OnExecute local3 = new OnExecute()
        {
          public void onExecute(String... data)
          {
            try {
              final String str = Enigma.decode(data[2],data[0],data[1]);
              paramActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                  localTextView.setText(str);
                }
              });
            } catch (MyException e) {
              if (e.getID() == 1){
                MyToast.show("Ключ не может быть больше 10 символов");
              }else{
                MyToast.show("Введите ключ");
              }
            }catch (Exception e){
              MyToast.show("Неправильно введены данные");
              e.printStackTrace();
            }
          }
        };
        new InputProcess(new EditText[] { edittext1, edittext2, localEditText }, localTextView, local2, local3).reloadProcess(paramActivity);
      }
    }) });
  }
  
  public OnClickMenu tothis(final Activity paramActivity)
  {
    return new OnClickMenu()
    {
      public void run(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong, ButtonMenu paramAnonymousButtonMenu)
      {
        NavigationView navigationView = (NavigationView) paramActivity.findViewById(R.id.nav_view);
        InitView.initMenu(paramActivity, ShiphrMain.this.getMenu(paramActivity), navigationView.getMenu().getItem(0).getSubMenu().getItem(6));
      }
    };
  }
}