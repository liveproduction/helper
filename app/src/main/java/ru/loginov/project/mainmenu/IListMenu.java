package ru.loginov.project.mainmenu;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.ListDialog;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 03.11.2016.
 */
public abstract class IListMenu extends IMenu{
    protected String name;

    @Override
    public abstract Menu getMenu(Activity activity);

    @Override
    public OnClickMenu tothis(final Activity activity) {
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                if (PreferenceValue.createListmenu){
                    new OnClickMenu() {
                        @Override
                        public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                            ListDialog listDialog = new ListDialog(name,getMenu(activity));
                            listDialog.getDialog(activity).show();
                        }
                    }.run(adapterView,view,i,l,buttonMenu);
                }else{
                    InitView.initMenu(activity,getMenu(activity),InitView.getNowMenuItem());
                }
            }
        };

    }
}
