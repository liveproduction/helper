package ru.loginov.project.mainmenu.english;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.NavigationView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import ru.loginov.project.R;
import ru.loginov.project.S;
import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.resourse.MyResourse;
import ru.loginov.project.workspace.InitView;
import ru.loginov.project.workspace.MainView;

/**
 * Created by user on 09.11.2016.
 */
public class EnglishMenu extends IMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Времена", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        InitView.initImage(activity,R.drawable.engtght2);
                    }
                }),
                new ButtonMenu("Неправильные глаголы", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditText editText = new EditText(activity);
                        editText.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                        editText.setHintTextColor(Color.parseColor(PreferenceValue.hintcolorinput));
                        final ListView lv = new ListView(activity);
                        final Integer[][] integer = {null};
                        class Test extends AsyncTask<String, Void, Integer[]>{
                            @Override
                            protected Integer[] doInBackground(String... strings) {
                                return find(strings[0]);
                            }

                            @Override
                            protected void onPostExecute(Integer[] integers) {
                                integer[0] = integers;
                                String[] str = new String[integers.length];
                                for (int i = 0; i < integers.length; i++){
                                    str[i] = MyResourse._strar1[0][integers[i]];
                                }
                                lv.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, str));
                                super.onPostExecute(integers);

                            }
                        }

                        editText.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                new Test().execute(charSequence.toString());
                            }

                            @Override
                            public void afterTextChanged(Editable editable) {

                            }
                        });

                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                S.hideKeyboard(activity);
                                if (integer[0] == null){
                                    InitView.initText(activity,"Первая форма: "+MyResourse._strar1[0][i]+"\nВторая форма: "+MyResourse._strar1[1][i]+
                                            "\nТретья форма: "+MyResourse._strar1[2][i]+"\nПеревод: "+MyResourse._strar1[3][i]);
                                }else if (i < integer[0].length){
                                    InitView.initText(activity,"Первая форма: "+MyResourse._strar1[0][integer[0][i]]+"\nВторая форма: "+MyResourse._strar1[1][integer[0][i]]+
                                    "\nТретья форма: "+MyResourse._strar1[2][integer[0][i]]+"\nПеревод: "+MyResourse._strar1[3][integer[0][i]]);
                                }
                            }
                        });
                        lv.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, MyResourse._strar1[0]));
                        LinearLayout layout = new LinearLayout(activity);
                        layout.setOrientation(LinearLayout.VERTICAL);
                        layout.addView(editText);
                        layout.addView(lv);
                        MainView mainView = new MainView(layout);
                        mainView.setRun(new MainView.OnInit() {
                            @Override
                            public void onInit(Activity ac, int w, int h) {
                                if (PreferenceValue.havebackfotoinput){
                                    InitView.setBackground(PreferenceValue.backfotoinput);
                                }else{
                                    InitView.setBackground(S.getDrawable(PreferenceValue.backcolorinput,PreferenceValue.gradbackinput));
                                }
                            }
                        });
                        InitView.initView(activity, mainView,InitView.getNowMenuItem());
                    }
                })


        });
    }




    public static Integer[] find(final String local){
        final int _count_thread = 4;
        final Integer[][] ret = new Integer[_count_thread][];
        Thread[] th = new Thread[_count_thread];
        for (int i = 0; i < _count_thread; i++){
            final int finalI = i;
            th[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    ret[finalI] = find(MyResourse._strar1[finalI],local);
                }
            });
            th[i].start();
        };
        for (int i = 0;i<_count_thread;i++){
            try {
                th[i].join();
            } catch (InterruptedException e) {

            }
        }
        ArrayList<Integer> _ret = new ArrayList<Integer>(0);
        for (int i = 0; i < _count_thread;i++){
            for (int j = 0; ret[i] != null && j < ret[i].length; j++){
                if (_ret.indexOf(ret[i][j]) == -1){
                    _ret.add(ret[i][j]);
                }
            }
        }
        return _ret.toArray(new Integer[_ret.size()]);
    }

    static class Pair<T, K>{
        T first;
        K second;
        public Pair(T first, K second){
            this.first = first;
            this.second = second;
        }

        public T getFirst() {
            return first;
        }

        public K getSecond() {
            return second;
        }
    }

    public static  Integer[] find(String[] str, String local){
        ArrayList<Pair<String, Integer>> ret = new ArrayList<>();
        for (int i = 0; i < str.length; i++){
            if (str[i].contains(local) || local.contains(str[i])) ret.add(new Pair<String, Integer>(str[i],i));
        }
        Pair<String, Integer>[] tmp = new Pair[ret.size()];
        tmp =  ret.toArray(tmp);
        Arrays.sort(tmp, new Comparator<Pair<String, Integer>>() {
            @Override
            public int compare(Pair<String, Integer> o1, Pair<String, Integer> o2) {
                for (int i = 0; i < o1.first.length() && i < o2.first.length(); i++) {
                    if (o1.first.charAt(i) < o2.first.charAt(i)) return -1;
                    else if (o1.first.charAt(i) > o2.first.charAt(i)) return 1;
                }
                if (o1.first.length() < o2.first.length()) return -1;
                else if (o1.first.length() > o2.first.length()) return 1;
                return 0;
            }
        });
        Integer[] _r = new Integer[tmp.length];
        for (int i = 0; i < tmp.length; i++){
            _r[i] = tmp[i].second;
        }
        return _r;
    }

    @Override
    public OnClickMenu tothis(final Activity activity){
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                NavigationView navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
                InitView.initMenu(activity,getMenu(activity),navigationView.getMenu().getItem(0).getSubMenu().getItem(4));
            }
        };
    };
}
