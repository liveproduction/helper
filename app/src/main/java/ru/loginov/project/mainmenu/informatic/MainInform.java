package ru.loginov.project.mainmenu.informatic;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.R;
import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 07.11.2016.
 */
public class MainInform extends IMenu {
    @Override
    public Menu getMenu(Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Системы счисления",new MuchNumber().tothis(activity)),
                new ButtonMenu("Дискретная математика",new DiscretMath().tothis(activity)),
                new ButtonMenu("Формулы",new Formul().tothis(activity))



        });
    }

    @Override
    public OnClickMenu tothis(final Activity activity){
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                NavigationView navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
                InitView.initMenu(activity,getMenu(activity),navigationView.getMenu().getItem(0).getSubMenu().getItem(2));
            }
        };
    };
}
