package ru.loginov.project.mainmenu.mathmenu;

import android.app.Activity;

import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;

/**
 * Created by Loginov Ilya on 12.11.2017.
 */

public class AlGeom extends IMenu {
    @Override
    public Menu getMenu(Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Матрицы (Алгем)",new MatrixMenu().tothis(activity)),
                new ButtonMenu("Вектора",new VectorMenu().tothis(activity)),
        });
    }
}
