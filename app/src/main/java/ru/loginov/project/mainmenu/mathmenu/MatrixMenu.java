package ru.loginov.project.mainmenu.mathmenu;

import android.app.Activity;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import ru.loginov.project.R;
import ru.loginov.project.calculator.Eq;
import ru.loginov.project.calculator.Matrix;
import ru.loginov.project.calculator.YObject;
import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.InputProcess;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by masa0 on 16.09.2017.
 */

public class MatrixMenu extends IMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Определитель матрицы", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                    final YEdit editText = new YEdit(activity);
                        editText.setHint("Матрица");
                        final TextView tv = new TextView(activity);
                        tv.setTextSize(PreferenceValue.textsizeformul);
                        tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                        InputProcess inputProcess = new InputProcess(new EditText[]{editText},tv,new InputProcess.OnClear(){
                            @Override
                            public void onClear(View view) {
                                tv.setText("");
                                editText.setText("");
                            }
                        }, new InputProcess.OnExecute() {
                            @Override
                            public void onExecute(String... data) {
                                if (!data[0].equals("")) {
                                    Matrix m = Matrix.fromString(data[0]);
                                    if (m == null){
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                tv.setText("Неправильно введены данные");
                                            }
                                        });
                                    }else{
                                        final double a = m.func_0();
                                            activity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    tv.setText(String.valueOf(a));
                                                }
                                            });
                                    }
                                }else{
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText("Введите данные");
                                        }
                                    });
                                }
                            }
                        });

                        inputProcess.reloadProcess(activity);
                        final RelativeLayout layout = (RelativeLayout)editText.getKeyBoard(activity, R.layout.l_keyboard_2);
                        layout.setVisibility(View.GONE);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                        layout.setLayoutParams(layoutParams);
                        layout.setGravity(Gravity.BOTTOM);
                        InitView.add(layout);
                        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean b) {
                                if (b) layout.setVisibility(View.VISIBLE);
                                else layout.setVisibility(View.GONE);
                            }
                        });
                    }
                }),new ButtonMenu("Сложение матриц", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final YEdit editText = new YEdit(activity);
                editText.setHint("Матрица #1");
                final YEdit edit = new YEdit(activity);
                edit.setHint("Матрица #2");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{editText,edit},tv,new InputProcess.OnClear(){
                    @Override
                    public void onClear(View view) {
                        tv.setText("");
                        editText.setText("");
                        edit.setText("");
                    }
                }, new InputProcess.OnExecute() {
                    @Override
                    public void onExecute(String... data) {
                        if (!data[0].equals("")) {
                            Matrix m = Matrix.fromString(data[0]);
                            Matrix m2 = Matrix.fromString(data[1]);
                            if (m == null && m2 == null){
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        tv.setText("Неправильно введены данные");
                                    }
                                });
                            }else{
                                final Matrix ret = m.add(m2);
                                if (ret != null) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText(ret.toString());
                                        }
                                    });
                                }else{
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText("Неправильно введены данные");
                                        }
                                    });
                                }
                            }
                        }else{
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    tv.setText("Введите данные");
                                }
                            });
                        }
                    }
                });

                inputProcess.reloadProcess(activity);
                final RelativeLayout layout = (RelativeLayout)editText.getKeyBoard(activity, R.layout.l_keyboard_2);
                layout.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layout.setLayoutParams(layoutParams);
                layout.setGravity(Gravity.BOTTOM);
                InitView.add(layout);
                final RelativeLayout layout2 = (RelativeLayout)edit.getKeyBoard(activity, R.layout.l_keyboard_2);
                layout2.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layout2.setLayoutParams(layoutParams);
                layout2.setGravity(Gravity.BOTTOM);
                InitView.add(layout2);
                editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if (b) layout.setVisibility(View.VISIBLE);
                        else layout.setVisibility(View.GONE);
                    }
                });
                edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if (b) layout2.setVisibility(View.VISIBLE);
                        else layout2.setVisibility(View.GONE);
                    }
                });
            }
        }),new ButtonMenu("Вычитание матриц", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final YEdit editText = new YEdit(activity);
                editText.setHint("Матрица #1");
                final YEdit edit = new YEdit(activity);
                edit.setHint("Матрица #2");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{editText,edit},tv,new InputProcess.OnClear(){
                    @Override
                    public void onClear(View view) {
                        tv.setText("");
                        editText.setText("");
                        edit.setText("");
                    }
                }, new InputProcess.OnExecute() {
                    @Override
                    public void onExecute(String... data) {
                        if (!data[0].equals("")) {
                            Matrix m = Matrix.fromString(data[0]);
                            Matrix m2 = Matrix.fromString(data[1]);
                            if (m == null && m2 == null){
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        tv.setText("Неправильно введены данные");
                                    }
                                });
                            }else{
                                final Matrix ret = m.sub(m2);
                                if (ret != null) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText(ret.toString());
                                        }
                                    });
                                }else{
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText("Неправильно введены данные");
                                        }
                                    });
                                }
                            }
                        }else{
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    tv.setText("Введите данные");
                                }
                            });
                        }
                    }
                });

                inputProcess.reloadProcess(activity);
                final RelativeLayout layout = (RelativeLayout)editText.getKeyBoard(activity, R.layout.l_keyboard_2);
                layout.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layout.setLayoutParams(layoutParams);
                layout.setGravity(Gravity.BOTTOM);
                InitView.add(layout);
                final RelativeLayout layout2 = (RelativeLayout)edit.getKeyBoard(activity, R.layout.l_keyboard_2);
                layout2.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layout2.setLayoutParams(layoutParams);
                layout2.setGravity(Gravity.BOTTOM);
                InitView.add(layout2);
                editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if (b) layout.setVisibility(View.VISIBLE);
                        else layout.setVisibility(View.GONE);
                    }
                });
                edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if (b) layout2.setVisibility(View.VISIBLE);
                        else layout2.setVisibility(View.GONE);
                    }
                });
            }
        }),new ButtonMenu("Умножение на число", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final YEdit editText = new YEdit(activity);
                editText.setHint("Матрица #1");
                final YEdit edit = new YEdit(activity);
                edit.setHint("Число");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{editText,edit},tv,new InputProcess.OnClear(){
                    @Override
                    public void onClear(View view) {
                        tv.setText("");
                        editText.setText("");
                        edit.setText("");
                    }
                }, new InputProcess.OnExecute() {
                    @Override
                    public void onExecute(String... data) {
                        if (!data[0].equals("")) {
                            Matrix m = Matrix.fromString(data[0]);
                            int a = 1;
                            try {
                                 a = Integer.valueOf(data[1]);
                            }catch (Exception e){
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        tv.setText("Неправильно введены данные");
                                    }
                                });
                                return;
                            }
                            if (m == null){
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        tv.setText("Неправильно введены данные");
                                    }
                                });
                            }else{
                                final Matrix ret = m.mult(a);
                                if (ret != null) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText(ret.toString());
                                        }
                                    });
                                }else{
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText("Неправильно введены данные");
                                        }
                                    });
                                }
                            }
                        }else{
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    tv.setText("Введите данные");
                                }
                            });
                        }
                    }
                });

                inputProcess.reloadProcess(activity);
                final RelativeLayout layout = (RelativeLayout)editText.getKeyBoard(activity, R.layout.l_keyboard_2);
                layout.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layout.setLayoutParams(layoutParams);
                layout.setGravity(Gravity.BOTTOM);
                InitView.add(layout);
                final RelativeLayout layout2 = (RelativeLayout)edit.getKeyBoard(activity, R.layout.l_keyboard_2);
                layout2.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layout2.setLayoutParams(layoutParams);
                layout2.setGravity(Gravity.BOTTOM);
                InitView.add(layout2);
                editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if (b) layout.setVisibility(View.VISIBLE);
                        else layout.setVisibility(View.GONE);
                    }
                });
                edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if (b) layout2.setVisibility(View.VISIBLE);
                        else layout2.setVisibility(View.GONE);
                    }
                });
            }
        }),new ButtonMenu("Умножение матриц", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final YEdit editText = new YEdit(activity);
                editText.setHint("Матрица #1");
                final YEdit edit = new YEdit(activity);
                edit.setHint("Матрица #2");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{editText,edit},tv,new InputProcess.OnClear(){
                    @Override
                    public void onClear(View view) {
                        tv.setText("");
                        editText.setText("");
                        edit.setText("");
                    }
                }, new InputProcess.OnExecute() {
                    @Override
                    public void onExecute(String... data) {
                        if (!data[0].equals("")) {
                            Matrix m = Matrix.fromString(data[0]);
                            Matrix m2 = Matrix.fromString(data[1]);
                            if (m == null && m2 == null){
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        tv.setText("Неправильно введены данные");
                                    }
                                });
                            }else{
                                final Matrix ret = m.mult(m2);
                                if (ret != null) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText(ret.toString());
                                        }
                                    });
                                }else{
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText("Неправильно введены данные");
                                        }
                                    });
                                }
                            }
                        }else{
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    tv.setText("Введите данные");
                                }
                            });
                        }
                    }
                });

                inputProcess.reloadProcess(activity);
                final RelativeLayout layout = (RelativeLayout)editText.getKeyBoard(activity, R.layout.l_keyboard_2);
                layout.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layout.setLayoutParams(layoutParams);
                layout.setGravity(Gravity.BOTTOM);
                InitView.add(layout);
                final RelativeLayout layout2 = (RelativeLayout)edit.getKeyBoard(activity, R.layout.l_keyboard_2);
                layout2.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layout2.setLayoutParams(layoutParams);
                layout2.setGravity(Gravity.BOTTOM);
                InitView.add(layout2);
                editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if (b) layout.setVisibility(View.VISIBLE);
                        else layout.setVisibility(View.GONE);
                    }
                });
                edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if (b) layout2.setVisibility(View.VISIBLE);
                        else layout2.setVisibility(View.GONE);
                    }
                });
            }
        }),new ButtonMenu("Транспонирование матрицы", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final YEdit editText = new YEdit(activity);
                editText.setHint("Матрица");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{editText},tv,new InputProcess.OnClear(){
                    @Override
                    public void onClear(View view) {
                        tv.setText("");
                        editText.setText("");
                    }
                }, new InputProcess.OnExecute() {
                    @Override
                    public void onExecute(String... data) {
                        if (!data[0].equals("")) {
                            Matrix m = Matrix.fromString(data[0]);
                            if (m == null){
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        tv.setText("Неправильно введены данные");
                                    }
                                });
                            }else{
                                final Matrix a = m.func_2();
                                if (a != null) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText(a.toString());
                                        }
                                    });
                                }else{
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText("Неправильно введены данные");
                                        }
                                    });
                                }
                            }
                        }else{
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    tv.setText("Введите данные");
                                }
                            });
                        }
                    }
                });

                inputProcess.reloadProcess(activity);
                final RelativeLayout layout = (RelativeLayout)editText.getKeyBoard(activity, R.layout.l_keyboard_2);
                layout.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layout.setLayoutParams(layoutParams);
                layout.setGravity(Gravity.BOTTOM);
                InitView.add(layout);
                editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if (b) layout.setVisibility(View.VISIBLE);
                        else layout.setVisibility(View.GONE);
                    }
                });
            }
        }),new ButtonMenu("Обратная матрица", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final YEdit editText = new YEdit(activity);
                editText.setHint("Матрица");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{editText},tv,new InputProcess.OnClear(){
                    @Override
                    public void onClear(View view) {
                        tv.setText("");
                        editText.setText("");
                    }
                }, new InputProcess.OnExecute() {
                    @Override
                    public void onExecute(String... data) {
                        if (!data[0].equals("")) {
                            Matrix m = Matrix.fromString(data[0]);
                            if (m == null){
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        tv.setText("Неправильно введены данные");
                                    }
                                });
                            }else{
                                final Matrix a = m.func_3();
                                if (a != null) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText(a.toString());
                                        }
                                    });
                                }else{
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv.setText("Неправильно введены данные");
                                        }
                                    });
                                }
                            }
                        }else{
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    tv.setText("Введите данные");
                                }
                            });
                        }
                    }
                });

                inputProcess.reloadProcess(activity);
                final RelativeLayout layout = (RelativeLayout)editText.getKeyBoard(activity, R.layout.l_keyboard_2);
                layout.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layout.setLayoutParams(layoutParams);
                layout.setGravity(Gravity.BOTTOM);
                InitView.add(layout);
                editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if (b) layout.setVisibility(View.VISIBLE);
                        else layout.setVisibility(View.GONE);
                    }
                });
            }
        })


        });
    }
}
