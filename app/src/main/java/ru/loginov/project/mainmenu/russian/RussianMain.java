package ru.loginov.project.mainmenu.russian;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.R;
import ru.loginov.project.S;
import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.resourse.MyResourse;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 12.11.2016.
 */
public class RussianMain extends IMenu {
    //new String[]{,,"Подлежащее и способы его выражения","Второстепенные члены предложения","Виды обстоятельств","Односоставные предложения","Обособленные члены предложения","Обособление определений и приложений"},
    @Override
    public Menu getMenu(final Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Лексика. Фразеология. Лексикография", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        Menu m = new Menu(new ButtonMenu[]{
                                new ButtonMenu("Слово и его значение", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        InitView.initText(activity, MyResourse.strar41[0]);
                                    }
                                }),
                                new ButtonMenu("Однозначность и многозначность слов", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        InitView.initText(activity,MyResourse.strar41[1]);
                                    }
                                }),
                                new ButtonMenu("Изобразительно-выразительные средства русского языка",S.getClick(activity,false,"Изобразительно-выразительные средства русского языка",MyResourse.strar42,MyResourse.strar43,null)),
                                new ButtonMenu("Омонимы", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        InitView.initText(activity,MyResourse.strar41[2]);
                                    }
                                }),
                                new ButtonMenu("Паронимы", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        InitView.initText(activity,MyResourse.strar41[3]);
                                    }
                                }),

                                new ButtonMenu("Синонимы", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        InitView.initText(activity,MyResourse.strar41[4]);
                                    }
                                }),
                                new ButtonMenu("Антонимы", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        InitView.initText(activity,MyResourse.strar41[5]);
                                    }
                                }),
                                new ButtonMenu("Лексика общеупотребительная и имеющая ограниченную сферу употребления", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        InitView.initText(activity,MyResourse.strar41[6]);
                                    }
                                }),
                                new ButtonMenu("Устаревфая лексика и неологизмы", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        InitView.initText(activity,MyResourse.strar41[7]);
                                    }
                                })

                        });
                        InitView.initMenu(activity,m,InitView.getNowMenuItem());
                    }
                }),
                new ButtonMenu("Фонетика. Графика. Орфоэпия",S.getClick(activity,false,"Фонетика. Графика. Орфоэпия",MyResourse.strar44,MyResourse.strar45,null))



        });
    }

    @Override
    public OnClickMenu tothis(final Activity activity){
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                NavigationView navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
                InitView.initMenu(activity,getMenu(activity),navigationView.getMenu().getItem(0).getSubMenu().getItem(3));
            }
        };
    };
}
