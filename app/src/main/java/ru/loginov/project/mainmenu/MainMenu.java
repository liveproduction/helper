package ru.loginov.project.mainmenu;

import android.app.Activity;

import ru.loginov.project.mainmenu.chemistry.ChemistryMain;
import ru.loginov.project.mainmenu.english.EnglishMenu;
import ru.loginov.project.mainmenu.fisicmenu.FisicMenu;
import ru.loginov.project.mainmenu.informatic.MainInform;
import ru.loginov.project.mainmenu.mathmenu.MathMenu;
import ru.loginov.project.mainmenu.russian.RussianMain;
import ru.loginov.project.mainmenu.shiphr.ShiphrMain;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;

/**
 * Created by user on 01.11.2016.
 */
public class MainMenu extends IMenu {
    @Override
    public Menu getMenu(Activity activity) {

        return new Menu(new ButtonMenu[]{new ButtonMenu("Математика",new MathMenu().tothis(activity)),new ButtonMenu("Физика",new FisicMenu().tothis(activity)),
                new ButtonMenu("Информатика",new MainInform().tothis(activity)),
                new ButtonMenu("Русский язык",new RussianMain().tothis(activity)),
                new ButtonMenu("Английский язык",new EnglishMenu().tothis(activity)),
                new ButtonMenu("Химия",new ChemistryMain().tothis(activity)),
                new ButtonMenu("Шифрование",new ShiphrMain().tothis(activity))
        });    }
}
