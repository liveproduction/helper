package ru.loginov.project.mainmenu.informatic;

import android.app.Activity;
import android.graphics.Color;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import ru.loginov.project.S;
import ru.loginov.project.mainmenu.IListMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.InputProcess;
import ru.loginov.project.otherclass.MyToast;
import ru.loginov.project.preference.PreferenceValue;

/**
 * Created by user on 07.11.2016.
 */
public class MuchNumber extends IListMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        return new Menu(new ButtonMenu[]{/*new ButtonMenu("Перевод из десятичной в двоичную систему счисления", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditText editText = new EditText(activity);
                editText.setHint("Число в десятичной системе счисления");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{editText}, tv, new InputProcess.OnClear() {
                    @Override
                    public void onClear(View view) {
                        editText.setText("");
                        tv.setText("");
                    }
                }, new InputProcess.OnExecute() {
                    @Override
                    public void onExecute(String... data) {
                        try{
                            String get = data[0].replace(',','.');
                            double g = Double.valueOf(get);
                            tv.setText(S.tento(get,2,0));
                        }catch (Exception e){
                            MyToast.show("Неправильно введены данные");
                        }
                    }
                });
                inputProcess.reloadProcess(activity);
            }
        }),new ButtonMenu("Перевод из десятичной системы счисления", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditText editText = new EditText(activity);
                editText.setHint("Число в десятичной системе счисления");
                final EditText editText1 = new EditText(activity);
                editText1.setHint("Система счисления");
                final TextView tv = new TextView(activity);
                tv.setTextSize(PreferenceValue.textsizeformul);
                tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                InputProcess inputProcess = new InputProcess(new EditText[]{editText,editText1}, tv, new InputProcess.OnClear() {
                    @Override
                    public void onClear(View view) {
                        editText.setText("");
                        editText1.setText("");
                        tv.setText("");
                    }
                }, new InputProcess.OnExecute() {
                    @Override
                    public void onExecute(String... data) {
                        try{
                            int c = Integer.valueOf(data[1]);
                            String get = data[0].replace(',','.');
                            double g = Double.valueOf(get);
                            tv.setText(S.tento(get, c, 0));
                        }catch (Exception e){
                            MyToast.show("Неправильно введены данные");
                        }
                    }
                });
                inputProcess.reloadProcess(activity);
            }
        }),
                new ButtonMenu("Перевод в десятичную систему счисления", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditText editText = new EditText(activity);
                        editText.setHint("Число");
                        final EditText editText1 = new EditText(activity);
                        editText1.setHint("Система счисления");
                        final TextView tv = new TextView(activity);
                        tv.setTextSize(PreferenceValue.textsizeformul);
                        tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                        InputProcess inputProcess = new InputProcess(new EditText[]{editText,editText1}, tv, new InputProcess.OnClear() {
                            @Override
                            public void onClear(View view) {
                                editText.setText("");
                                editText1.setText("");
                                tv.setText("");
                            }
                        }, new InputProcess.OnExecute() {
                            @Override
                            public void onExecute(String... data) {
                                try{
                                    int c = Integer.valueOf(data[1]);
                                    String get = data[0].replace(',','.');
                                    double g = Double.valueOf(get);
                                    tv.setText(S.gototen(get, c));
                                }catch (Exception e){
                                    MyToast.show("Неправильно введены данные");
                                }
                            }
                        });
                        inputProcess.reloadProcess(activity);
                    }
                }),*/
                new ButtonMenu("Перевод в разные системы счисления", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditText editText = new EditText(activity);
                        editText.setHint("Число");
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                        final EditText editText1 = new EditText(activity);
                        editText1.setHint("Система счисления");
                        editText1.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                        final EditText editText2 = new EditText(activity);
                        editText2.setHint("Будущая система счисления");
                        editText2.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                        final TextView tv = new TextView(activity);
                        tv.setTextSize(PreferenceValue.textsizeformul);
                        tv.setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
                        InputProcess inputProcess = new InputProcess(new EditText[]{editText,editText1,editText2}, tv, new InputProcess.OnClear() {
                            @Override
                            public void onClear(View view) {
                                editText.setText("");
                                editText1.setText("");
                                editText2.setText("");
                                tv.setText("");
                            }
                        }, new InputProcess.OnExecute() {
                            @Override
                            public void onExecute(String... data) {
                                try{
                                    int d = Integer.valueOf(data[2]);
                                    int c = Integer.valueOf(data[1]);
                                    String get = data[0].replace(',','.');
                                    if (get.charAt(0) != '-' && get.charAt(0) != '-') {
                                        final String str = S.tento(S.gototen(get, c), d, 0);
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                tv.setText(str);
                                            }
                                        });

                                    }else{
                                        MyToast.show("Только положительные числа");
                                    }
                                }catch (Exception e){
                                    MyToast.show("Неправильно введены данные");
                                }
                            }
                        });
                        inputProcess.reloadProcess(activity);
                    }
                })








        });
    }
}
