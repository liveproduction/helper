package ru.loginov.project.mainmenu.fisicmenu;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.R;
import ru.loginov.project.S;
import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.resourse.MyResourse;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 01.11.2016.
 */
public class FisicMenu extends IMenu {
    @Override
    public Menu getMenu(Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Кинематика",new Kinem().tothis(activity)),
                new ButtonMenu("Динамика",new Dinamic().tothis(activity)),
                new ButtonMenu("Электродинамика",new Energy1().tothis(activity)),
                new ButtonMenu("МКТ и Термодинамика",new MKT().tothis(activity)),
                new ButtonMenu("Магнетизм",new Magnit().tothis(activity)),
                new ButtonMenu("Колебания и Волны", S.getClick(activity,true,"Колебания и Волны",MyResourse.strar53,MyResourse.strar54,MyResourse.strar55)),
                new ButtonMenu("Волны",InitView.createFormul(activity, MyResourse.strar27[0],InitView.getNowMenuItem(),MyResourse.strar46[0])),
                new ButtonMenu("Оптика",new Optic().tothis(activity)),
                new ButtonMenu("Количество теплоты",InitView.createFormul(activity,MyResourse.strar27[1],InitView.getNowMenuItem(),MyResourse.strar46[1])),
                new ButtonMenu("Константы",new Konstant().tothis(activity))
        });
    }

    @Override
    public OnClickMenu tothis(final Activity activity){
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                NavigationView navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
                InitView.initMenu(activity,getMenu(activity),navigationView.getMenu().getItem(0).getSubMenu().getItem(1));
            }
        };
    };
}
