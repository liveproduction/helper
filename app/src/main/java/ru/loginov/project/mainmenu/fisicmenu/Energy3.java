package ru.loginov.project.mainmenu.fisicmenu;

import android.app.Activity;

import ru.loginov.project.S;
import ru.loginov.project.mainmenu.IListMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.resourse.MyResourse;

/**
 * Created by user on 06.11.2016.
 */
public class Energy3 extends IListMenu {
    @Override
    public Menu getMenu(Activity activity) {
        return S.getMENU(activity, MyResourse.strar12,MyResourse.strar13,MyResourse.strar14);
    }
}
