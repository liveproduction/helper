package ru.loginov.project.mainmenu.mathmenu;

import android.app.Activity;

import ru.loginov.project.S;
import ru.loginov.project.mainmenu.IListMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.resourse.MyResourse;

/**
 * Created by user on 03.11.2016.
 */
public class Algebra_Sub extends IListMenu {
    @Override
    public Menu getMenu(Activity activity) {
        name = "Формулы";
        return S.getMENU(activity,MyResourse.strar1,MyResourse.strar2,null);
    }
}
