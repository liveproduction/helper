package ru.loginov.project.mainmenu.fisicmenu;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.R;
import ru.loginov.project.S;
import ru.loginov.project.mainmenu.IListMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.TouchImageView;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.resourse.MyResourse;
import ru.loginov.project.workspace.InitView;
import ru.loginov.project.workspace.MainView;

/**
 * Created by user on 05.11.2016.
 */
public class Kinem extends IListMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        return S.getMENU(activity,MyResourse.strar3,MyResourse.strar4,MyResourse.strar5,new int[]{3},new OnClickMenu[]{new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final TouchImageView touchImageView = new TouchImageView(activity);
                touchImageView.setImageResource(R.drawable.fizic_kinem_4);
                if (PreferenceValue.fotoformul){
                    InitView.setBackground(PreferenceValue.backdrawformul);
                }else{
                    InitView.setBackground(S.getDrawable(PreferenceValue.backcolorformul,PreferenceValue.gradformul));
                }
                InitView.initView(activity,new MainView(touchImageView),InitView.getNowMenuItem());
            }
        }});
    }
}
