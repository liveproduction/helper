package ru.loginov.project;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import java.io.IOException;
import java.util.List;

import ru.loginov.project.dopmenu.About;
import ru.loginov.project.dopmenu.Help;
import ru.loginov.project.mainmenu.chemistry.ChemistryMain;
import ru.loginov.project.mainmenu.english.EnglishMenu;
import ru.loginov.project.mainmenu.fisicmenu.FisicMenu;
import ru.loginov.project.mainmenu.informatic.MainInform;
import ru.loginov.project.mainmenu.mathmenu.MathMenu;
import ru.loginov.project.mainmenu.russian.RussianMain;
import ru.loginov.project.mainmenu.shiphr.ShiphrMain;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.CheckBoxMenu;
import ru.loginov.project.menuclass.MenuView;
import ru.loginov.project.otherclass.MyToast;
import ru.loginov.project.preference.Preference;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.table.TableView;
import ru.loginov.project.workspace.InitView;

/**
 * Главное активити всего приложения. + шторка
 */
public final class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    static DrawerLayout relative = null;//Parent всех View
        static Context context = null;
        static Activity activity = null;
        static boolean start = false;//Запущено ли приложение

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
        context = this;
        activity = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        relative = drawer;
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);


        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        InitView.removeH(this);//Удаление всей истории при перезапуске окна. Нужно чтобы при запуске приложения в нём открывалось главное меню.

        new Task().execute();//Потоковая загрузка
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); *** Только вертикальная ориентация


        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        Log.i("LIVe","MainActivity: onCreate");
    }

    @Override
    protected void onStart() {
        Log.i("LIVe","MainActivity: onStart()");
        super.onStart();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {//Создание меню
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.m, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){//Меню.
        InitView.onMenu(this);
        return true;
    }

    @Override
    protected void onResume() {//При открытии приложения
        super.onResume();
        start = true;
    }

    @Override
    protected void onPause() {//При приостановки приложения
        start = false;
        super.onPause();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_MENU == keyCode){
            InitView.onMenu(this);//Отработка апаратной клавиши меню
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {//Открытие долгим нажатием назад меню в таблицах или быстрый выход из программы
        if (KeyEvent.KEYCODE_BACK == keyCode){
            if (TableView.visible){
                if (PreferenceValue.tablemenuonback) {
                    InitView.onMenu(activity);
                    return true;
                }
            }
                finish();
            return true;
        }
        return super.onKeyLongPress(keyCode, event);
    }

    @Override
    public void onBackPressed() {//Обработка апаратной клавивиши Назад
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (!InitView.onBackPress(this)){
                super.onBackPressed();
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {//Меню из шторки
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.math){
            InitView.initMenu(activity,new MathMenu().getMenu(this),item);
        }
        switch (id){
            case R.id.pref:
                Preference.onStart(this,item);
                break;
            case R.id.clearhistory:
                InitView.removeHistory(this);
                item.setCheckable(false);
                item.setChecked(false);
                break;
            case R.id.fisics:
                InitView.initMenu(activity,new FisicMenu().getMenu(this),item);
                break;
            case R.id.english:
                InitView.initMenu(activity,new EnglishMenu().getMenu(this),item);
                break;
            case R.id.inform:
                InitView.initMenu(activity,new MainInform().getMenu(this),item);
                break;
            case R.id.russian:
                InitView.initMenu(activity,new RussianMain().getMenu(this),item);
                break;
            case R.id.chemistry:
                InitView.initMenu(activity,new ChemistryMain().getMenu(this),item);
                break;
            case R.id.shiphr:
                InitView.initMenu(activity,new ShiphrMain().getMenu(this),item);
                break;
            case R.id.help:
                InitView.initMenu(activity, new Help().getMenu(activity),item);
                break;
            case R.id.aboutprog:
                About.start(activity,item);
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class Task extends AsyncTask<Void,Void,Void> {//Потоковая загрузка
        /*ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(context,"","Загрузка");
        }*/

        @Override
        protected Void doInBackground(Void... voids) {
            S.initHelper(relative,activity);
            return null;
        }

        /*@Override
        protected void onPostExecute(Void aVoid){
               progressDialog.cancel();
            progressDialog.dismiss();
        }*/
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data){//Получение фоток из галлереи
        if (activity != null) {
            if (resultCode == RESULT_OK) {
                Uri fileUri = data.getData();
                ContentResolver cr = activity.getContentResolver();
               /* if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    final int takeFlags = data.getFlags() & Intent.FLAG_GRANT_READ_URI_PERMISSION;
                    cr.takePersistableUriPermission(fileUri, takeFlags);
                }*/
                if (requestCode == 0){
                    Preference.setPref(Preference.foto1,S.getRealPath(this,fileUri));
                    try {
                        PreferenceValue.backdrawmenu = new BitmapDrawable(MediaStore.Images.Media.getBitmap(cr, fileUri));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    InitView.reloadNow(activity,0);
                }else
                if (requestCode == 1){
                    Preference.setPref(Preference.foto2,S.getRealPath(this,fileUri));

                    try {
                        PreferenceValue.backdrawformul = new BitmapDrawable(MediaStore.Images.Media.getBitmap(cr,fileUri));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    InitView.reloadNow(activity,0);
                }else
                if (requestCode == 2){
                    Preference.setPref(Preference.input4,S.getRealPath(this,fileUri));
                    try {
                        PreferenceValue.backfotoinput = new BitmapDrawable(MediaStore.Images.Media.getBitmap(cr,fileUri));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    InitView.reloadNow(activity);
                }else
                if (requestCode == 3){
                    Preference.setPref(Preference.foto3,S.getRealPath(this,fileUri));
                    try {
                        PreferenceValue.backfotoheadernavbar = new BitmapDrawable(MediaStore.Images.Media.getBitmap(cr,fileUri));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    InitView.reloadNavBar(activity);
                }else
                    if (requestCode == 4){
                        Preference.setPref(Preference.foto4,S.getRealPath(this,fileUri));
                        try {
                            PreferenceValue.backfotonavmenu = new BitmapDrawable(MediaStore.Images.Media.getBitmap(cr,fileUri));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        InitView.reloadNavBar(activity);
                    }
                else
                   if (requestCode == 5){
                       Preference.setPref(Preference.foto5,S.getRealPath(this,fileUri));
                       try {
                           PreferenceValue.fotoclick = new BitmapDrawable(MediaStore.Images.Media.getBitmap(cr,fileUri));
                           S.getRealDrawable(activity,PreferenceValue.heightadapter,true,false);
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
                       InitView.reloadNow(this);
                   }
                else
                       if (requestCode == 6){
                           Preference.setPref(Preference.foto6,S.getRealPath(this,fileUri));
                           try {
                               PreferenceValue.fotoback = new BitmapDrawable(MediaStore.Images.Media.getBitmap(cr,fileUri));
                               S.getRealDrawable(activity,PreferenceValue.heightadapter,false,true);
                           } catch (IOException e) {
                               e.printStackTrace();
                           }
                           InitView.reloadNow(this);
                       }
            }
        }
    }

    public static final int THEMEMENU_3 = 1;
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case THEMEMENU_3: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    PreferenceValue.backdrawmenuboolean = false;
                    View view = InitView.getNow().getView();
                    if (view instanceof MenuView){
                        ru.loginov.project.menuclass.Menu menu = ((MenuView) view).getMenu();
                        List<ButtonMenu> list = menu.getButtoms();
                        list.get(0).setEnabled(true);
                        list.get(1).setEnabled(true);
                        ((CheckBoxMenu)((MenuView) view).getMenu().getButton(((MenuView) view).getMenu().getNames().indexOf("Фото"))).setChecked(false);
                        list.get(3).setEnabled(false);
                    }
                    InitView.reloadNow(activity);
                }
                return;
            }
            case 2:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){}else{
                    PreferenceValue.havebackfotoinput = false;
                    View view = InitView.getNow().getView();
                    if (view instanceof MenuView){
                        ru.loginov.project.menuclass.Menu menu = ((MenuView) view).getMenu();
                        List<ButtonMenu> list = menu.getButtoms();
                        list.get(list.size()-1).setEnabled(false);
                        list.get(list.size()-3).setEnabled(true);
                        list.get(list.size()-4).setEnabled(true);
                        ((CheckBoxMenu)list.get(list.size()-2)).setChecked(false);
                    }
                    InitView.reloadNow(activity);
                }
                break;
            }
            case 3:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){}else{
                    PreferenceValue.havebackfotoinput = false;
                    View view = InitView.getNow().getView();
                    if (view instanceof MenuView){
                        ru.loginov.project.menuclass.Menu menu = ((MenuView) view).getMenu();
                        List<ButtonMenu> list = menu.getButtoms();
                        list.get(0).setEnabled(true);
                        list.get(1).setEnabled(true);
                        ((CheckBoxMenu)list.get(2)).setChecked(false);
                        list.get(3).setEnabled(false);
                    }
                    InitView.reloadNow(activity);
                }
                break;
            }
            case 4:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){}else{
                    PreferenceValue.havebackfotoinput = false;
                    View view = InitView.getNow().getView();
                    if (view instanceof MenuView){
                        ru.loginov.project.menuclass.Menu menu = ((MenuView) view).getMenu();
                        List<ButtonMenu> list = menu.getButtoms();
                        list.get(4).setEnabled(true);
                        list.get(5).setEnabled(true);
                        ((CheckBoxMenu)list.get(6)).setChecked(false);
                        list.get(7).setEnabled(false);
                    }
                    InitView.reloadNow(activity);
                }
                break;
            }
            case 5:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){}else{
                    PreferenceValue.havebackfotoinput = false;
                    View view = InitView.getNow().getView();
                    if (view instanceof MenuView){
                        ru.loginov.project.menuclass.Menu menu = ((MenuView) view).getMenu();
                        List<ButtonMenu> list = menu.getButtoms();
                        list.get(1).setEnabled(true);
                        ((CheckBoxMenu)list.get(2)).setChecked(false);
                        list.get(3).setEnabled(false);
                    }
                    InitView.reloadNow(activity);
                }
                break;
            }
            case 6:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){}else{
                    PreferenceValue.havebackfotoinput = false;
                    View view = InitView.getNow().getView();
                    if (view instanceof MenuView){
                        ru.loginov.project.menuclass.Menu menu = ((MenuView) view).getMenu();
                        List<ButtonMenu> list = menu.getButtoms();
                        list.get(4).setEnabled(true);
                        ((CheckBoxMenu)list.get(5)).setChecked(false);
                        list.get(6).setEnabled(false);
                    }
                    InitView.reloadNow(activity);
                }
                break;
            }
            case 7:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){}else{
                    PreferenceValue.havebackfotoinput = false;
                    View view = InitView.getNow().getView();
                    if (view instanceof MenuView){
                        ru.loginov.project.menuclass.Menu menu = ((MenuView) view).getMenu();
                        List<ButtonMenu> list = menu.getButtoms();
                        list.get(3).setEnabled(true);
                        list.get(4).setEnabled(true);
                        ((CheckBoxMenu)list.get(5)).setChecked(false);
                        list.get(6).setEnabled(false);
                    }
                    InitView.reloadNow(activity);
                }
                break;
            }
        }
    }

    @Override
    protected void onStop() {//При приостановки
        Log.i("LIVe","MainActivity: onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {//При очищении из памяти
        Log.i("LIVe","MainActivity: onDestroy()");
        S.writeDataBase(this);
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {//При изменении ориентации
        super.onConfigurationChanged(newConfig);
        InitView.reloadNow(activity);
    }

    private float x1;
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {//Отработка свайпа. P.S. Открытие приложения по свайпу
        if (ev.getAction() == ev.ACTION_DOWN){
            x1 = ev.getX();
        }
        if (ev.getAction() == ev.ACTION_UP){
            float l = ev.getX() - x1;
            float maxl = PreferenceValue.lenghtnavbar;
            if (maxl > 0) {
                if (l > maxl) {
                    if (PreferenceValue.startintent){
                        try {
                            PackageManager pm = getPackageManager();
                            Intent i = pm.getLaunchIntentForPackage(PreferenceValue.packintent);
                            startActivity(i);
                        }catch (Exception e){
                            MyToast.show("Приложение не найдено");
                        }
                    }else {
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        if (!drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.openDrawer(GravityCompat.START);
                        }
                    }
                }
            }else if (maxl < 0){
                if (l<maxl){
                    if (PreferenceValue.startintent){
                        try {
                            PackageManager pm = getPackageManager();
                            Intent i = pm.getLaunchIntentForPackage(PreferenceValue.packintent);
                            startActivity(i);
                        }catch (Exception e){
                            MyToast.show("Приложение не найдено");
                        }
                    }else {
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        if (!drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.openDrawer(GravityCompat.START);
                        }
                    }
                }
            }else if (maxl == 0){

            }
        }
        return super.dispatchTouchEvent(ev);
    }
}
