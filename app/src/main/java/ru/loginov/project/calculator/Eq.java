package ru.loginov.project.calculator;

public class Eq {

	public static YObject getY(String str){
		str = replacechar(str);
		if (str.endsWith("x")){
			str += "+";
		}
		YObject y = new YObject();
		double value = 1;
		int power = 0;
		boolean isx = false;
		boolean isminus = false;
		String s = "";
		for (int i = 0;i<str.length();i++){
			if (str.charAt(i) == 'x'){
				if (!s.equals("")){
					if (isminus){
						value = -Double.valueOf(s);
					}else{
						value = Double.valueOf(s);
					}
					s = "";
				}
				else{
					if (isminus){
						value = -1;
					}else
						value = 1;
				}
				power = 1;
				isx = true;
			}else if (str.charAt(i) == '^'){}else if (str.charAt(i) == '-'){

				if (isx){
					if (!s.equals("")){
						power = Integer.valueOf(s);
					}
				}else{
					if (!s.equals("")){
						if (isminus){
							value = -Double.valueOf(s);
						}else{
							value = Double.valueOf(s);
						}
					}
				}
				y.addItem(value, power);
				value = 1;
				power = 0;
				isx = false;
				s = "";
				isminus = true;
			}

			else{
				if (isNumber(String.valueOf(str.charAt(i)))){
					s+=str.charAt(i);
				}else{
					if (isx){
						if (!s.equals("")){
								power = Integer.valueOf(s);
						}
					}else{
						if (!s.equals("")){
							if (isminus){
								value = -Double.valueOf(s);
							}else{
								value = Double.valueOf(s);
							}
						}
					}
					y.addItem(value, power);
					value = 1;
					power = 0;
					isx = false;
					isminus = false;
					s = "";
				}
			}
		}

		if (!s.equals("")){
			if (isx){
				if (!s.equals("")){
					power = Integer.valueOf(s);
				}
			}else{
				if (!s.equals("")){
					if (isminus){
						value = -Double.valueOf(s);
					}else{
						value = Double.valueOf(s);
					}
				}
			}
			y.addItem(value, power);
			value = 1;
			power = 0;
			isx = false;
			s = "";
		}
		y.sort();
		return y;
	}
	
	public static boolean isNumber(String a){
		try{
			Double.valueOf(a);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	private static String replacechar(String str) {
		str = str.toLowerCase();
		str = str.replace('-', '-');
		str = str.replace(" ", "");
		str = str.replace(',', '.');
		return str;
	}
	
}
