package ru.loginov.project.calculator;

import java.util.ArrayList;
import java.util.List;

public class Calculator {
	private static int findleft(String str,char t){
		int u;
		u = -8;
		for (int i = 0;i<str.length();i++){
			if (str.charAt(i) == t){
				return i;
			}
		}
		if (u == -8){
			return -1;
		}
			else{
				return u;
			}
	}
	
	private static int[] getscob(String str){
		int[] ret = new int[str.length()];
		int sc = Integer.MIN_VALUE;
		for (int i = 0;i<str.length();i++){
			if(str.charAt(i) == '('){
				ret[i] = sc;
				sc++;
			}
			else{
				if (str.charAt(i) == ')'){
					ret[i] = -sc;
					sc--;
				}
			}
		}
		return ret;
	}
	
	private static byte getid(char c){
		switch(c){
		case '+':
			return 1;
		case '-':
			return 2;
		case '/':
			return 3;
		case '*':
			return 4;
			default:
				return 0;
		}
	}
	
	private static byte[] getid(String str){
		byte[] ret = new byte[str.length()];
		for (int i = 0;i<str.length();i++){
			ret[i] = getid(str.charAt(i));
		}
		return ret;
	}
	
	private static CObject cn(List<CObject> arg){
		if (arg.size() > 2){
			switch(arg.get(1).getId()){
			case 1:
				return new CObject(String.valueOf(arg.get(0).getValue() + arg.get(2).getValue()));
			case 2:
				return new CObject(String.valueOf(arg.get(0).getValue() - arg.get(2).getValue()));
			case 3:
				return new CObject(String.valueOf(arg.get(0).getValue() / arg.get(2).getValue()));
			case 4:
				return new CObject(String.valueOf(arg.get(0).getValue() * arg.get(2).getValue()));
			}
		}
		return new CObject("0");
	}
	
	private static List<CObject> getFormul(String str,byte[] op){
		List<CObject> ret = new ArrayList<CObject>();
		String s = "";
		for (int i = 0;i<str.length();i++){
			if (op[i] == 0){
				s+= str.charAt(i);
			}else{
				try{
					ret.add(new CObject(s));
					ret.add(new CObject(op[i]));
				}catch(Exception e){
					e.printStackTrace();
				}
				s= "";
			}
		}
		if (!s.equals("")){
			try{
			ret.add(new CObject(s));
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return ret;
	}
	
	private static String calculate(List<CObject> arg){
		if (arg.size() > 0){
			for (int i = 0;i<arg.size();i++){
				if (arg.get(i).getId() == 4){
					CObject rep = cn(arg.subList(i-1, i+2));
					arg.set(i, rep);
					arg.remove(i-1);
					arg.remove(i);
					i-=2;
				}
			}
			for (int i = 0;i<arg.size();i++){
				if (arg.get(i).getId() == 3){
					CObject rep = cn(arg.subList(i-1, i+2));
					arg.set(i, rep);
					arg.remove(i-1);
					arg.remove(i);
					i-=2;
				}
			}
			for (int i = 0;i<arg.size();i++){
				if (arg.get(i).getId() == 2){
					CObject rep = cn(arg.subList(i-1, i+2));
					arg.set(i, rep);
					arg.remove(i-1);
					arg.remove(i);
					i-=2;
				}
			}
			for (int i = 0;i<arg.size();i++){
				if (arg.get(i).getId() == 1){
					CObject rep = cn(arg.subList(i-1, i+2));
					arg.set(i, rep);
					arg.remove(i-1);
					arg.remove(i);
					i-=2;
				}
			}
			String ret = "";
			for (int i = 0;i<arg.size();i++){
				ret+= String.format("%1$.5f",arg.get(i).getValue());
			}
			return ret;
		}
		
		return "0";
	}
	
	private static String replacechar(String str){
		str = str.toLowerCase();
		str = str.replace('-', '-');
		str = str.replace(" ", "");
		str = str.replace(',', '.');
		return str;
	}
	
	public static String calculate(String str){
		if (str.length() == 0){
			return "0";
		}
		str = replacechar(str);
		String calc;
		if (findleft(str,'(') > -1){
			calc = returnstr(str);
		}else{
			calc = str;
		}
		byte[] op = getid(calc);
		return calculate(getFormul(calc,op));
	}
	
	private static String returnstr(String str){
		int r[] = getscob(str);
		String c = "";
		for (int i = 0;i<r.length;i++){
			if (r[i] < 0){
				for (int j = i;j<r.length;j++){
					if (r[j] + r[i] + 1 == 0){
						c += calculate(str.subSequence(i+1, j).toString());
						i = j;
						break;
					}
				}
			}else if (r[i] == 0){
				c+= str.charAt(i);
			}
		}
		return c;
	}
	
	

}
