package ru.loginov.project.calculator;

public class CObject {
	protected byte id = 0;
	protected double value = 0;
	public CObject(String v){
		this.id = 0;
		this.value = getValue(v);
	}
	
	public CObject(){}
	
	public CObject(byte id){
		this.id = id;
		this.value = 0;
	}
	public double getValue(){
		return value;
	}
	public byte getId(){
		return id;
	}
	
	protected double getValue(String str){
		str = replacechar(str);
		if (str.startsWith("sin")){
			return Math.sin(Math.toRadians(getValue(str.substring(3))));
		}else
		if (str.startsWith("cos")){
			return Math.cos(Math.toRadians(getValue(str.substring(3))));
		}else
		if (str.startsWith("tan")){
			return Math.tan(Math.toRadians(getValue(str.substring(3))));
		}else
		if (str.startsWith("tg")){
			return Math.tan(Math.toRadians(getValue(str.substring(2))));
		}else
		if (str.startsWith("ctan")){
			return (1/Math.tan(Math.toRadians(getValue(str.substring(4)))));
		}else
		if (str.startsWith("ctg")){
			return (1/Math.tan(Math.toRadians(getValue(str.substring(3)))));
		}else
		if (str.endsWith("!")){
			double z = getValue(str.substring(0, str.length()-1));
			return z*gam(z);
		}
		if (str.startsWith("sinh")){
			return Math.sinh(Math.toRadians(getValue(str.substring(4))));
		}else
			if (str.startsWith("exp")){
				return Math.exp((getValue(str.substring(3))));
			}
			else
				if (str.startsWith("ln")){
					return Math.log((getValue(str.substring(2))));
				}
				else
				if (str.startsWith("lg")){
					return Math.log10((getValue(str.substring(2))));
				}
				else
					if (str.startsWith("log")){
						return Math.log10(getValue(str.substring(3)));
					}
		
		
		
		
		else{
			if (str.contains("sqrt")){
				int index = str.indexOf("sqrt");
				String prevalue = "";
				for (int i = 0;i<index;i++){
					prevalue += str.charAt(i);
				}
				if (prevalue.equals("")){
					prevalue = "2";
				}
				String postvalue = "";
				for (int i = index+4;i<str.length();i++){
					postvalue += str.charAt(i);
				}
				return Math.pow(getValue(postvalue),1/getValue(prevalue));
			}
			if (str.contains("log")){
				int index = str.indexOf("log");
				return (Math.log(getValue(str.substring(index+3))) / Math.log(getValue(str.substring(0,index))));
			}
			if (str.contains("^")){
				int index = str.indexOf("^");
				return Math.pow(getValue(str.substring(0,index)), getValue(str.substring(index+1,str.length())));
			}
			
		}
		try{
			return Double.valueOf(str);
		}catch(Exception e){
			return 0;
		}
	}
 private static double gam(double z){
	 return Math.exp(gammalog(z));
 }
 

private static double gammalog(double x){
	double[] c =new double[]{
			2.5066282746310005,
		       1.0000000000190015,
		       76.18009172947146,
		      -86.50532032941677,
		       24.01409824083091,
		      -1.231739572450155,
		       0.1208650973866179e-2,
		      -0.5395239384953e-5,};
	return (x+0.5) * Math.log(x+5.5) - (x+5.5) + Math.log(c[0]*(c[1] + c[2]/(x+1) + c[3]/(x+2) + c[4] / (x+3) + c[5] / (x+4) + c[6] / (x+5) + c[7] / (x+8))/x);
}

private static String replacechar(String str){
	str = str.toLowerCase();
	str = str.replace('-', '-');
	str = str.replace(" ", "");
	str = str.replace(',', '.');
	return str;
}
}
