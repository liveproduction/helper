package ru.loginov.project.calculator;

public class XObject{

	protected double power = 0;
	protected double value = 0;
	
	public XObject(double power){
		this.power = power;
	}
	
	public XObject addValue(double addvalue){
		value += addvalue;
		return this;
	}
	
	public void removeValue(double rvalue){
		value -= rvalue;
	}
	
	public void setValue(double value){
		this.value = value;
	}
	
	public double getPower(){
		return power;
	}
	
	public double getValue(){
		return value;
	}
	
	public void setPower(double power){
		this.power = power;
	}
	
}
