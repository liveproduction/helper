package ru.loginov.project.calculator;

import java.util.ArrayList;
import java.util.List;

public class Vector {
    double x = 0;
    double y = 0;
    double z = 0;
    public Vector(){}
    public Vector(double x,double y,double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double smult(Vector b){
        try{
            return x*b.x + y*b.y + z*b.z;
        }catch (Exception e){
            return 0;
        }
    }

    public Vector mult(Vector b){ //Векторное произведение
        try {
            return new Vector(this.y * b.z - b.y * this.z,-(this.x * b.z - b.x * this.z),this.x * b.y - b.x * this.y);
        }catch (Exception e){
            return new Vector();
        }
    }

    public double lenght(){ //Длина вектора
        try {
            return Math.sqrt(x * x + y * y + z * z);
        }catch (Exception e){
            return -1;
        }
    }

    public double mult(Vector b,Vector c){//Смешанное произведение
        try{
            return new Matrix(3,new double[][]{{this.x,this.y,this.z},{b.x,b.y,b.z},{c.x,c.y,c.z}}).func_0();
        }catch (Exception e){
            return -1;
        }
    }

    public static double squareRect(Vector a,Vector b){ //Плошадь параллелограма
        return new Matrix(3,new double[][]{{1,1,1},{a.x,a.y,a.z},{b.x,b.y,b.z}}).func_0();
    }

    public static double squareTriangle(Vector a,Vector b){ //Площадь треугольника
        return squareRect(a,b)/2;
    }

    public static double volumeRect(Vector a,Vector b,Vector c){//Объём параллелепипеда
        return new Matrix(3,new double[][]{{a.x,a.y,a.z},{b.x,b.y,b.z},{c.x,c.y,c.z}}).func_0();
    }

    public static double volumePrism(Vector a,Vector b,Vector c){//Объём пирамиды
        return volumeRect(a,b,c)/6;
    }

    public static Vector fromString(String str){//Получение вектора из строки
        List<Double> list = new ArrayList<Double>();
        str = str.replace(',','.');
        while (!str.equals("")){
            int index = str.indexOf(';');
            if (index > -1) {
                String l = str.substring(0, index);
                try{
                    list.add(Double.valueOf(l));
                    str = str.substring(index+1);
                }catch (Exception e){
                    list.add(0.0);
                }
            }else{
                try {
                    list.add(Double.valueOf(str));
                }catch (Exception e){}
                break;
            }
        }
        if (list.size() < 1) return new Vector();
        else if (list.size() == 1) return new Vector(list.get(0),0,0);
        else if (list.size() == 2) return new Vector(list.get(0),list.get(1),0);
        else return new Vector(list.get(0),list.get(1),list.get(2));
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(x).append(";").append(y).append(";").append(z).append(";");
        return sb.toString();
    }
}
