package ru.loginov.project.calculator;

import java.util.ArrayList;
import java.util.List;

public class Matrix {

    int size_0 = 0;
    int size_1 = 0;
    double[][] value = null;

    public Matrix(){};
    public Matrix(int n){
        this.size_0 = n;
        this.size_1 = n;
        this.value = new double[n][n];
    };
    public Matrix(int m,int n){
        this.size_0 = m;
        this.size_1 = n;
        this.value = new double[m][n];
    }
    public Matrix(int n,double[][] value){
        this.size_0 = n;
        this.size_1 = n;
        this.value = value;
    }
    public Matrix(int m,int n,double[][] a){
        this.size_0 = m;
        this.size_1 = n;
        this.value = a;
    }

    public double func_0(){ // Определитель матрицы
        if (value != null){
            if (size_0 == size_1){
                    if (size_0 == 1) return value[0][0];
                    if (size_0 == 2) return value[0][0]*value[1][1] - value[0][1]*value[1][0];
                    if (size_0 == 3) return value[0][0]*value[1][1]*value[2][2] - value[0][0]*value[2][1]*value[1][2] - value[0][1]*value[1][0]*value[2][2]+value[0][1]*value[2][0]*value[1][2] + value[0][2]*value[1][0]*value[2][1] - value[0][2]*value[2][0]*value[1][1];
                    if (size_0 > 3){
                        long ret = 0;
                        for (int i = 0;i<size_0;i++){
                            if (i%2 == 0){
                                ret += value[0][i] * func_1(0,i).func_0();
                            }else{
                                ret -= value[0][i] * func_1(0,i).func_0();
                            }
                        }
                        return ret;
                    }
            }
        }
        return -1;
    }

    public Matrix func_1(int x,int y){
        double[][] value = new double[size_0-1][size_1-1];
        int l_0 = 0;
        int l_1 = 0;
        for (int i = 0;i<size_0;i++){
            if (i != x){
                for (int j = 0;j<size_1;j++){
                    if (j != y){
                        value[l_0][l_1++] = this.value[i][j];
                    }
                }
                l_0++;
                l_1 = 0;
            }
        }
        return new Matrix(size_0-1,size_1-1,value);
    }

    public String toString(){
        if (value != null){
            if (size_0 > 0 && size_1 > 0){
                StringBuilder str = new StringBuilder();
                for (int i = 0;i<size_0;i++){
                    for (int j = 0;j<size_1;j++){
                        str.append(value[i][j]);
                        if (j != size_1-1){
                            str.append(';');
                        }
                    }
                    str.append('\n');
                }
                return str.toString();
            }
        }
        return null;
    }

    public static Matrix fromString(String str){
        str = str.replace(',','.');
        int n_0 = 0;
        List<List<Double>> l_1 = new ArrayList<List<Double>>();
        int max = 0;
        String g = "";
        while (!str.equals("")){
            List<Double> local = new ArrayList<Double>();
            int index = str.indexOf('\n');
            String l = str;
            if (index > -1) l = str.substring(0,index);
            for (int j = 0;j<l.length();j++){
                if (l.charAt(j) == ';'){
                    local.add(Double.valueOf(g));
                    g = "";
                }else{
                    g+=l.charAt(j);
                }
            }
            if (!g.equals("")) local.add(Double.valueOf(g));
            g = "";
            l_1.add(local);
            if(local.size() > max) max = local.size();
            index = str.indexOf('\n');
            if (index > -1) str = str.substring(index+1);
            else str = "";
            n_0++;
        }
        double[][] ret = new double[n_0][max];
        for (int i = 0;i<n_0;i++){
            List<Double> local = l_1.get(i);
            for (int j = 0;j<local.size();j++){
                ret[i][j] = local.get(j);
            }
        }
        return new Matrix(n_0,max,ret);
    }

    public Matrix add(Matrix a){
        if (this.size_0 == a.size_0 && this.size_1 == a.size_1){
            double[][] ret = new double[size_0][size_1];
            for (int i = 0;i<size_0;i++){
                for (int j = 0;j<size_1;j++){
                    ret[i][j] = this.value[i][j] + a.value[i][j];
                }
            }
            return new Matrix(size_0,size_1,ret);
        }
        return null;
    }

    public Matrix sub(Matrix a){
        if (this.size_0 == a.size_0 && this.size_1 == a.size_1){
            double[][] ret = new double[size_0][size_1];
            for (int i = 0;i<size_0;i++){
                for (int j = 0;j<size_1;j++){
                    ret[i][j] = this.value[i][j] - a.value[i][j];
                }
            }
            return new Matrix(size_0,size_1,ret);
        }
        return null;
    }

    public Matrix mult(int a){
        double[][] ret = new double[size_0][size_1];
        for (int i = 0;i<size_0;i++){
            for (int j=0;i<size_1;i++){
                ret[i][j] = this.value[i][j] * a;
            }
        }
        return new Matrix(size_0,size_1,ret);
    }

    public Matrix mult(Matrix a){
        if (size_1 == a.size_0){
            double[][] ret = new double[size_0][a.size_1];
            for (int i = 0;i<size_0;i++){
                for (int j = 0;j<a.size_1;j++){
                    for (int k = 0;k<size_1;k++){
                        ret[i][j] += this.value[i][k] * a.value[k][j];
                    }
                }
            }
            return new Matrix(size_0,a.size_1,ret);
        }
        return null;
    }

    public Matrix func_2(){
        if (size_1 == size_0) {
            double[][] ret = new double[size_1][size_0];
            for (int i = 0; i < size_0; i++) {
                for (int j = 0;j<size_1;j++){
                    ret[j][i] = value[i][j];
                }
            }
            return new Matrix(size_1,size_0,ret);
        }
        return null;
    }

    public Matrix func_3(){
        double a = this.func_0();
        double[][] ret = new double[size_0][size_1];
        if (a == 1) {
            for (int i = 0; i < size_0; i++) {
                for (int j = 0; j < size_1; j++) {
                    if ((i + j) % 2 == 0) {
                        ret[i][j] += func_1(i, j).func_0();
                    } else {
                        ret[i][j] -= func_1(i, j).func_0();
                    }
                }
            }
        }else{
            for (int i = 0; i < size_0; i++) {
                for (int j = 0; j < size_1; j++) {
                    if ((i + j) % 2 == 0) {
                        ret[i][j] += func_1(i, j).func_0() / a;
                    } else {
                        ret[i][j] -= func_1(i, j).func_0() / a;
                    }
                }
            }
        }
        return new Matrix(size_0,size_1,ret);
    }
}
