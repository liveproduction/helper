package ru.loginov.project.calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

public class YObject {
	
	List<XObject> items = new ArrayList<XObject>();
	
	public YObject(){}
	
	public void addItem(double value,double power){
		int index = havePower(power);
		if (index > -1){
			items.get(index).addValue(value);
		}else{
			items.add(new XObject(power).addValue(value));
		}
	}
	
	public double getValue(double power){
		int index = havePower(power);
		if (index > -1){
			return items.get(index).getValue();
		}
		return 0;
	}
	
	public int havePower(double power){
		for (int i = 0;i<items.size();i++){
			if (items.get(i).getPower() == power){
				return i;
			}
		}
		return -1;
	}
	
	public boolean isMaxPower(double power){
		if (havePower(power) > -1){
			
		}else{
			return false;
		}
		
		for (int i = 0;i<items.size();i++){
			if (items.get(i).getPower() > power){
				return false;
			}
		}
		return true;
	}

	public void deletePowerOn(double rempower){
		for (int i = 0;i<items.size();i++){
			items.get(i).setPower(items.get(i).getPower() - rempower);
		}
	}

	public List<String> getX(){
		List<String> list = new ArrayList<String>();
		double[] a = new double[items.size()];
		int[] b = new int[items.size()];
		for (int i = 0;i<items.size();i++){
			a[i] = items.get(i).getValue();
			b[i] = (int)items.get(i).getPower();
		}
		double[] ret = removeDuplicates(getNumbers(a,b));
		for (int i = 0;i<ret.length;i++){
			list.add(String.valueOf(ret[i]));
		}
		return list;
	}

	public static double[] removeDuplicates(double[] values) {
		boolean mask[] = new boolean[values.length];
		int removeCount = 0;

		for (int i = 0; i < values.length; i++) {
			if (!mask[i]) {
				double tmp = values[i];

				for (int j = i + 1; j < values.length; j++) {
					if (tmp == values[j]) {
						mask[j] = true;
						removeCount++;
					}
				}
			}
		}

		double[] result = new double[values.length - removeCount];

		for (int i = 0, j = 0; i < values.length; i++) {
			if (!mask[i]) {
				result[j++] = values[i];
			}
		}
		return result;

	}

	public static long func_0(long a,long b){
		if (a == 0) return a;
		return func_0(b % a,a);
	}

	public static boolean func_1(long[] ar,long a){
		for (int i = 0;i<ar.length;i++){
			if (ar[i] == a) return true;
			if (ar[i] == 0) return false;
		}
		return false;
	}

	public static long[] func_2(long a){
		if (a < 3){
			if (a == 2) return new long[]{1,2};
			if (a == 1) return new long[]{1};
			return new long[0];
		}
		if (func_0(a,1) == 1) return new long[]{1,a};
		long s = (long)Math.sqrt(a)+1;
		s++;
		long[] ret = new long[(int)s];
		ret[0] = 1;
		int index = 1;
		for (long i = 2;i<s;i++){
			if (a % i == 0){
				ret[index++] = i;
				ret[index++] = a / i;
			}
		}
		for (int i = 1; i< index; i++){
			for (int j = 0+i; j< index; j++){
				long local = ret[i] * ret[j];
				if (!func_1(ret,local)) if (a % local == 0) ret[index++] = local;
			}
		}
		if (!func_1(ret,a)) ret[index++] = a;
		return ret;
	}


	public static long[] delitel(long a){
		long[] ret = func_2(a);
		Arrays.sort(ret);
		ret = Arrays.copyOfRange(ret,Arrays.binarySearch(ret,1),ret.length);
		return ret;
	}

	public static double[] delitel(double a){
		long i = 0;
		while (a % 1 != 0){
			a*=10;
			i++;
		}
		long x = (long) a;
		long[] j = func_2(x);
		Arrays.sort(j);
		int index = Arrays.binarySearch(j,1);
		if (index > -1) {
			j = Arrays.copyOfRange(j, index, j.length);
		}
		double[] ret = new double[j.length];
		if (i != 0){
			for (int k = 0;k<ret.length;k++){
				ret[k] = ((double)j[k])/Math.pow(10,i);
			}
		}else{
			for (int k = 0;k<ret.length;k++){
				ret[k] = j[k];
			}
		}

		return ret;
	}

	public static double[] getNumbers(double[] a,int[] b){
		boolean y = false;
		while (b[b.length-1] != 0){
			y = true;
			for (int i = 0;i<b.length;i++){
				b[i]--;
			}
		}
		if (y)  return merge(new double[]{0}, getNumbers(a,b));
		int max = b[0];
		double[] j = new double[max+1];
		j[0] = a[0];
		int l = max;
		int index = 1;
		for (int i = 1;i<j.length;i++){
			while (l - b[index] > 1){
				l--;
				j[i++] = 0;
			}
			j[i] = a[index];
			l = b[index++];
		}
		if (j.length == 3){
			double a_0 = j[0];
			double b_0 = j[1];
			double c_0 = j[2];
			double d = b_0*b_0 - 4*a_0*c_0;
			if (d == 0) return new double[]{-b_0/2/a_0};
			if (d < 0) return new double[0];
			double x1 = (-b_0-Math.sqrt(d))/(2*a_0);
			double x2 = (-b_0+Math.sqrt(d))/(2*a_0);
			return new double[]{x1,x2};
		}
		if (j.length == 2){
			double m = ((double)j[1]) / ((double)j[0]);
			return new double[]{0-m};
		}
		double[] all = delitel(a[a.length-1]);
		double[] all2 = delitel(a[0]);
		if (all2.length > 0){
			for (int i = 0;i<all2.length;i++){
				for (int o = 0;o<all.length;o++){
					double local1_0 = all[o];
					double local1_1 = -all[o];
					double local2 = all2[i];
					double[] j_0 = new double[j.length];
					j_0[0] = j[0];
					for (int k = 1;k<j.length;k++){
						j_0[k] = local1_0 * j_0[k-1] / local2 + j[k];
					}
					if (j_0[j_0.length-1] == 0){
						double[] ret = new double[]{local1_0/local2};
						int[] b_0 = new int[j_0.length-1];
						for (int p = 0;p<b_0.length;p++){
							b_0[p] = max-p-1;
						}
						return merge(ret, getNumbers(Arrays.copyOf(j_0, j_0.length-1),b_0));
					}
					for (int k = 1;k<j.length;k++){
						j_0[k] = local1_1 * j_0[k-1] / local2 + j[k];
					}
					if (j_0[j_0.length-1] == 0){
						double[] ret = new double[]{local1_1/local2};
						int[] b_0 = new int[j_0.length-1];
						for (int p = 0;p<b_0.length;p++){
							b_0[p] = max-p-1;
						}
						return merge(ret, getNumbers(Arrays.copyOf(j_0, j_0.length-1),b_0));
					}
				}
			}
		}else {
			for (int i = 0; i < all.length; i++) {
				double local = all[i];
				double local1 = -all[i];
				double[] j_0 = new double[j.length];
				j_0[0] = j[0];
				for (int k = 1; k < j.length; k++) {
					j_0[k] = local * j_0[k - 1] + j[k];
				}
				if (j_0[j_0.length - 1] == 0) {
					double[] ret = new double[]{local};
					int[] b_0 = new int[j_0.length - 1];
					for (int p = 0; p < b_0.length; p++) {
						b_0[p] = max - p - 1;
					}
					return merge(ret, getNumbers(Arrays.copyOf(j_0, j_0.length - 1), b_0));
				}
				for (int k = 1; k < j.length; k++) {
					j_0[k] = local1 * j_0[k - 1] + j[k];
				}
				if (j_0[j_0.length - 1] == 0) {
					double[] ret = new double[]{local1};
					int[] b_0 = new int[j_0.length - 1];
					for (int p = 0; p < b_0.length; p++) {
						b_0[p] = max - p - 1;
					}
					return merge(ret, getNumbers(Arrays.copyOf(j_0, j_0.length - 1), b_0));
				}
			}
		}
		return new double[0];
	}


	public static double[] merge(double[] ret,double[] ds){
		if (ret.length == 0) return ds;
		if (ds.length == 0) return ret;
		double[] result = new double[ret.length + ds.length];
		for (int i = 0;i<ret.length;i++){
			result[i] = ret[i];
		}
		for (int i = 0;i<ds.length;i++){
			result[i+ret.length] = ds[i];
		}
		Arrays.sort(result);
		return result;
	}

	public double[] getK(){
		double[] d = new double[items.size()];
		Comparator c = new Comparator<XObject>(){

			@Override
			public int compare(XObject o1, XObject o2) {
				if(o1.getPower() > o2.getPower()){
					return -1;
				}else if (o1.getPower() < o2.getPower()){
					return 1;
				}else
					return 0;
			}};
		Object[] a = items.toArray();
		Arrays.sort(a,(Comparator) c);
		ListIterator i = items.listIterator();
		for (Object e:a){
			i.next();
			i.set(e);
		}
		for (int j = 0;j<d.length;j++){
			d[j] = items.get(j).getValue();
		}
		return d;
	}

	public void sort() {
		XObject[] array = new XObject[items.size()];
		array = items.toArray(array);
		Arrays.sort(array, new Comparator<XObject>(){

			@Override
			public int compare(XObject o1, XObject o2) {
				if(o1.getPower() > o2.getPower()){
					return -1;
				}else if (o1.getPower() < o2.getPower()){
					return 1;
				}else
					return 0;
			}});
		items = Arrays.asList(array);
	}
}
