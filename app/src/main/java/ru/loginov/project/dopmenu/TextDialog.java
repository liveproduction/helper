package ru.loginov.project.dopmenu;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import ru.loginov.project.S;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.OnClickMenu;

/**
 * Created by user on 20.11.2016.
 */

public class TextDialog {
    String title;
    String text;
    String buttomtext;

    public TextDialog(String title,String text,String buttomtext){
        this.title = title;
        this.text = text;
        this.buttomtext = buttomtext;
    }

    public void show(Activity activity){
        AlertDialog.Builder adb = new AlertDialog.Builder(activity);
        adb.setTitle(title);
        LinearLayout ll = new LinearLayout(activity);
        ll.setOrientation(LinearLayout.VERTICAL);
        TextView tv = new TextView(activity);
        tv.setTextSize(20);
        tv.setText(text);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20,0,20,0);
        ScrollView sc = new ScrollView(activity);
        sc.setLayoutParams(layoutParams);
        sc.addView(tv);
        ll.addView(sc);
        Button button = new Button(activity);
        button.setBackground(S.getDrawable("#FFFFFFFF", GradientDrawable.Orientation.BL_TR));
        button.setText(buttomtext);
        ll.addView(button);
        adb.setView(ll);
        final Dialog d = adb.create();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.cancel();
                d.dismiss();
            }
        });
        d.show();
    }

    public OnClickMenu getclick(final Activity activity){
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                show(activity);
            }
        };
    }

}
