package ru.loginov.project.dopmenu;

import android.app.Activity;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import ru.loginov.project.S;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;
import ru.loginov.project.workspace.MainView;

/**
 * Created by masa0 on 27.07.2017.
 */

public class About{

    public static void start(Activity activity, MenuItem item){
        ScrollView sc = new ScrollView(activity);
        TextView textView = new TextView(activity);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,RelativeLayout.LayoutParams.FILL_PARENT);
        sc.setLayoutParams(layoutParams);
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        textView.setTextSize(PreferenceValue.textsizeformul);
        textView.setTextColor(Color.parseColor(PreferenceValue.colorformul));
        textView.setText("Программа \"Помощник\"\nВерсия: beta 1.3\nАвтор: LIVe Production\nemail: dantes2104@gmail.com\nДополнительные библиотеки:\nJLaTexMath Library");
        if (PreferenceValue.fotoformul){
            sc.setBackgroundDrawable(PreferenceValue.backdrawformul);
        }else {
            sc.setBackgroundDrawable(S.getDrawable(PreferenceValue.backcolorformul, PreferenceValue.gradformul));
        }
        sc.addView(textView);
        InitView.initView(activity,new MainView(sc),item);
    }

}
