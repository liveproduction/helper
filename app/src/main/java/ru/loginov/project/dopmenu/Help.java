package ru.loginov.project.dopmenu;

import android.app.Activity;

import ru.loginov.project.mainmenu.IMenu;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;

/**
 * Created by user on 20.11.2016.
 */

public class Help extends IMenu {
    @Override
    public Menu getMenu(Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Как установить градиент цветов", new TextDialog("Установка градиента","Установить градиент можно только там, где в подсказке будет написано \"HEX Цвета\". Для этого следует указать составляющие цвета через запятую.","Ок").getclick(activity)),
                new ButtonMenu("Поиск в таблицах",new TextDialog("Поиск в таблицах","Для поска сочетания букв в начале слова напишите \"#\" перед строкой, для поиска после пишите \"$\". Для поиска сроки в любом месте слова поместите строку между \"{\" и \"}\".\n Пример: \"про#н\" = \"#н{про}\"","Ок").getclick(activity)),
                new ButtonMenu("Прозрачность при устаноки цвета",new TextDialog("Прозрачность цвета","Для настройки цвета пишите его полный HEX код. То есть \"AARRGGBB\", где \"AA\" - прозрачность. \"FF\" - 0% прозрачности,\"00\" - 100% прозрачности","Ок").getclick(activity))



        });
    }
}
