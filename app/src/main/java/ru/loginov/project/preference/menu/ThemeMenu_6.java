package ru.loginov.project.preference.menu;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.S;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.CheckBoxMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.EditDialog;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 20.11.2016.
 */

public class ThemeMenu_6 extends PrefMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        final ButtonMenu buttonMenu1 = new ButtonMenu("Цвет заднего фона верхнего элемента", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditDialog editDialog = new EditDialog("Цвет заднего фона верхнего элемента","Ок","HEX Цвета", PreferenceValue.backcolorheadernavbar);
                editDialog.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity) {
                        PreferenceValue.backcolorheadernavbar = editDialog.getOutput();
                        InitView.reloadNavBar(activity);
                    }
                });
                editDialog.getDialog(activity).show();
            }
        });

        final ButtonMenu buttonMenu2 = new ButtonMenu("Фото для верхнего элемента", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                /*Intent in = new Intent(Intent.ACTION_PICK);
                in.setType("image/*");
                activity.startActivityForResult(in, 3);*/
                S.startForSerchFoto(activity,3);
            }
        });

        final CheckBoxMenu boxMenu = new CheckBoxMenu("Устанавливать фото на верхний элемент", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                buttonMenu1.setEnabled(!buttonMenu1.isEnabled());
                buttonMenu2.setEnabled(!buttonMenu2.isEnabled());
                CheckBoxMenu c = (CheckBoxMenu) buttonMenu;
                c.setChecked(!c.isChecked());
                PreferenceValue.havefotohead = !PreferenceValue.havefotohead;
                S.getPermission(activity,5);
                InitView.reloadNow(activity);
                InitView.reloadNavBar(activity);
            }
        },PreferenceValue.havefotohead);


        final ButtonMenu buttonMenu3 = new ButtonMenu("Цвет заднего фона всей шторки", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditDialog editDialog = new EditDialog("Цвет заднего всей шторки","Ок","HEX Цвета", PreferenceValue.backcolornavmenu);
                editDialog.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity) {
                        PreferenceValue.backcolornavmenu = editDialog.getOutput();
                        InitView.reloadNavBar(activity);
                    }
                });
                editDialog.getDialog(activity).show();
            }
        });

        final ButtonMenu buttonMenu4 =  new ButtonMenu("Фото для всей шторки", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                /*Intent in = new Intent(Intent.ACTION_PICK);
                in.setType("image/*");
                activity.startActivityForResult(in, 4);*/
                S.startForSerchFoto(activity,4);

            }
        });

        CheckBoxMenu checkBoxMenu = new CheckBoxMenu("Устанавливать фото на шторку", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                buttonMenu3.setEnabled(!buttonMenu3.isEnabled());
                buttonMenu4.setEnabled(!buttonMenu4.isEnabled());
                CheckBoxMenu c = (CheckBoxMenu) buttonMenu;
                c.setChecked(!c.isChecked());
                PreferenceValue.havefotomenu = !PreferenceValue.havefotomenu;
                S.getPermission(activity,6);
                InitView.reloadNow(activity);
                InitView.reloadNavBar(activity);
            }
        }, PreferenceValue.havefotomenu);

        buttonMenu1.setEnabled(!PreferenceValue.havefotohead);
        buttonMenu2.setEnabled(PreferenceValue.havefotohead);
        buttonMenu3.setEnabled(!PreferenceValue.havefotomenu);
        buttonMenu4.setEnabled(PreferenceValue.havefotomenu);

        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Цвет текста", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog editDialog = new EditDialog("Цвет текста","Ок","HEX Цвет",PreferenceValue.textcolornavbar);
                        editDialog.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                PreferenceValue.textcolornavbar = editDialog.getOutput();
                                InitView.reloadNavBar(activity);
                            }
                        });
                        editDialog.getDialog(activity).show();
                    }
                }),
                buttonMenu1,boxMenu,buttonMenu2,buttonMenu3,checkBoxMenu,buttonMenu4
        });
    }
}
