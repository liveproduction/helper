package ru.loginov.project.preference.menu;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.S;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.CheckBoxMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.MenuView;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.EditDialog;
import ru.loginov.project.otherclass.ListDialog;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 20.10.2016.
 */
public class ThemeMenu_2 extends PrefMenu {
    public Activity activity = null;
    @Override
    public Menu getMenu(final Activity activity) {
        this.activity =activity;

        final ButtonMenu buttonMenu = new ButtonMenu("Цвет нажатий", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditDialog d = new EditDialog("Цвет нажатий", "Ок", "HEX цвета", PreferenceValue.colorontouchmenu);
                d.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity1) {
                        PreferenceValue.colorontouchmenu = d.getOutput();
                        InitView.reloadNow(activity, ((MenuView) InitView.getNow().getView()).getH());
                    }
                });
                d.getDialog(activity).show();
            }
        });
        final ButtonMenu buttonMenu1 = new ButtonMenu("Тип градиента при нажатии", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                ListDialog d = new ListDialog("Тип градиента при нажатии", new Menu(new ButtonMenu[]{
                        new ButtonMenu("Сверху вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradonclickadapter = GradientDrawable.Orientation.TOP_BOTTOM;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Снизу вверх", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradonclickadapter = GradientDrawable.Orientation.BOTTOM_TOP;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Слева направо", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradonclickadapter = GradientDrawable.Orientation.LEFT_RIGHT;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Справа налево", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradonclickadapter = GradientDrawable.Orientation.RIGHT_LEFT;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Сверху слева направо вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradonclickadapter = GradientDrawable.Orientation.TL_BR;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Снизу слева направо вверх", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradonclickadapter = GradientDrawable.Orientation.BL_TR;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Сверху справа налево вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradonclickadapter = GradientDrawable.Orientation.TR_BL;
                                InitView.reloadNow(activity);
                            }
                        }), new ButtonMenu("Снизу справа налево вверх", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        PreferenceValue.gradonclickadapter = GradientDrawable.Orientation.BR_TL;
                        InitView.reloadNow(activity);
                    }
                })
                }));
                d.getDialog(activity).show();
            }
        });


        final ButtonMenu buttonMenu2 = new ButtonMenu("Цвет кнопок", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditDialog d = new EditDialog("Задний фон", "Ок", "HEX цвета", PreferenceValue.backcolormenu);
                d.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity1) {
                        PreferenceValue.backcolormenu = d.getOutput();
                        InitView.reloadNow(activity, ((MenuView) InitView.getNow().getView()).getH());
                    }
                });
                d.getDialog(activity).show();
            }
        });
        final ButtonMenu buttonMenu3 = new ButtonMenu("Тип градиента кнопок", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                ListDialog d = new ListDialog("Тип градиента", new Menu(new ButtonMenu[]{
                        new ButtonMenu("Сверху вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradorientationadapter = GradientDrawable.Orientation.TOP_BOTTOM;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Снизу вверх", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradorientationadapter = GradientDrawable.Orientation.BOTTOM_TOP;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Слева направо", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradorientationadapter = GradientDrawable.Orientation.LEFT_RIGHT;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Справа налево", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradorientationadapter = GradientDrawable.Orientation.RIGHT_LEFT;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Сверху слева направо вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradorientationadapter = GradientDrawable.Orientation.TL_BR;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Снизу слево направо вверх", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradorientationadapter = GradientDrawable.Orientation.BL_TR;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Сверху справа налево вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradorientationadapter = GradientDrawable.Orientation.TR_BL;
                                InitView.reloadNow(activity);
                            }
                        }), new ButtonMenu("Снизу справа налево вверх", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        PreferenceValue.gradorientationadapter = GradientDrawable.Orientation.BR_TL;
                        InitView.reloadNow(activity);
                    }
                })
                }));
                d.getDialog(activity).show();
            }
        });
        ButtonMenu buttonMenu4 = new ButtonMenu("Цвет текста", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditDialog d = new EditDialog("Цвет текста", "Ок", "HEX цвет", PreferenceValue.colormenutext);
                d.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity) {
                        PreferenceValue.colormenutext = d.getOutput();
                        InitView.reloadNow(activity, ((MenuView) InitView.getNow().getView()).getH());
                    }
                });
                d.getDialog(activity).show();
            }
        });
        ButtonMenu buttonMenu5 = new ButtonMenu("Размер текста", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditDialog d = new EditDialog("Размер текста", "Ок", "Целое число", String.valueOf(PreferenceValue.htextadapter));
                d.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity) {
                        try {
                            PreferenceValue.htextadapter = Integer.valueOf(d.getOutput());
                        } catch (Exception e) {
                            InitView.createSnackbarOnNow("Неправильно введено число");
                            PreferenceValue.htextadapter = 10;
                        }
                        InitView.reloadNow(activity, ((MenuView) InitView.getNow().getView()).getH());
                    }
                });
                d.getDialog(activity).show();
            }
        });

        ButtonMenu buttonMenu6 = new ButtonMenu("Высота кнопок", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditDialog d = new EditDialog("Высота кнопок", "Ок", "Целое число", String.valueOf(PreferenceValue.heightadapter));
                d.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity) {
                        try {
                            PreferenceValue.heightadapter = Integer.valueOf(d.getOutput());
                        } catch (Exception e) {
                            PreferenceValue.heightadapter = 40;
                        }
                        InitView.reloadNow(activity, ((MenuView) InitView.getNow().getView()).getH());
                    }
                });
                d.getDialog(activity).show();
            }
        });
        final ButtonMenu buttonMenu7 = new ButtonMenu("Фото на нажатие", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                /*Intent in = new Intent(Intent.ACTION_PICK);
                in.setType("image/*");
                activity.startActivityForResult(in, 5);*/
                S.startForSerchFoto(activity,5);
            }
        });
        CheckBoxMenu checkBoxMenu = new CheckBoxMenu("Фото при нажатии", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu bM) {
                buttonMenu.setEnabled(!buttonMenu.isEnabled());
                buttonMenu1.setEnabled(!buttonMenu1.isEnabled());
                buttonMenu7.setEnabled(!buttonMenu7.isEnabled());
                CheckBoxMenu checkBoxMenu1 = (CheckBoxMenu) bM;
                checkBoxMenu1.setChecked(!checkBoxMenu1.isChecked());
                PreferenceValue.fotoonclick = !PreferenceValue.fotoonclick;
                S.getPermission(activity,3);
                InitView.reloadNow(activity);
            }
        },PreferenceValue.fotoonclick);
        final ButtonMenu buttonMenu8 = new ButtonMenu("Фото на задний фон", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                /*Intent in = new Intent(Intent.ACTION_PICK);
                in.setType("image/*");
                activity.startActivityForResult(in, 6);*/
                S.startForSerchFoto(activity,6);

            }
        });


        CheckBoxMenu checkBoxMenu1 = new CheckBoxMenu("Фото как задний фон", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu bM) {
                buttonMenu2.setEnabled(!buttonMenu2.isEnabled());
                buttonMenu3.setEnabled(!buttonMenu3.isEnabled());
                buttonMenu8.setEnabled(!buttonMenu8.isEnabled());
                CheckBoxMenu checkBoxMenu1 = (CheckBoxMenu) bM;
                checkBoxMenu1.setChecked(!checkBoxMenu1.isChecked());
                PreferenceValue.fotoonback = !PreferenceValue.fotoonback;
                S.getPermission(activity,4);
                InitView.reloadNow(activity);
            }
        },PreferenceValue.fotoonback);
        buttonMenu.setEnabled(!PreferenceValue.fotoonclick);
        buttonMenu1.setEnabled(!PreferenceValue.fotoonclick);
        buttonMenu2.setEnabled(!PreferenceValue.fotoonback);
        buttonMenu3.setEnabled(!PreferenceValue.fotoonback);
        buttonMenu7.setEnabled(PreferenceValue.fotoonclick);
        buttonMenu8.setEnabled(PreferenceValue.fotoonback);

        return new Menu(new ButtonMenu[]{
                buttonMenu,buttonMenu1,checkBoxMenu,buttonMenu7,buttonMenu2,buttonMenu3,checkBoxMenu1,buttonMenu8,buttonMenu4,buttonMenu5,buttonMenu6        });
    }

    @Override
    public OnClickMenu tothis(final Activity activity) {
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                InitView.initMenu(activity,getMenu(activity),InitView.getNowMenuItem());
            }
        };
    }
}
