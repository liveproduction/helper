package ru.loginov.project.preference.menu;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.S;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.CheckBoxMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.EditDialog;
import ru.loginov.project.otherclass.ListDialog;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 20.10.2016.
 */
public final class ThemeOutput extends PrefMenu {
    @Override
    public Menu getMenu(final Activity activity) {

        final ButtonMenu button1 = new ButtonMenu("Цвет заднего фона", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditDialog d = new EditDialog("Задний фон","Ок","HEX Цвета", PreferenceValue.backcolorformul);
                d.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity) {
                        PreferenceValue.backcolorformul = d.getOutput();
                        InitView.reloadNow(activity);
                    }
                });
                d.getDialog(activity).show();
            }
        });

        final ButtonMenu buttonMenu = new ButtonMenu("Размер текста", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditDialog d = new EditDialog("Размер текста","Ок","Число",String.valueOf(PreferenceValue.textsizeformul));
                d.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity) {
                        try {
                            PreferenceValue.textsizeformul = Float.valueOf(d.getOutput());
                        }catch (Exception e){
                            InitView.createSnackbarOnNow("Неправильный ввод данных");
                        }
                        InitView.reloadNow(activity);
                    }
                });
                d.getDialog(activity).show();
            }
        });

        final ButtonMenu buttonM = new ButtonMenu("Тип градиента при нажатии", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                ListDialog d = new ListDialog("Тип градиента при нажатии",new Menu(new ButtonMenu[]{
                        new ButtonMenu("Сверху вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradformul = GradientDrawable.Orientation.TOP_BOTTOM;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Снизу вверх", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradformul = GradientDrawable.Orientation.BOTTOM_TOP;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Слева направо", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradformul = GradientDrawable.Orientation.LEFT_RIGHT;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Справа налево", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradformul = GradientDrawable.Orientation.RIGHT_LEFT;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Сверху слева направо вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradformul = GradientDrawable.Orientation.TL_BR;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Снизу слева направо вверх", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradformul = GradientDrawable.Orientation.BL_TR;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Сверху справа налево вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradformul = GradientDrawable.Orientation.TR_BL;
                                InitView.reloadNow(activity);
                            }
                        }),new ButtonMenu("Снизу справа налево вверх", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        PreferenceValue.gradformul = GradientDrawable.Orientation.BR_TL;
                        InitView.reloadNow(activity);
                    }
                })
                }));
                d.getDialog(activity).show();
            }
        });
        buttonM.setEnabled(!PreferenceValue.fotoformul);
        button1.setEnabled(!PreferenceValue.fotoformul);
        final ButtonMenu button3 = new ButtonMenu("Выбрать фото", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                /*Intent in = new Intent(Intent.ACTION_PICK);
                in.setType("image/*");
                activity.startActivityForResult(in, 1);*/
                S.startForSerchFoto(activity,1);
            }
        });
        button3.setEnabled(PreferenceValue.fotoformul);

        CheckBoxMenu cb =  new CheckBoxMenu("Фото",new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l,ButtonMenu buttonMenu) {
                buttonM.setEnabled(!buttonM.isEnabled());
                button1.setEnabled(!button1.isEnabled());
                button3.setEnabled(!button3.isEnabled());
                CheckBoxMenu cb = (CheckBoxMenu) buttonMenu;
                cb.setChecked(!cb.isChecked());
                S.getPermission(activity,7);
                PreferenceValue.fotoformul = !PreferenceValue.fotoformul;
                Log.i("fotoformul",String.valueOf(PreferenceValue.fotoformul));
                InitView.reloadNow(activity);
            }
        },true);
        cb.setChecked(PreferenceValue.fotoformul);

        ButtonMenu button4 = new ButtonMenu("Цвет текста", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditDialog d = new EditDialog("Цвет текста","Ок","HEX Цвет", PreferenceValue.colorformul);
                d.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity) {
                        PreferenceValue.colorformul = d.getOutput();
                        InitView.reloadNow(activity);
                    }
                });
                d.getDialog(activity).show();
            }
        });

        return new Menu(new ButtonMenu[]{new CheckBoxMenu("Открывать как диалог со списком", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                CheckBoxMenu cb = (CheckBoxMenu) buttonMenu;
                cb.setChecked(!cb.isChecked());
                PreferenceValue.createListmenu = !PreferenceValue.createListmenu;
                InitView.reloadNow(activity);
            }
        },PreferenceValue.createListmenu),button4,buttonMenu,button1,buttonM,cb,button3

        });
    }
}
