package ru.loginov.project.preference.menu;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 18.10.2016.
 */
public class ThemeMenu  extends PrefMenu {


    @Override
    public Menu getMenu(Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Настройки меню",new ThemeMenuMenu().tothis(activity)),
                new ButtonMenu("Настройка шторки",new ThemeMenu_6().tothis(activity)),
                new ButtonMenu("Настройки формул",new ThemeOutput().tothis(activity)),
                new ButtonMenu("Настройки окон ввода",new ThemeInput().tothis(activity)),
                new ButtonMenu("Настройки диалогов",new ThemeEditDialog().tothis(activity)),
                new ButtonMenu("Настройка таблиц",new ThemeTable().tothis(activity)),

        });
    }

    @Override
    public OnClickMenu tothis(final Activity activity) {
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l,ButtonMenu buttonMenu) {
                InitView.initMenu(activity,getMenu(activity),InitView.getNowMenuItem());
            }
        };
    }
}
