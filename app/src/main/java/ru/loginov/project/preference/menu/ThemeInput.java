package ru.loginov.project.preference.menu;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.S;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.CheckBoxMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.EditDialog;
import ru.loginov.project.otherclass.ListDialog;
import ru.loginov.project.preference.Preference;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 03.11.2016.
 */
public class ThemeInput extends PrefMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        final ButtonMenu buttonMenu2 = new ButtonMenu("Цвет заднего фона", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditDialog editDialog = new EditDialog("Цвет заднего фона","Ок","HEX Цвета", PreferenceValue.backcolorinput);
                editDialog.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity) {
                        PreferenceValue.backcolorinput = editDialog.getOutput();
                        Preference.setPref(Preference.input2,PreferenceValue.backcolorinput);
                    }
                });
                editDialog.getDialog(activity).show();
            }
        });

        final ButtonMenu buttonMenu1 = new ButtonMenu("Тип градиента заднего фона", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                    ListDialog d = new ListDialog("Тип градиента",new Menu(new ButtonMenu[]{
                            new ButtonMenu("Сверху вниз", new OnClickMenu() {
                                @Override
                                public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                    PreferenceValue.gradbackinput = GradientDrawable.Orientation.TOP_BOTTOM;
                                    InitView.reloadNow(activity);
                                }
                            }),
                            new ButtonMenu("Снизу вверх", new OnClickMenu() {
                                @Override
                                public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                    PreferenceValue.gradbackinput = GradientDrawable.Orientation.BOTTOM_TOP;
                                    InitView.reloadNow(activity);
                                }
                            }),
                            new ButtonMenu("Слева направо", new OnClickMenu() {
                                @Override
                                public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                    PreferenceValue.gradbackinput = GradientDrawable.Orientation.LEFT_RIGHT;
                                    InitView.reloadNow(activity);
                                }
                            }),
                            new ButtonMenu("Справа налево", new OnClickMenu() {
                                @Override
                                public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                    PreferenceValue.gradbackinput = GradientDrawable.Orientation.RIGHT_LEFT;
                                    InitView.reloadNow(activity);
                                }
                            }),
                            new ButtonMenu("Сверху слева направо вниз", new OnClickMenu() {
                                @Override
                                public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                    PreferenceValue.gradbackinput = GradientDrawable.Orientation.TL_BR;
                                    InitView.reloadNow(activity);
                                }
                            }),
                            new ButtonMenu("Снизу слево направо вверх", new OnClickMenu() {
                                @Override
                                public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                    PreferenceValue.gradbackinput = GradientDrawable.Orientation.BL_TR;
                                    InitView.reloadNow(activity);
                                }
                            }),
                            new ButtonMenu("Сверху справа налево вниз", new OnClickMenu() {
                                @Override
                                public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                    PreferenceValue.gradbackinput = GradientDrawable.Orientation.TR_BL;
                                    InitView.reloadNow(activity);
                                }
                            }),new ButtonMenu("Снизу справа налево вверх", new OnClickMenu() {
                        @Override
                        public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                            PreferenceValue.gradbackinput = GradientDrawable.Orientation.BR_TL;
                            InitView.reloadNow(activity);
                        }
                    })
                    }));
                    d.getDialog(activity).show();
                }
        });

        final ButtonMenu foto = new ButtonMenu("Фото на задний фон", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                /*Intent in = new Intent(Intent.ACTION_PICK);
                in.setType("image/*");
                activity.startActivityForResult(in, 2);*/
                S.startForSerchFoto(activity,2);
            }
        });
        buttonMenu2.setEnabled(!PreferenceValue.havebackfotoinput);
        buttonMenu1.setEnabled(!PreferenceValue.havebackfotoinput);
        foto.setEnabled(PreferenceValue.havebackfotoinput);

        CheckBoxMenu checkBoxMenu = new CheckBoxMenu("Фото", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                buttonMenu2.setEnabled(!buttonMenu2.isEnabled());
                buttonMenu1.setEnabled(!buttonMenu1.isEnabled());
                foto.setEnabled(!foto.isEnabled());
                CheckBoxMenu cb = (CheckBoxMenu) buttonMenu;
                cb.setChecked(!cb.isChecked());
                PreferenceValue.havebackfotoinput = !PreferenceValue.havebackfotoinput;
                S.getPermission(activity, 2);
                InitView.reloadNow(activity);
            }
        }, true);
        checkBoxMenu.setChecked(PreferenceValue.havebackfotoinput);


        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Цвет вывода текста", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog editDialog = new EditDialog("Цвет вывода текста","Ок","HEX Цвет", PreferenceValue.textcolorinput);
                        editDialog.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                PreferenceValue.textcolorinput = editDialog.getOutput();
                            }
                        });
                        editDialog.getDialog(activity).show();
                    }
                }),

                new ButtonMenu("Цвет подсказок", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog editDialog = new EditDialog("Цвет подсказок","Ок","HEX Цвет", PreferenceValue.hintcolorinput);
                        editDialog.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                PreferenceValue.hintcolorinput = editDialog.getOutput();
                            }
                        });
                        editDialog.getDialog(activity).show();
                    }
                }),
                new ButtonMenu("Цвет вводимого текста", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog editDialog = new EditDialog("Цвет вводимого текста","Ок","HEX Цвет", PreferenceValue.edittextcolotinput);
                        editDialog.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                PreferenceValue.edittextcolotinput = editDialog.getOutput();
                            }
                        });
                        editDialog.getDialog(activity).show();
                    }
                }),

                new ButtonMenu("Цвет текста кнопок", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog editDialog = new EditDialog("Цвет текста кнопок","Ок","HEX Цвет", PreferenceValue.textcolorbuttominput);
                        editDialog.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                PreferenceValue.textcolorbuttominput = editDialog.getOutput();
                            }
                        });
                        editDialog.getDialog(activity).show();
                    }
                }),
                new ButtonMenu("Задний фон кнопок", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog editDialog = new EditDialog("Задний фон кнопок","Ок","HEX Цвета", PreferenceValue.backcolotbuttominput);
                        editDialog.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                PreferenceValue.backcolotbuttominput = editDialog.getOutput();
                            }
                        });
                        editDialog.getDialog(activity).show();
                    }
                }),buttonMenu2,buttonMenu1,checkBoxMenu,foto










        });
    }
}
