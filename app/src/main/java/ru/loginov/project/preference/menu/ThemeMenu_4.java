package ru.loginov.project.preference.menu;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.MenuView;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.EditDialog;
import ru.loginov.project.otherclass.ListDialog;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 20.10.2016.
 */
public class ThemeMenu_4 extends PrefMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        return new Menu(new ButtonMenu[]{
                new ButtonMenu("Цвет разделителя", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog d = new EditDialog("Цвет разделителя","Ок","HEX цвет", PreferenceValue.colormenudivide);
                        d.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity1) {
                                PreferenceValue.colormenudivide = d.getOutput();
                                InitView.reloadNow(activity, ((MenuView) InitView.getNow().getView()).getH());
                            }
                        });
                        d.getDialog(activity).show();
                    }
                }),
                new ButtonMenu("Тип градиента разделителей", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        ListDialog d = new ListDialog("Тип градиента",new Menu(new ButtonMenu[]{
                                new ButtonMenu("Сверху вниз", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.gradordivide = GradientDrawable.Orientation.TOP_BOTTOM;
                                        InitView.reloadNow(activity);
                                    }
                                }),
                                new ButtonMenu("Снизу вверх", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.gradordivide = GradientDrawable.Orientation.BOTTOM_TOP;
                                        InitView.reloadNow(activity);
                                    }
                                }),
                                new ButtonMenu("Слева направо", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.gradordivide = GradientDrawable.Orientation.LEFT_RIGHT;
                                        InitView.reloadNow(activity);
                                    }
                                }),
                                new ButtonMenu("Справа налево", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.gradordivide = GradientDrawable.Orientation.RIGHT_LEFT;
                                        InitView.reloadNow(activity);
                                    }
                                }),
                                new ButtonMenu("Сверху слева направо вниз", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.gradordivide = GradientDrawable.Orientation.TL_BR;
                                        InitView.reloadNow(activity);
                                    }
                                }),
                                new ButtonMenu("Снизу слево направо вверх", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.gradordivide = GradientDrawable.Orientation.BL_TR;
                                        InitView.reloadNow(activity);
                                    }
                                }),
                                new ButtonMenu("Сверху справа налево вниз", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.gradordivide = GradientDrawable.Orientation.TR_BL;
                                        InitView.reloadNow(activity);
                                    }
                                }),new ButtonMenu("Снизу справа налево вверх", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.gradordivide = GradientDrawable.Orientation.BR_TL;
                                InitView.reloadNow(activity);
                            }
                        })
                        }));
                        d.getDialog(activity).show();
                    }
                }),
                
                
                
                
                
                new ButtonMenu("Высота разделителя", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l,ButtonMenu buttonMenu) {
                        final  EditDialog d = new EditDialog("Высота разделителя","Ок","Целое число",String.valueOf(PreferenceValue.hmenudivide));
                        d.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity1) {
                                try {
                                    PreferenceValue.hmenudivide = Integer.valueOf(d.getOutput());
                                }catch (Exception e){
                                    PreferenceValue.hmenudivide = 2;
                                }
                                InitView.reloadNow(activity,((MenuView) InitView.getNow().getView()).getH());
                            }
                        });
                        d.getDialog(activity).show();
                    }
                }),


        });
    }
}
