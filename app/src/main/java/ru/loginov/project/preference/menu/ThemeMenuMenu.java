package ru.loginov.project.preference.menu;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 19.10.2016.
 */
public class ThemeMenuMenu extends PrefMenu {
    Activity activity = null;
    @Override
    public Menu getMenu(final Activity activity){
        this.activity = activity;



        return new Menu(new ButtonMenu[]{new ButtonMenu("Настройки кнопок",new ThemeMenu_2().tothis(activity)),
                new ButtonMenu("Настройки всего меню",new ThemeMenu_3().tothis(activity)),
                new ButtonMenu("Настройки отступов",new ThemeMenu_5().tothis(activity)),
                new ButtonMenu("Настройки разделителей",new ThemeMenu_4().tothis(activity))
        });
    }
    @Override
    public OnClickMenu tothis(final Activity activity){
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l,ButtonMenu buttonMenu) {
                InitView.initMenu(activity,getMenu(activity),InitView.getNowMenuItem());
            }
        };
    }

}
