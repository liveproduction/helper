package ru.loginov.project.preference.appsearch;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.loginov.project.S;
import ru.loginov.project.preference.PreferenceValue;

/**
 * Created by user on 16.11.2016.
 */

public class AppAdapter extends BaseAdapter {
    List<Item> items = new ArrayList<>();
    Context context;
    public AppAdapter(Context context,List<String> names,List<String> packege,List<Drawable> icons){
        this.context = context;
        for (int i = 0;i<names.size();i++){
            items.add(new Item(packege.get(i),names.get(i),icons.get(i)));
        }
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        RelativeLayout rl = new RelativeLayout(context);
        rl.setId(777);
        ImageView iv = new ImageView(context);
        TextView tv = new TextView(context);
        iv.setImageBitmap(S.drawableToBitmap(items.get(i).getIcon(),72,72));
        tv.setText(items.get(i).getName());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_VERTICAL,777);
        iv.setLayoutParams(layoutParams);
        iv.setId(6666);
        rl.addView(iv);
        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams1.addRule(RelativeLayout.RIGHT_OF,6666);
        layoutParams1.addRule(RelativeLayout.CENTER_VERTICAL,777);
        tv.setTextColor(Color.parseColor(PreferenceValue.colormenutext));
        tv.setLayoutParams(layoutParams1);
        rl.addView(tv);
        return rl;
    }

    public class Item{
        String pakage;
        String name;
        Drawable icon;

        Item(String pakage,String name,Drawable icon){
            this.pakage = pakage;
            this.name = name;
            this.icon = icon;
        }

        public Drawable getIcon() {
            return icon;
        }

        public String getName() {
            return name;
        }

        public String getPakage() {
            return pakage;
        }
    }
}
