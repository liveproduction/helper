package ru.loginov.project.preference.menu;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.MainActivity;
import ru.loginov.project.S;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.CheckBoxMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.MenuView;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.EditDialog;
import ru.loginov.project.otherclass.ListDialog;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 20.10.2016.
 */
public class ThemeMenu_3 extends PrefMenu{
    @Override
    public Menu getMenu(final Activity activity) {
        final ButtonMenu b = new ButtonMenu("Фото на задний фон", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                /*Intent in = new Intent(Intent.ACTION_PICK);
                in.setType("image/*");
                activity.startActivityForResult(in, 0);*/
                S.startForSerchFoto(activity,0);
            }
        });
        b.setEnabled(PreferenceValue.backdrawmenuboolean);
        final ButtonMenu buttom1 = new ButtonMenu("Задний фон", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l,ButtonMenu buttonMenu) {
                final EditDialog d = new EditDialog("Цвет текста","Ок","HEX цвет",PreferenceValue.backcolorm);
                d.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity) {
                        PreferenceValue.backcolorm = d.getOutput();
                        InitView.reloadNow(activity,((MenuView) InitView.getNow().getView()).getH());
                    }
                });
                d.getDialog(activity).show();
            }
        });
        final ButtonMenu button2 = new ButtonMenu("Тип градиента заднего фона", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                ListDialog d = new ListDialog("Тип градиента",new Menu(new ButtonMenu[]{
                        new ButtonMenu("Сверху вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.getGradorientationmenu = GradientDrawable.Orientation.TOP_BOTTOM;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Снизу вверх", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.getGradorientationmenu = GradientDrawable.Orientation.BOTTOM_TOP;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Слева направо", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.getGradorientationmenu = GradientDrawable.Orientation.LEFT_RIGHT;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Справа налево", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.getGradorientationmenu = GradientDrawable.Orientation.RIGHT_LEFT;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Сверху слева направо вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.getGradorientationmenu = GradientDrawable.Orientation.TL_BR;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Снизу слево направо вверх", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.getGradorientationmenu = GradientDrawable.Orientation.BL_TR;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Сверху справа налево вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.getGradorientationmenu = GradientDrawable.Orientation.TR_BL;
                                InitView.reloadNow(activity);
                            }
                        }),new ButtonMenu("Снизу справа налево вверх", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        PreferenceValue.getGradorientationmenu = GradientDrawable.Orientation.BR_TL;
                        InitView.reloadNow(activity);
                    }
                })
                }));
                d.getDialog(activity).show();
            }
        });
        buttom1.setEnabled(!PreferenceValue.backdrawmenuboolean);
        button2.setEnabled(!PreferenceValue.backdrawmenuboolean);
        CheckBoxMenu cb =  new CheckBoxMenu("Фото", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l,ButtonMenu buttonMenu) {
                b.setEnabled(!b.isEnabled());
                buttom1.setEnabled(!buttom1.isEnabled());
                button2.setEnabled(!button2.isEnabled());
                CheckBoxMenu cb = (CheckBoxMenu) buttonMenu;
                cb.setChecked(!cb.isChecked());
                PreferenceValue.backdrawmenuboolean = !PreferenceValue.backdrawmenuboolean;
                if (PreferenceValue.backdrawmenuboolean){
                   S.getPermission(activity,MainActivity.THEMEMENU_3);
                }
                InitView.reloadNow(activity, ((MenuView) InitView.getNow().getView()).getH());
            }
        },true);
        cb.setChecked(PreferenceValue.backdrawmenuboolean);

        return new Menu(new ButtonMenu[]{
                buttom1,button2,cb,b
        });
    }
}
