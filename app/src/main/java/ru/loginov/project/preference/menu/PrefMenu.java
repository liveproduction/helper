package ru.loginov.project.preference.menu;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 19.10.2016.
 */
public abstract class PrefMenu {

    public abstract Menu getMenu(final Activity activity);

    public OnClickMenu tothis(final Activity activity){
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                InitView.initMenu(activity,getMenu(activity),InitView.getNowMenuItem());
            }
        };
    };

}
