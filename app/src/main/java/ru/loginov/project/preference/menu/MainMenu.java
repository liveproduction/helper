package ru.loginov.project.preference.menu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.CheckBoxMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.MenuView;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.EditDialog;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.preference.appsearch.AppAdapter;
import ru.loginov.project.workspace.InitView;
import ru.loginov.project.workspace.MainView;

/**
 * Created by user on 18.10.2016.
 */
public class MainMenu extends PrefMenu {
    @Override
    public Menu getMenu(final Activity activity){
        final ButtonMenu buttonMenu2 = new ButtonMenu("Чуствительность свайпа", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditDialog dialog = new EditDialog("Чувствительность свайпа","Ок","Минимальная длина (Любое число)",String.valueOf(PreferenceValue.lenghtnavbar));
                dialog.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity) {
                        try{
                            Float f = Float.valueOf(dialog.getOutput());
                            PreferenceValue.lenghtnavbar = f;
                        }catch (Exception e){
                            InitView.createSnackbarOnNow("Неправиоьно введены данные");
                        }
                    }
                });
                dialog.getDialog(activity).show();
            }
        });

        final ButtonMenu buttonMenu1 = new ButtonMenu("Приложение при запуске", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                Thread th = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final ProgressDialog[] pd = new ProgressDialog[1];
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pd[0] = ProgressDialog.show(activity,"","Загрузка");
                            }
                        });
                        final PackageManager pm = activity.getPackageManager();
                        List<PackageInfo> packs = pm.getInstalledPackages(0);
                        List<String> name = new ArrayList<>();
                        List<String> pack = new ArrayList<>();
                        List<Drawable> drawables = new ArrayList<>();
                        for (int i = 0;i<packs.size();i++){
                            name.add(String.valueOf(packs.get(i).applicationInfo.loadLabel(pm)));
                            pack.add(String.valueOf(packs.get(i).packageName));
                            drawables.add(packs.get(i).applicationInfo.loadIcon(pm));
                        }
                        ListView lv = new ListView(activity);
                         final AppAdapter appAdapter = new AppAdapter(activity,name,pack,drawables);
                        lv.setAdapter(appAdapter);
                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                PreferenceValue.packintent =((AppAdapter.Item) appAdapter.getItem(i)).getPakage();
                                InitView.onBackPress(activity);
                            }
                        });
                        InitView.initView(activity,new MainView(lv),InitView.getNowMenuItem());
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pd[0].cancel();
                                pd[0].dismiss();
                            }
                        });
                    }
                });
                th.start();
            }
        });
        buttonMenu2.setEnabled(true);
        buttonMenu1.setEnabled(PreferenceValue.startintent);

        final CheckBoxMenu checkBoxMenu = new CheckBoxMenu("Открывать приложение по свайпу", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                buttonMenu1.setEnabled(!buttonMenu1.isEnabled());
                if (buttonMenu.isCheckBox()) {
                    CheckBoxMenu checkBoxMenu1 = (CheckBoxMenu) buttonMenu;
                    checkBoxMenu1.setChecked(!checkBoxMenu1.isChecked());
                }
                PreferenceValue.startintent =!PreferenceValue.startintent;
                InitView.reloadNow(activity,((MenuView) InitView.getNow().getView()).getH());
            }
        },PreferenceValue.startintent);



        return new Menu(new ButtonMenu[]{new ButtonMenu("Тема",new ThemeMenu().tothis(activity)),
        new ButtonMenu("Свайп", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                Menu m = new Menu(new ButtonMenu[]{
                        buttonMenu2,checkBoxMenu,buttonMenu1
                });
                InitView.initMenu(activity,m,InitView.getNowMenuItem());
            }
        })


        });
    }
    @Override
    public OnClickMenu tothis(final Activity activity){
        return new OnClickMenu(){
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l,ButtonMenu buttonMenu) {
                InitView.initMenu(activity,getMenu(activity),InitView.getNowMenuItem());
            }
        };
    }

}
