package ru.loginov.project.preference.menu;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.CheckBoxMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.EditDialog;
import ru.loginov.project.otherclass.ListDialog;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 26.10.2016.
 */
public class ThemeTable extends PrefMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        return new Menu(new ButtonMenu[]{
                new CheckBoxMenu("Открытие меню клавишей назад", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        PreferenceValue.tablemenuonback = !PreferenceValue.tablemenuonback;
                        CheckBoxMenu checkBoxMenu = (CheckBoxMenu) buttonMenu;
                        checkBoxMenu.setChecked(!checkBoxMenu.isChecked());
                        InitView.reloadNow(activity);
                    }
                },PreferenceValue.tablemenuonback),


                new ButtonMenu("Размер текста", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog d = new EditDialog("Размер текста","Ок","Целое число",String.valueOf(PreferenceValue.tableheighttext));
                        d.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                try {
                                    PreferenceValue.tableheighttext = Integer.valueOf(d.getOutput());
                                }catch (Exception e){
                                    InitView.createSnackbarOnNow("Неправильно указан размер текста");
                                }
                            }
                        });
                        d.getDialog(activity).show();
                    }
                }),
                new ButtonMenu("Цвет имён (Первый столбец)", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog d = new EditDialog("Цвет имён","Ок","HEX цвет",PreferenceValue.tablecolortextname);
                        d.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                PreferenceValue.tablecolortextname = d.getOutput();
                            }
                        });
                        d.getDialog(activity).show();
                    }
                }),
                new ButtonMenu("Цвет значений (Второй столбец)", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog d = new EditDialog("Цвет значений","Ок","HEX цвет",PreferenceValue.tablecolortextvalue);
                        d.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                PreferenceValue.tablecolortextvalue = d.getOutput();
                            }
                        });
                        d.getDialog(activity).show();
                    }
                }),
                new ButtonMenu("Цвет заднего фона (Всей таблицы)", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog d = new EditDialog("Цвет фона 1","Ок","HEX цвета", PreferenceValue.tablebackcolorall);
                        d.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                PreferenceValue.tablebackcolorall = d.getOutput();
                            }
                        });
                        d.getDialog(activity).show();
                    }
                }),
       new ButtonMenu("Тип градиента заднего фона (Всей таблицы)", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                ListDialog d = new ListDialog("Тип градиента Фона 1", new Menu(new ButtonMenu[]{
                        new ButtonMenu("Сверху вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.tablegradleback = GradientDrawable.Orientation.TOP_BOTTOM;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Снизу вверх", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.tablegradleback = GradientDrawable.Orientation.BOTTOM_TOP;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Слева направо", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.tablegradleback = GradientDrawable.Orientation.LEFT_RIGHT;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Справа налево", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.tablegradleback = GradientDrawable.Orientation.RIGHT_LEFT;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Сверху слева направо вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.tablegradleback = GradientDrawable.Orientation.TL_BR;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Снизу слева направо вверх", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.tablegradleback = GradientDrawable.Orientation.BL_TR;
                                InitView.reloadNow(activity);
                            }
                        }),
                        new ButtonMenu("Сверху справа налево вниз", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.tablegradleback = GradientDrawable.Orientation.TR_BL;
                                InitView.reloadNow(activity);
                            }
                        }), new ButtonMenu("Снизу справа налево вверх", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        PreferenceValue.tablegradleback = GradientDrawable.Orientation.BR_TL;
                        InitView.reloadNow(activity);
                    }
                })
                }));
                d.getDialog(activity).show();
            }
        }),
                new ButtonMenu("Цвет заднего фона (Отдельного элемента)", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog d = new EditDialog("Цвет фона 2","Ок","HEX цвета",PreferenceValue.tablebackcoloritems);
                        d.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                PreferenceValue.tablebackcoloritems = d.getOutput();
                            }
                        });
                        d.getDialog(activity).show();
                    }
                }),
                new ButtonMenu("Тип градиента заднего фона (Отдельного элемента)", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        ListDialog d = new ListDialog("Тип градиента Фона 2", new Menu(new ButtonMenu[]{
                                new ButtonMenu("Сверху вниз", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.tablegradleitemback = GradientDrawable.Orientation.TOP_BOTTOM;
                                        InitView.reloadNow(activity);
                                    }
                                }),
                                new ButtonMenu("Снизу вверх", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.tablegradleitemback = GradientDrawable.Orientation.BOTTOM_TOP;
                                        InitView.reloadNow(activity);
                                    }
                                }),
                                new ButtonMenu("Слева направо", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.tablegradleitemback = GradientDrawable.Orientation.LEFT_RIGHT;
                                        InitView.reloadNow(activity);
                                    }
                                }),
                                new ButtonMenu("Справа налево", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.tablegradleitemback = GradientDrawable.Orientation.RIGHT_LEFT;
                                        InitView.reloadNow(activity);
                                    }
                                }),
                                new ButtonMenu("Сверху слева направо вниз", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.tablegradleitemback = GradientDrawable.Orientation.TL_BR;
                                        InitView.reloadNow(activity);
                                    }
                                }),
                                new ButtonMenu("Снизу слева направо вверх", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.tablegradleitemback = GradientDrawable.Orientation.BL_TR;
                                        InitView.reloadNow(activity);
                                    }
                                }),
                                new ButtonMenu("Сверху справа налево вниз", new OnClickMenu() {
                                    @Override
                                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                        PreferenceValue.tablegradleitemback = GradientDrawable.Orientation.TR_BL;
                                        InitView.reloadNow(activity);
                                    }
                                }), new ButtonMenu("Снизу справа налево вверх", new OnClickMenu() {
                            @Override
                            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                                PreferenceValue.tablegradleitemback = GradientDrawable.Orientation.BR_TL;
                                InitView.reloadNow(activity);
                            }
                        })
                        }));
                        d.getDialog(activity).show();
                    }
                }),
                new ButtonMenu("Цвет разделителя", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog d = new EditDialog("Цвет разделителя","Ок","HEX цвета",PreferenceValue.tablecolordivide);
                        d.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                PreferenceValue.tablecolordivide = d.getOutput();
                            }
                        });
                        d.getDialog(activity).show();
                    }
                })
        });
    }
}
