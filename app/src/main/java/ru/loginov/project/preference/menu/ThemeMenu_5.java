package ru.loginov.project.preference.menu;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.EditDialog;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 03.11.2016.
 */
public class ThemeMenu_5 extends PrefMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        return new Menu(new ButtonMenu[]{new ButtonMenu("Верхний отступ", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                final EditDialog editDialog = new EditDialog("Верхний отступ","Ок","Целое число",String.valueOf(PreferenceValue.menutop));
                editDialog.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity) {
                        try{
                            int g = Integer.valueOf(editDialog.getOutput());
                            PreferenceValue.menutop = g;
                            InitView.reloadNow(activity);
                        }catch (Exception e){
                            InitView.createSnackbarOnNow("Неправильно указанно число");
                        }
                    }
                }
                );
                editDialog.getDialog(activity).show();
            }
        }),


                new ButtonMenu("Нижний отступ", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog editDialog = new EditDialog("Нижний отступ","Ок","Целое число",String.valueOf(PreferenceValue.menubutton));
                        editDialog.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                try{
                                    int g = Integer.valueOf(editDialog.getOutput());
                                    PreferenceValue.menubutton = g;
                                    InitView.reloadNow(activity);
                                }catch (Exception e){
                                    InitView.createSnackbarOnNow("Неправильно указанно число");
                                }
                            }
                        });
                        editDialog.getDialog(activity).show();
                    }
                }),

                new ButtonMenu("Отступ слева", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog editDialog = new EditDialog("Отступ слева","Ок","Целое число",String.valueOf(PreferenceValue.menuleft));
                        editDialog.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                try{
                                    int g = Integer.valueOf(editDialog.getOutput());
                                    PreferenceValue.menuleft = g;
                                    InitView.reloadNow(activity);
                                }catch (Exception e){
                                    InitView.createSnackbarOnNow("Неправильно указанно число");
                                }
                            }
                        });
                        editDialog.getDialog(activity).show();
                    }
                }),

                new ButtonMenu("Отступ справа", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog editDialog = new EditDialog("Отступ справа","Ок","Целое число",String.valueOf(PreferenceValue.menuright));
                        editDialog.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                try{
                                    int g = Integer.valueOf(editDialog.getOutput());
                                    PreferenceValue.menuright = g;
                                    InitView.reloadNow(activity);
                                }catch (Exception e){
                                    InitView.createSnackbarOnNow("Неправильно указанно число");
                                }
                            }
                        });
                        editDialog.getDialog(activity).show();
                    }
                })






        });
    }
}
