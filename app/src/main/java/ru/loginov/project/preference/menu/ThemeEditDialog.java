package ru.loginov.project.preference.menu;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;

import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.EditDialog;
import ru.loginov.project.preference.Preference;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 19.10.2016.
 */
public class ThemeEditDialog extends PrefMenu {
    @Override
    public Menu getMenu(final Activity activity) {
        return new Menu(new ButtonMenu[]{
        new ButtonMenu("Цвет кнопки", new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l,ButtonMenu buttonMenu) {
                final EditDialog d = new EditDialog("Цвет кнопки","Ок","HEX цвет", PreferenceValue.coloredittextbuttom);
                d.setRunnable(new EditDialog.onEnded() {
                    @Override
                    public void run(Activity activity1) {
                        Preference.setPref(Preference.color4,d.getOutput());
                        InitView.reloadNow(activity);
                    }
                });
                d.getDialog(activity).show();

            }
        }),
                new ButtonMenu("Размер формул", new OnClickMenu() {
                    @Override
                    public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                        final EditDialog editDialog = new EditDialog("Размер формул","Ок","Число",String.valueOf(PreferenceValue.sizedialogformultext));
                        editDialog.setRunnable(new EditDialog.onEnded() {
                            @Override
                            public void run(Activity activity) {
                                try {
                                    PreferenceValue.sizedialogformultext = Float.valueOf(editDialog.getOutput());
                                }finally {

                                }
                            }
                        });
                        editDialog.getDialog(activity).show();
                    }
                })



        });
    }

    @Override
    public OnClickMenu tothis(final Activity activity) {
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l,ButtonMenu buttonMenu) {
                InitView.initMenu(activity,getMenu(activity),InitView.getNowMenuItem());
            }
        };
    }
}
