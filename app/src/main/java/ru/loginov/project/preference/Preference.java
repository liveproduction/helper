package ru.loginov.project.preference;

import android.app.Activity;
import android.util.Log;
import android.view.MenuItem;

import ru.loginov.project.DataBase.DB;
import ru.loginov.project.preference.menu.MainMenu;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 17.10.2016.
 */
public class Preference {
    public static final String color1 = "colormenuadapter";
    public static final String color2 = "backcolormenuadapter";
    public static final String color3 = "colormenutext";
    public static final String colord1 = "colormenudivide";
    public static final String hdiv1 = "hmenudivide";
    public static final String color4 = "editdialogbuttomcolor";
    public static final String color5 = "backcolormenu";
    public static final String foto1 = "backdrawfotomenu";
    public static final String foto2 = "backdrawfotoformul"; //Фото в формулах
    public static final String boolean1 = "backrawfotomenuboolean";
    public static final String boolean2 = "backdrawfotoformulboolean"; //Ставить ли фото в формулу
    public static final String h2 = "heightmenuadapter";
    public static final String or1 = "orientationone";
    public static final String or2 = "orientationtwo";
    public static final String or3 = "orientationthree";
    public static final String or4 = "orientationfore";
    public static final String or5 = "orientationfive";
    public static final String float1 = "textsizeformul";
    public static final String h3 = "heighttextinmenu"; //Размер текста в меню
    public static final String color6 = "colorformulsss"; //Цвет текта в формулах
    public static final String color7 = "backgroundcolorformul"; // Задний фон в формулах

    //Table

    public static final String color8 = "tablecolor1";
    public static final String color9 = "tablecolor2";
    public static final String color10 = "tablecolor3";
    public static final String color11 = "tablecolor4";
    public static final String color12 = "tablecolor5";
    public static final String h4 = "tableheight1";
    public static final String boolean6 = "tableonbackpressmenuonlongclick";
    public static final String or6 = "tableor6";
    public static final String or7 = "tableor7";

    //Menu
    public static final String boolean7 = "kfdsf";
    public static final String boolean8 = "kdkdk";
    public static final String foto5 = "fkfjjsasd";
    public static final String foto6 = "okddjdjd";
    //Отступы
    public static final String menutop = "menuottop";
    public static final String menubutton = "menuotbutton";
    public static final String menuleft = "menuotleft";
    public static final String menuright = "menuotright";
    //Listmenu
    public static final String boolean3 = "listormenu";//Список или меню
    //InputProcess

    public static final String input1 = "inputtextcoloroutput";//Цвет вывода
    public static final String input2 = "inputbackcolor";//Задний фон
    public static final String input3 = "inputhavebackfoto";//Ставить ли картинку на задний фон
    public static final String input4 = "inputuribackfoto";//Картинка на задний фон
    public static final String input5 = "inputhintcolor";//Цвет подсказок в EditTexts
    public static final String input6 = "inputtextcoloredittext";//Цвет текста при наборе в EditTexts
    public static final String input7 = "inputbackcolorbutton";//Задний фон кнопок
    public static final String input8 = "inputtextcolorbutton";//Цвет текста кнопок
    public static final String input9 = "inputgradbackcolor";//Градент заднего фона


    //Toasts
    public static final String color13 = "toastscolortext";

    //Nav bar
    public static final String l1 = "navbarlenght";
    public static final String color14 = "navcolorbackheader";
    public static final String color15 = "navbackcolormenu";
    public static final String boolean4 = "havefotonavbarheader";
    public static final String boolean5 = "havefotonavmenu";
    public static final String foto3 = "fotonavbarheader";
    public static final String foto4 = "fotonavbarmenu";
    public static final String color16 = "colortextnavbar";

    //Swipe
    public static final String swipe1 = "booleanintent";
    public static final String swipe2 = "packtointent";

    //Dialog
    public static final String size22 = "kdfhjdhf";


    public static void onStart(Activity activity, MenuItem m) {
        InitView.initMenu(activity,new MainMenu().getMenu(activity).setTitle("Настройки"),m);
    }

    public static void setPref(String key, String value){
        DB.setString(PreferenceValue.dbt,key,value);
    }

    public static String getPref(String key,String value){//Получение строки из базы данных из таблицы data
        String ret = DB.getString(PreferenceValue.dbt,key);
        if (ret == null){
            DB.setString(PreferenceValue.dbt,key,value);
            ret = value;
        }
        Log.i("LIVE: helper",key+" : "+ ret);
        return ret;
    }

    public static void setPref(String key,int value){
        DB.setInteger(PreferenceValue.dbt,key,value);
    }

    public static int getPref(String key,int value){//Получение строки из базы данных из таблицы data
        String ret = DB.getString(PreferenceValue.dbt,key);
        if (ret == null || ret.equals("")){
            setPref(key,value);
            return value;
        }
        Log.i("LIVE: helper",key+" : "+ ret);
        return Integer.valueOf(ret);
    }

    public static float getPref(String key,float value){
        String ret = DB.getString(PreferenceValue.dbt,key);
        if (ret == null || ret.equals("")){
            setPref(key,value);
            return value;
        }
        Log.i("LIVE: helper",key+" : "+ ret);
        return Float.valueOf(ret);
    }

    public static void setPref(String key,float value){
        DB.setFloat(PreferenceValue.dbt,key,value);
    }

    public static boolean getPref(String key,boolean value){
        return Boolean.valueOf(getPref(key,String.valueOf(value)));
    }

    public static void setPref(String key,boolean value){
        DB.setBooolean(PreferenceValue.dbt,key,value);
    }
}
