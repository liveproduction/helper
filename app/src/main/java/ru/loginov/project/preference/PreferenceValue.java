package ru.loginov.project.preference;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;

/**
 * Created by user on 19.10.2016.
 */
public class PreferenceValue {

    public static final String dbt = "data";

    public static String colorontouchmenu = "#880000FF";//Цвет при нажатии на пункт меню
    public static String backcolormenu = "#00FFFFFF";//Задний фон кнопок меню
    public static String colormenutext = "#000000";//Цвет текста в меню
    public static String colormenudivide = "#000000";//Цвет разделителей в меню
    public static int hmenudivide = 2;//Высота разделителя в меню
    public static String coloredittextbuttom = "#FFFFFFFF";//Цвет кнопки в диалогах
    public static String backcolorm = "#FFFFFF";//Задний фон всего меню
    public static Drawable backdrawmenu = null;//Фото на задний фон
    public static boolean backdrawmenuboolean = false;//Устанавливать ли фото на задний фон
    public static int heightadapter = 90;//Высота кнопок в меню
    public static GradientDrawable.Orientation gradorientationadapter = GradientDrawable.Orientation.TL_BR;
    public static GradientDrawable.Orientation getGradorientationmenu = GradientDrawable.Orientation.TL_BR;
    public static GradientDrawable.Orientation gradordivide = GradientDrawable.Orientation.TL_BR;
    public static GradientDrawable.Orientation gradonclickadapter = GradientDrawable.Orientation.TL_BR;
    public static int htextadapter = 10;//высота текста в меню
    public static String colorformul = "#FFFFFF"; //Цвет текста формулы
    public static String backcolorformul = "#000000"; //Задний фон формулы
    public static float textsizeformul = 20F;//Размер шрифта в формуле
    public static GradientDrawable.Orientation gradformul = GradientDrawable.Orientation.TL_BR; //Тип градиента в формуле
    public static boolean fotoformul = false; //Задний фон как фото. Output
    public static Drawable backdrawformul = null; //Фото для заднего фона. Output
    //Table

    public static String tablecolortextname = "#000000"; //Цвет названий
    public static String tablecolortextvalue = "#000000"; //Цвет значений
    public static String tablebackcolorall = "#FFFFFF"; //Цвет заднего фона родителя
    public static String tablebackcoloritems = "#00000000"; //Цвет заднего фона items
    public static String tablecolordivide = "#000000"; //Цвет разделителей
    public static int tableheighttext = 20; //Высота текста
    public static boolean tablemenuonback = false;//Меню клавишей назад
    public static GradientDrawable.Orientation tablegradleback = GradientDrawable.Orientation.TL_BR;
    public static GradientDrawable.Orientation tablegradleitemback = GradientDrawable.Orientation.TL_BR;

    //Menu
    public static boolean fotoonclick = false;
    public static Drawable fotoclick = null;
    public static boolean fotoonback = false;
    public static Drawable fotoback = null;
    public static Drawable fotoclickh = null;
    public static Drawable fotobackh = null;
    //Отступы
    public static int menutop = 0;
    public static int menubutton = 0;
    public static int menuleft = 0;
    public static int menuright = 0;
    //ListMenu
    public static boolean createListmenu = false;


    //InputProcess

    public static String textcolorinput = "#000000";//Цвет вывода
    public static String backcolorinput = "#FFFFFF";//Цвет заднего фона
    public static boolean havebackfotoinput = false;//Ставить ли картинку на задний фон
    public static Drawable backfotoinput = null;//Картинка заднего фона
    public static String hintcolorinput = "#888888";//Цвет подсказок в EditTexts
    public static String edittextcolotinput = "#000000";//Цвет текста прри наборе в EditTexts
    public static String backcolotbuttominput = "#aa888888";//Задний фон кнопок
    public static String textcolorbuttominput = "#000000";//Цвет текста кнопок
    public static GradientDrawable.Orientation gradbackinput = GradientDrawable.Orientation.BL_TR;//Ориентация

    //Toasts

    public static String colortexttoast = "#FF0000";

    //NavBar
    public static float lenghtnavbar = 0F;
    public static String backcolorheadernavbar = "#81C784,#4CAF50,#2E7D32";
    public static String backcolornavmenu = "#FFFFFF";
    public static boolean havefotohead = false;
    public static boolean havefotomenu = false;
    public static Drawable backfotoheadernavbar = null;
    public static Drawable backfotonavmenu = null;
    public static String textcolornavbar = "#000000";



    //Svipe

    public static boolean startintent = false;
    public static String packintent = "";

    //Dialog
    public static float sizedialogformultext = 20F;


}
