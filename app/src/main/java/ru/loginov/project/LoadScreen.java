package ru.loginov.project;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import maximsblog.blogspot.com.jlatexmath.core.AjLatexMath;
import ru.loginov.project.DataBase.DB;

/**
 * Created by Loginov Ilya on 07.10.2017.
 * Экран загрузки.
 */

public class LoadScreen extends Activity {

    public static Activity l_0;
    private static long time;//Время загрузки

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loadscreen);//Вывод на экран сообщения о загрузке
        DB.setActivity(this);//Подгрузка класса базы данных
        l_0 = this;
        time = System.currentTimeMillis();
        Thread th = new Thread(new Runnable() {//Поток загрузки
            @Override
            public void run() {
                AjLatexMath.init(LoadScreen.l_0);//Подгрузка библиотеки

                DB.createDB();//Создание базы если её нету

                S.initDataBase(LoadScreen.l_0);//Подгрузка настроек

                //Обработка времени, чтобы не было быстрой смены интерфейса

                long h = System.currentTimeMillis() - LoadScreen.time;
                if (h < 100){
                    try {
                        Thread.sleep(h);
                    } catch (InterruptedException e) {

                    }
                }
                LoadScreen.l_0.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(LoadScreen.l_0,MainActivity.class);
                        LoadScreen.l_0.startActivity(intent);
                    }
                });
            }
        });
        th.start();//Загрузка в потоке
    }

}
