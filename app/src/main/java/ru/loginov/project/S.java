package ru.loginov.project;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import maximsblog.blogspot.com.jlatexmath.core.Insets;
import maximsblog.blogspot.com.jlatexmath.core.TeXConstants;
import maximsblog.blogspot.com.jlatexmath.core.TeXFormula;
import maximsblog.blogspot.com.jlatexmath.core.TeXIcon;
import ru.loginov.project.calculator.Calculator;
import ru.loginov.project.mainmenu.mathmenu.YEdit;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.ListDialog;
import ru.loginov.project.otherclass.MyBigInteger;
import ru.loginov.project.otherclass.MyToast;
import ru.loginov.project.preference.Preference;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.searchpack.SearchItem;
import ru.loginov.project.table.TableItem;
import ru.loginov.project.workspace.InitView;
import ru.loginov.project.workspace.MainView;

/**
 * Created by Loginov Ilia on 17.10.2016.
 * Главная библиотека всего приложения
 */
public final class S {
    public static RelativeLayout workspace = null;//Не такая уж и нужная переменная.
    // Init method

    /**
     * Инициализация всего в потоке.
     * Запуск приложения и подгрузка всего нужного
     * @param dw      Главная форма
     * @param context MainActivity
     */
    public static void initHelper(DrawerLayout dw, final Activity context) {
        workspace = (RelativeLayout) dw.findViewById(R.id.superparent); //Главный родитель всех окон. Если нужно всё удалить, то очищаем его
        if (workspace != null) { //Проверка на существование
            Log.i("LIVeHelper", "workspace not null");
        }
        new InitView(context); //Инициализация класса работы с окнами
        MyToast.init(context); //Инициализация класса сообщений для пользователя
        initMainMenu(context); //Инициализация главного меню
        InitView.reloadNavBar(context); //Перезагрузка шторки
    }

    /**
     * Создание главного меню
     * @param activity
     */
    public static void initMainMenu(Activity activity) {
        MainView mainmenu = new MainView(new ru.loginov.project.mainmenu.MainMenu().getMenu(activity).getView(activity), null, null, new MainView.OnInitMenu() {
            @Override
            public void onOpenMenu(Activity ac) {
                DrawerLayout drawer = (DrawerLayout) ac.findViewById(R.id.drawer_layout);
                if (!drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.openDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.START);
                }
            }
        }); //Получение меню и обработка открытия шторки и её закрытия. Заворачиваем всё в оболочку
        InitView.initView(activity, mainmenu, null); //Выводим на экран меню
    }

    /**
     * Инициализация базы данных. Создание и заполнение переменных. При первом запуске может лагать, потому что половина инструкций выполняются в UI потоке
     * @param activity
     */
    public static void initDataBase(Activity activity) {
        PreferenceValue.colorontouchmenu = Preference.getPref(Preference.color1, "#880000FF");
        PreferenceValue.backcolormenu = Preference.getPref(Preference.color2, "#00FFFFFF");
        PreferenceValue.colormenutext = Preference.getPref(Preference.color3, "#000000");
        PreferenceValue.colormenudivide = Preference.getPref(Preference.colord1, "#000000");
        PreferenceValue.hmenudivide = Preference.getPref(Preference.hdiv1, 2);
        PreferenceValue.backcolorm = Preference.getPref(Preference.color5, "#00FFFFFF");
        PreferenceValue.heightadapter = Preference.getPref(Preference.h2, 90);
        PreferenceValue.backdrawmenuboolean = Preference.getPref(Preference.boolean1, false);
        PreferenceValue.gradorientationadapter = GradientDrawable.Orientation.valueOf(Preference.getPref(Preference.or1, GradientDrawable.Orientation.BL_TR.name()));
        PreferenceValue.getGradorientationmenu = GradientDrawable.Orientation.valueOf(Preference.getPref(Preference.or2, GradientDrawable.Orientation.BL_TR.name()));
        PreferenceValue.gradordivide = GradientDrawable.Orientation.valueOf(Preference.getPref(Preference.or3, GradientDrawable.Orientation.BL_TR.name()));
        PreferenceValue.gradonclickadapter = GradientDrawable.Orientation.valueOf(Preference.getPref(Preference.or4, GradientDrawable.Orientation.BL_TR.name()));
        PreferenceValue.htextadapter = Preference.getPref(Preference.h3, 20);
        if (PreferenceValue.heightadapter < 1) {
            PreferenceValue.heightadapter = 90;
        }
        if (PreferenceValue.htextadapter < 1) {
            PreferenceValue.htextadapter = 20;
        }
        String foto1 = Preference.getPref(Preference.foto1, "");
        if (!foto1.equals("")) {
            PreferenceValue.backdrawmenu = new BitmapDrawable(foto1);
        }


        //Menu
        PreferenceValue.fotoonclick = Preference.getPref(Preference.boolean7, false);
        PreferenceValue.fotoonback = Preference.getPref(Preference.boolean8, false);
        String fotoclick = Preference.getPref(Preference.foto5, "");
        String fotoback = Preference.getPref(Preference.foto6, "");
        PreferenceValue.menutop = Preference.getPref(Preference.menutop, 0);
        PreferenceValue.menubutton = Preference.getPref(Preference.menubutton, 0);
        PreferenceValue.menuleft = Preference.getPref(Preference.menuleft, 0);
        PreferenceValue.menuright = Preference.getPref(Preference.menuright, 0);
        boolean have1 = false;
        boolean have2 = false;
        if (PreferenceValue.fotoonclick) {

            if (!fotoclick.equals("")) {
                PreferenceValue.fotoclick = new BitmapDrawable(fotoclick);
            }
        } else {
            have1 = true;
        }
        if (PreferenceValue.fotoonback) {
            if (!fotoback.equals("")) {
                PreferenceValue.fotoback = new BitmapDrawable(fotoback);
            }
        } else {
            have2 = true;
        }
        //Граница

        //onload = true;

        //Граница
        S.getRealDrawable(activity,PreferenceValue.heightadapter,true,true);
        if (have1) {
            if (!fotoclick.equals("")) {
                PreferenceValue.fotoclick = new BitmapDrawable(fotoclick);
            }
        }
        if (have2) {
            if (!fotoback.equals("")) {
                PreferenceValue.fotoback = new BitmapDrawable(fotoback);
            }
        }
        PreferenceValue.coloredittextbuttom = Preference.getPref(Preference.color4, "#FFFFFFFF");
        //ListMenu
        PreferenceValue.createListmenu = Preference.getPref(Preference.boolean3, false);
        //Table
        PreferenceValue.tablecolortextname = Preference.getPref(Preference.color8, "#000000");
        PreferenceValue.tablecolortextvalue = Preference.getPref(Preference.color9, "#000000");
        PreferenceValue.tablebackcolorall = Preference.getPref(Preference.color10, "#FFFFFF");
        PreferenceValue.tablebackcoloritems = Preference.getPref(Preference.color11, "#00000000");
        PreferenceValue.tablecolordivide = Preference.getPref(Preference.color12, "#000000");
        PreferenceValue.tableheighttext = Preference.getPref(Preference.h4, 20);
        PreferenceValue.tablemenuonback = Preference.getPref(Preference.boolean6, false);
        PreferenceValue.tablegradleback = GradientDrawable.Orientation.valueOf(Preference.getPref(Preference.or6, GradientDrawable.Orientation.BL_TR.name()));
        PreferenceValue.tablegradleitemback = GradientDrawable.Orientation.valueOf(Preference.getPref(Preference.or7, GradientDrawable.Orientation.BL_TR.name()));

        //Formuls
        PreferenceValue.textsizeformul = Preference.getPref(Preference.float1, 20F);
        PreferenceValue.fotoformul = Preference.getPref(Preference.boolean2, false);
        PreferenceValue.colorformul = Preference.getPref(Preference.color6, "#FFFFFF");
        PreferenceValue.backcolorformul = Preference.getPref(Preference.color7, "#000000");
        PreferenceValue.gradformul = GradientDrawable.Orientation.valueOf(Preference.getPref(Preference.or5, GradientDrawable.Orientation.BL_TR.name()));
        String foto2 = Preference.getPref(Preference.foto2, "");
        if (!foto2.equals("")) {
            PreferenceValue.backdrawformul = new BitmapDrawable(foto2);
        }


        //InputProgress
        PreferenceValue.textcolorinput = Preference.getPref(Preference.input1, "#000000");
        PreferenceValue.backcolorinput = Preference.getPref(Preference.input2, "#FFFFFF");
        PreferenceValue.havebackfotoinput = Preference.getPref(Preference.input3, false);
        String foto3 = Preference.getPref(Preference.input4, "");
        PreferenceValue.hintcolorinput = Preference.getPref(Preference.input5, "#888888");
        PreferenceValue.edittextcolotinput = Preference.getPref(Preference.input6, "#000000");
        PreferenceValue.backcolotbuttominput = Preference.getPref(Preference.input7, "#aa888888");
        PreferenceValue.textcolorbuttominput = Preference.getPref(Preference.input8, "#000000");
        PreferenceValue.gradbackinput = GradientDrawable.Orientation.valueOf(Preference.getPref(Preference.input9, GradientDrawable.Orientation.BL_TR.name()));
        if (!foto3.equals("")) {
            PreferenceValue.backfotoinput = new BitmapDrawable(foto3);
        }

        //Toasts

        PreferenceValue.colortexttoast = Preference.getPref(Preference.color13, "#FF0000");

        //NavBar

        PreferenceValue.lenghtnavbar = Preference.getPref(Preference.l1, 0);
        PreferenceValue.backcolorheadernavbar = Preference.getPref(Preference.color14, "#81C784,#4CAF50,#2E7D32");
        PreferenceValue.backcolornavmenu = Preference.getPref(Preference.color15, "#FFFFFF");
        PreferenceValue.havefotohead = Preference.getPref(Preference.boolean4, false);
        PreferenceValue.havefotomenu = Preference.getPref(Preference.boolean5, false);
        PreferenceValue.textcolornavbar = Preference.getPref(Preference.color16, "#000000");
        String foto4 = Preference.getPref(Preference.foto3, "");
        String foto5 = Preference.getPref(Preference.foto4, "");
        if (!foto4.equals("")) {
            PreferenceValue.backfotoheadernavbar = new BitmapDrawable(foto4);
        }
        if (!foto5.equals("")) {
            PreferenceValue.backfotonavmenu = new BitmapDrawable(foto5);
        }
        //Swipe

        PreferenceValue.startintent = Preference.getPref(Preference.swipe1, false);
        PreferenceValue.packintent = Preference.getPref(Preference.swipe2, "");

        //Dialog
        PreferenceValue.sizedialogformultext = Preference.getPref(Preference.size22, 20F);
    }

    /**
     * Метод для запуска галереи на выбор картинки.
     * @param activity
     * @param id
     */
    public static void startForSerchFoto(Activity activity,int id){
        Intent intent;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            intent = new Intent(Intent.ACTION_PICK);
            intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        }else{
            intent = new Intent(Intent.ACTION_PICK);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/*");
        activity.startActivityForResult(Intent.createChooser(intent, "Hello"), id);
    }

    /**
     * Запись в базу данных.
     * @param activity
     * @link MainActivity.onDestroy()
     */
   public static void writeDataBase(final Activity activity){
       Thread th = new Thread(new Runnable() {
           @Override
           public void run() {
               Preference.setPref(Preference.color1,PreferenceValue.colorontouchmenu);
               Preference.setPref(Preference.color2,PreferenceValue.backcolormenu);
               Preference.setPref(Preference.color3,PreferenceValue.colormenutext);
               Preference.setPref(Preference.color4,PreferenceValue.coloredittextbuttom);
               Preference.setPref(Preference.colord1,PreferenceValue.colormenudivide);
               Preference.setPref(Preference.color5,PreferenceValue.backcolorm);
               Preference.setPref(Preference.hdiv1,PreferenceValue.hmenudivide);
               Preference.setPref(Preference.h2,PreferenceValue.heightadapter);
               Preference.setPref(Preference.boolean1,PreferenceValue.backdrawmenuboolean);
               Preference.setPref(Preference.or1,PreferenceValue.gradorientationadapter.name());
               Preference.setPref(Preference.or2,PreferenceValue.getGradorientationmenu.name());
               Preference.setPref(Preference.or3,PreferenceValue.gradordivide.name());
               Preference.setPref(Preference.or4,PreferenceValue.gradonclickadapter.name());
               Preference.setPref(Preference.or5,PreferenceValue.gradformul.name());
               Preference.setPref(Preference.h3,PreferenceValue.htextadapter);
               //Formul
               Preference.setPref(Preference.or5,PreferenceValue.gradformul.name());
               Preference.setPref(Preference.boolean2,PreferenceValue.fotoformul);
               Preference.setPref(Preference.float1,PreferenceValue.textsizeformul);
               Preference.setPref(Preference.color6,PreferenceValue.colorformul);
               Preference.setPref(Preference.color7,PreferenceValue.backcolorformul);
               //Table
               Preference.setPref(Preference.color8,PreferenceValue.tablecolortextname);
               Preference.setPref(Preference.color9,PreferenceValue.tablecolortextvalue);
               Preference.setPref(Preference.color10,PreferenceValue.tablebackcolorall);
               Preference.setPref(Preference.color11,PreferenceValue.tablebackcoloritems);
               Preference.setPref(Preference.color12,PreferenceValue.tablecolordivide);
               Preference.setPref(Preference.h4,PreferenceValue.tableheighttext);
               Preference.setPref(Preference.boolean6,PreferenceValue.tablemenuonback);
               Preference.setPref(Preference.or6,PreferenceValue.tablegradleback.name());
               Preference.setPref(Preference.or7,PreferenceValue.tablegradleitemback.name());
               //Menu
               Preference.setPref(Preference.boolean7,PreferenceValue.fotoonclick);
               Preference.setPref(Preference.boolean8,PreferenceValue.fotoonback);
               Preference.setPref(Preference.menutop,PreferenceValue.menutop);
               Preference.setPref(Preference.menuleft,PreferenceValue.menuleft);
               Preference.setPref(Preference.menuright,PreferenceValue.menuright);
               Preference.setPref(Preference.menubutton,PreferenceValue.menubutton);
               //ListMenu
               Preference.setPref(Preference.boolean3,PreferenceValue.createListmenu);
               //InputProcess
               Preference.setPref(Preference.input1,PreferenceValue.textcolorinput);
               Preference.setPref(Preference.input2,PreferenceValue.backcolorinput);
               Preference.setPref(Preference.input3,PreferenceValue.havebackfotoinput);
               Preference.setPref(Preference.input5,PreferenceValue.hintcolorinput);
               Preference.setPref(Preference.input6,PreferenceValue.edittextcolotinput);
               Preference.setPref(Preference.input7,PreferenceValue.backcolotbuttominput);
               Preference.setPref(Preference.input8,PreferenceValue.textcolorbuttominput);
               Preference.setPref(Preference.input9,PreferenceValue.gradbackinput.name());
               //Toasts
               Preference.setPref(Preference.color13,PreferenceValue.colortexttoast);
               //NavBar
               Preference.setPref(Preference.l1,PreferenceValue.lenghtnavbar);
               Preference.setPref(Preference.color14,PreferenceValue.backcolorheadernavbar);
               Preference.setPref(Preference.color15,PreferenceValue.backcolornavmenu);
               Preference.setPref(Preference.boolean4,PreferenceValue.havefotohead);
               Preference.setPref(Preference.boolean5,PreferenceValue.havefotomenu);
               Preference.setPref(Preference.color16,PreferenceValue.textcolornavbar);
               //Swipe
               Preference.setPref(Preference.swipe1,PreferenceValue.startintent);
               Preference.setPref(Preference.swipe2,PreferenceValue.packintent);
               //Dialog
               Preference.setPref(Preference.size22,PreferenceValue.sizedialogformultext);
           }
       });
       th.start();
   }



    //Help method

    /**
     * Проверяет. Является ли строка HEX строкой
     * @param str
     * @return
     */
    public static boolean isHEX(String str){
        if (str == null){
            return false;
        }
        str = str.toUpperCase();
        boolean bool = true;
        if (str.length() != 9 & str.length()!=7){
            bool = false;
        }
        else{
            if (str.charAt(0) != '#'){
                bool = false;
            }
            else{
                for (int i = 1;i<str.length();i++){
                    if (!(is16(str.charAt(i)))){
                        bool = false;
                    }
                }
            }
        }


        return bool;
    }

    /**
     * Принадлежит ли символ шестнадцатиричной системе счисления
     * @param t
     * @return
     */
    public static boolean is16(char t){
        switch (t){
            case '0': return true;

            case '1': return true;

            case '2': return true;

            case '3': return true;

            case '4': return true;

            case '5': return true;

            case '6': return true;

            case '7': return true;

            case '8': return true;

            case '9': return true;

            case 'A': return true;

            case 'B': return true;

            case 'C': return true;

            case 'D': return true;

            case 'E': return true;

            case 'F': return true;

            default: return false;
        }
    }

    /**
     * Получение градиента в виде изображения
     * @param str
     * @param op
     * @return
     */
    public static Drawable getDrawable(String str,GradientDrawable.Orientation op){
        if (str != null)
        if (str.length() > 6){
            List<String> l = new ArrayList<>();
            String in = "";
            for (int i = 0;i<str.length();i++){
                if (str.charAt(i) == ','){
                    l.add(in);
                    in = "";
                }else{
                    in +=str.charAt(i);
                }
            }
            if (!in.equals("")){
                l.add(in);
            }
            if (l.size() > 1){
                List<Integer> colors = new ArrayList<>();
                for (int i = 0;i<l.size();i++){
                    if (S.isHEX(l.get(i))) {
                        colors.add(Color.parseColor(l.get(i)));
                    }
                }
                int[]a = new int[colors.size()];
                for (int i = 0;i<a.length;i++){
                    a[i] = colors.get(i);
                }
                GradientDrawable gd = new GradientDrawable(op,a);
                return gd;
            }else{
                return new ColorDrawable(Color.parseColor(l.get(0)));
            }
        }
        return null;
    }

    /**
     * Создание уведомления. Тип 1
     * @param v
     * @param text
     */
    public static void createSnackbar(View v,String text){
        Snackbar s = Snackbar.make(v,text,Snackbar.LENGTH_LONG);
        s.show();
    }

    /**
     * Создание формулы в виде картинки
     * @param text Текст
     * @param width Ширина
     * @param height Высота
     * @param textcolor Цвет текста
     * @param textsize Размер текста
     * @param havebackfoto
     * @param backfoto Картинка на задний фон
     * @param backcolors Цвета заднего фона
     * @param orientation Тип градиента заднего фона
     * @param pixel не знаю зачем
     * @return
     */
    public static Bitmap getFormulBitmap(String text, int width, int height, int textcolor, float textsize, boolean havebackfoto, Drawable backfoto, String backcolors, GradientDrawable.Orientation orientation,int pixel) {
        TeXFormula formula = new TeXFormula(text);
        formula.setColor(textcolor);
        TeXIcon icon = formula.new TeXIconBuilder().setStyle(TeXConstants.STYLE_DISPLAY).setSize(textsize).build();
        icon.setInsets(new Insets(5,5,5,5));
        if (width < 1){
            width = icon.getIconWidth();
        }
        if (height < 1){
            height = icon.getIconHeight();
        }
        Drawable d;
        if (havebackfoto){
            d = backfoto;
        }else{
            d = S.getDrawable(backcolors,orientation);
        }
        if (d!= null) {
            InitView.setBackground(d);
        }
        Bitmap haveimage = null;
        if (icon.getIconWidth() > width){
            if (icon.getIconHeight() > height){
                haveimage = Bitmap.createBitmap(icon.getIconWidth(),icon.getIconHeight(),Bitmap.Config.ARGB_8888);
            }else{
                haveimage = Bitmap.createBitmap(icon.getIconWidth(),height, Bitmap.Config.ARGB_8888);
            }
        }else{
            if (icon.getIconHeight() > height){
                haveimage = Bitmap.createBitmap(width,icon.getIconHeight(), Bitmap.Config.ARGB_8888);
            }else{
                haveimage = Bitmap.createBitmap(width,height, Bitmap.Config.ARGB_8888);
            }
        }
        Canvas g2 = new Canvas(haveimage);
        icon.paintIcon(g2,0,0);
        return haveimage;
    }

    /**
     * Урезанная версия метода getFormulBitmap
     * @param text
     * @param width
     * @param height
     * @param textcolor
     * @param textsize
     * @param havebackfoto
     * @param backfoto
     * @param backcolors
     * @param orientation
     * @return
     */
    public static Bitmap getFormulBitmap(String text, int width, int height, int textcolor, float textsize, boolean havebackfoto, Drawable backfoto, String backcolors, GradientDrawable.Orientation orientation){
        return getFormulBitmap(text,width,height,textcolor,textsize,havebackfoto,backfoto,backcolors,orientation,20*text.length());
    }

    /**
     * Используеммая версия метода getFormulBitmap
     * @param text
     * @param width
     * @param height
     * @return
     */
    public static Bitmap getFormulBitmap(String text,int width,int height){
       return getFormulBitmap(text,width,height,Color.parseColor(PreferenceValue.colorformul),PreferenceValue.textsizeformul,PreferenceValue.fotoformul,PreferenceValue.backdrawformul,PreferenceValue.backcolorformul,PreferenceValue.gradformul);
    }

    /**
     * Целочисленное деление. Div
     * @param x
     * @param y
     * @return
     */
    public static int floorDiv(int x, int y) {
        int r = x / y;
        if ((x ^ y) < 0 && (r * y != x)) {
            r--;
        }
        return r;
    }

    public static long floorDiv(long x,long y){
        long r = x / y;
        if ((x ^ y) < 0 && (r * y != x)) {
            r--;
        }
        return r;
    }

    /**
     * Остаток от деления. Mod. Вроде не используется
     * @param x
     * @param y
     * @return
     */
    public static int floorMod(int x, int y) {
        int r = x - floorDiv(x, y) * y;
        return r;
    }

    public static long floorMod(long x, long y) {
        long r = x - floorDiv(x, y) * y;
        return r;
    }

    /**
     * Поиск в массиве элемента больше 1
     * @param ret
     * @return
     */
    public static boolean high0(int[] ret){
        boolean bool = false;
        for (int i=0;i< ret.length;i++){
            if (ret[i] >1){
                bool = true;
            }
        }
        return bool;
    }

    public static boolean high0(long[] ret){
        for (int i = 0;i<ret.length;i++){
            if(ret[i] > 1L){
                return true;
            }
        }
        return false;
    }

    /**
     * Нахождение делителей числа
     * @param x
     * @return
     */
    public static List<Long> delitel(long x){
        if (x < 0){return delitel(Math.abs(x));}else {
            try {
                List<Long> ret = new ArrayList();
                ret.add(1L);
                for (long i = 2; i < (long)Math.sqrt(x) + 1; i++) {

                    if (x % i == 0) {
                        ret.add(i);
                        ret.add(x/i);
                    }

                }
                ret.add(x);
                return ret;
            }catch (Exception e){
                InitView.createSnackbarOnNow("Необработанное исключение");
                return null;
            }
        }
    }

    /**
     * Создание строки которая по размерам будет подходить tv
     * tv Уже должен быть отображён на экране
     * @param tv
     * @param list
     * @return
     */
    public static String write(TextView tv,List<Long> list){
        String str = "";
        String now = "";
        for (int i = 0;i<list.size();i++){
            float textWidth = tv.getPaint().measureText((now + list.get(i) + ","));
            if (textWidth >= tv.getMeasuredWidth ()){
                str += now + "\n";
                now = "";
            }else{
                now+= list.get(i) + ",";
            }
        }
        if (!now.equals("")){
            str+= now;
        }
        str = delrigth(str);
        return str;
    }

    /**
     * Получение строки из массивов
     * @param list
     * @return
     */

    public static String write(List<Long> list){
        String str = "";
        for (int i = 0;i<list.size();i++){
            str += list.get(i) + ",";
        }
        return str;
    }
    //--//--//
    public static String write(long[] x){
        String str = "";
        for (int i = 0;i<x.length;i++){
            if (x[i] != 0){
                str = str + x[i] + ",";
            }
        }

        str = delrigth(str);
        return str;
    }
    // --//--
    public static String write(int[] x){
        String str = "";
        for (int i = 0;i<x.length;i++){
            if (x[i] != 0){
                str = str + x[i] + ",";
            }
        }

        str = delrigth(str);
        return str;
    }

    /**
     * Удаление правого символа
     * @param str
     * @return
     */
    public static String delrigth(String str){
        return str.substring(0,str.length()-1);
        /*String ret;
        ret = "";
        for (int i = 0;i<str.length()-1;i++){
            ret = ret + str.charAt(i);
        }
        return ret;*/
    }

    /**
     * Получение чисел, которые идут через ','
     * @param str
     * @return
     */
    public static Long[] getNumbers(String str){
        List<Long> longs = new ArrayList<>();
        String in = "";
        for (int i = 0;i<str.length();i++){
            if (str.charAt(i) == ',' && !in.equals("")){
                longs.add(Long.valueOf(in));
                in = "";
            }else{
                in+= str.charAt(i);
            }
        }
        if (!in.equals("")){
            longs.add(Long.valueOf(in));
        }
        return longs.toArray(new Long[longs.size()]);
    }

    /**
     * НОК
     * @param a
     * @param b
     * @return
     */
    public static long lcm2(long a, long b){
        return (long) (Math.abs(a * b) / gcd(a,b));
    }

    public static long lcm(long... a){
        if (a.length <2){
            return 0;
        }else{
            if (a.length == 2){
                return lcm2(a[0],a[1]);
            }else{
                long[] b =new long[a.length-1];
                for (int i=0;i<a.length-1;i++){
                    b[i] = a[i];
                }
                return lcm2(lcm(b),a[a.length-1]);
            }
        }
    }

    /**
     * НОД для дробных чисел
     * @param doubles
     * @return
     */
    public static double gcddouble(double... doubles){
        if (doubles.length > 2){
            double[] newdoubles = new double[doubles.length-1];
            for (int i = 0;i<doubles.length-1;i++){
                newdoubles[i] = doubles[i];
            }
            return gcddouble(gcddouble(newdoubles),doubles[doubles.length-1]);
        }else if (doubles.length == 2){
            return gcddouble(doubles[0],doubles[1]);
        }else if (doubles.length == 1)
        {
            return doubles[0];
        }else{
            return -1;
        }
    }

    /**
     * Поиск по имени
     * @param items
     * @param string
     * @return
     */
    public static TableItem[] search(TableItem[] items,String string){
        List<TableItem> list = new ArrayList<>();
        for (int i = 0;i<items.length;i++){
            if (find(items[i].getName(),string) > -1){
                list.add(items[i]);
            }
        }
        return list.toArray(new TableItem[list.size()]);
    }

    /**
     * Своеобразный substr
     * @param str
     * @param on
     * @param to
     * @return
     */
    public static String copyadd(String str,int on,int to){
        String ret;
        ret = "";
        if ((on > to)|(str.length() < to)){
            return "Error";
        }
        else{
            for (int i = on+1;i<to;i++){
                ret = ret + str.charAt(i);
            }
            return ret;
        }
    }

    /**
     * Поиск подстроки
     * @param str
     * @param str2
     * @return
     */
    public static int find(String str,String str2){
        if (str != null & str2 != null){
            if (str.length() == str2.length()){
                if (str.equals(str2)){
                    return -2;
                }
                else{
                    return -3;
                }
            }
            else if (str2.length() > str.length()){
                return -4;
            }
            else if (str2.length() < str.length()){
                int o = -3;
                for (int i = 0; i < str.length()-str2.length()+1; i++){
                    if (str2.equals(copyadd(str,i-1,str2.length()+i))){
                        o = i;
                    }
                }
                return o;

            }
            else {
                return -4;
            }
        }else{
            return -5;
        }
    }

    /**
     * Нод для дробных
     * @param a
     * @param b
     * @return
     */
    public static double gcddouble(double a, double b) {
        if (a < 0){return gcddouble(Math.abs(a),b);}else {
            if (b < 0) {
                return gcddouble(a, Math.abs(b));
            } else {
                if (a == b) return a;
                if (b == 0) return a;
                if (a == 0) return b;
                if (Math.floor(a / b) > Math.floor(b / a)) {
                    double x = (a / b) - Math.floor(a / b);
                    if (x > 1) {
                        double t = gcddouble(b, x);
                        if (t < 1) {
                            return 1;
                        }
                    } else if (x == 0) {
                        return gcddouble(b, x);
                    }
                    return 1;
                } else {
                    return gcddouble(b, a);
                }
            }
        }


    }

    /**
     * НОД
     * @param a
     * @return
     */
    public static long gcd(long... a){
        if (a.length < 2){
            return 1;
        }else{
            if (a.length == 2){
                return (long) gcd2(a[0],a[1]);
            }else{
                long[] b = new long[a.length-1];
                for (int i = 0;i<a.length-1;i++){
                    b[i] = a[i];
                }
                return gcd(gcd(b),a[a.length-1]);
            }
        }
    }

    public static double gcd2(long b,long d1) {
        if (b < 0) {
            return gcd(-b, d1);
        }
        if (d1 < 0) {
            return gcd(b, -d1);
        }
        if (b == 0) {
            return d1;
        }
        if (d1 == 0) {
            return b;
        }
        if (b == 1) {
            return 1;
        }
        if (d1 == 1) {
            return 1;
        }
        if (b == d1) {
            return b;
        }
        if (krat2(b) & krat2(d1)) {
            return 2 * gcd(b / 2, d1 / 2);
        }
        if (krat2(b) & !krat2(d1)) {
            return gcd(b / 2, d1);
        }
        if (!krat2(b) & krat2(d1)) {
            return gcd(d1 / 2, b);
        }
        if (!krat2(b) & !krat2(d1)) {
            if (b > d1) {
                return gcd((b - d1) / 2, d1);
            }
            if (b < d1) {
                return gcd((d1 - b) / 2, b);
            }
        }
        return 0;
    }

    /**
     * Кратность 2
     * @param a
     * @return
     */
    public static boolean krat2(long a){
        if (a != 0){
            double c = a%2;
            if (c==0){
                return true;
            }
            return false;
        }else{
            return false;
        }
    }

    /**
     * Создание меню для формул
     * @param activity
     * @param strings
     * @param strings2
     * @param strings3
     * @param ints
     * @param clickMenus
     * @return
     */
    public static Menu getMENU(Activity activity, String[] strings, String[] strings2, String[] strings3, int[] ints, OnClickMenu[] clickMenus){
        if (ints.length == 0){
            return getMENU(activity,strings,strings2,strings3);
        }else if (ints.length == clickMenus.length){
            List<ButtonMenu> buttonMenus = new ArrayList<>();
            List<Integer> l = new ArrayList<>();
            for (int i = 0;i<ints.length;i++){
                l.add(ints[i]);
            }
            for (int i = 0;i<strings.length;i++){
                if (l.indexOf(i) >-1) {
                    buttonMenus.add(new ButtonMenu(strings[i], clickMenus[l.indexOf(i)]));
                }else{
                    String str = "";
                    if(strings3 != null){
                        str = strings3[i];
                    }
                    buttonMenus.add(new ButtonMenu(strings[i],InitView.createFormul(activity,strings2[i],InitView.getNowMenuItem(),str)));
                }
            }
            return new Menu(buttonMenus);
        }else{
            return getMENU(activity,strings,strings2,strings3);
        }
    }


    public static Menu getMENU(final Activity activities, String[] strings, final String[] strings2, String[] strings3){
        List<ButtonMenu> buttonMenus = new ArrayList();
        for (int i = 0;i<strings.length;i++){
            String str = "";
            if(strings3 != null){
                str = strings3[i];
            }
            buttonMenus.add(new ButtonMenu(strings[i],InitView.createFormul(activities,strings2[i],InitView.getNowMenuItem(),str)));
        }
        return new Menu(buttonMenus);
    }

    public static OnClickMenu getClick(final Activity activity, final boolean bool, final String name, final String[]... strings){
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                Menu m = null;
                if(strings.length > 2) {
                    if (bool) {
                        m = getMENU(activity, strings[0], strings[1], strings[2]);
                    } else{
                        m = getTextMENU(activity,strings[0],strings[1]);
                    }
                }
                if (PreferenceValue.createListmenu){
                    final Menu finalM = m;
                    new OnClickMenu() {
                        @Override
                        public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                            ListDialog listDialog = new ListDialog(name, finalM);
                            listDialog.getDialog(activity).show();
                        }
                    }.run(adapterView,view,i,l,buttonMenu);
                }else{
                    InitView.initMenu(activity,m,InitView.getNowMenuItem());
                }
            }
        };
    }

    public static Menu getTextMENU(final Activity activity, final String[] strings, final String[] strings2){
        List<ButtonMenu> buttonMenus = new ArrayList<>();
        for (int i =0 ;i<strings.length;i++){
            buttonMenus.add(new ButtonMenu(strings[i], new OnClickMenu() {
                @Override
                public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                    InitView.initText(activity,strings2[i]);
                }
            }));
        }
        return new Menu(buttonMenus);
    }


    //Search

    public static List<SearchItem> getItems(String str){
        List<SearchItem> list = new ArrayList();
        String now = "";
        boolean bool = false;
        boolean a = false;
        boolean b = false;
        for (int i = 0;i<str.length();i++){
            if (str.charAt(i) == '{'){
                bool = true;
                list.add(new SearchItem(now,a,b));
                now = "";
                a =  false;
                b=  false;
            }else if (str.charAt(i) == '}'){
                bool = false;
                list.add(new SearchItem(now,a,b));
                now = "";
                a = false;
                b = false;
            }else{
                if (str.charAt(i) == '#'){
                    if (now != ""){
                        if (!bool){
                            list.add(new SearchItem(now,a,b));
                            now = "";
                            a = true;
                            b = false;
                        }else{
                            now += str.charAt(i);
                        }
                    }else{
                        a = true;
                    }
                }else if (str.charAt(i) == '$'){
                    if (now != ""){
                        if (!bool){
                            list.add(new SearchItem(now,a,b));
                            now ="";
                            a = false;
                            b = true;
                        }else{
                            now += str.charAt(i);
                        }
                    }else{
                        b = true;
                    }
                }else{
                    now += str.charAt(i);
                }
            }
        }
        if (now != ""){
            list.add(new SearchItem(now,a,b));
        }
        return list;
    }


    public static SearchItem getItem(String str){
        if (str.charAt(0) == '#'){
            str = delleft(str);
            SearchItem r = getItem(str);
            r.setStart(true);
            return r;
        }
        if (str.charAt(0) == '$'){
            str = delleft(str);
            SearchItem r = getItem(str);
            r.setEnd(true);
            return r;
        }
        return new SearchItem(str,false,false);

    }

    public static List<TableItem> prosearch(String str,List<TableItem> l){
        List<TableItem> list = new ArrayList();
        List<SearchItem> items = getItems(str);
        for (int i = 0;i<l.size();i++){
            String s = l.get(i).getName().toLowerCase();
            boolean bool = true;
            for (int j = 0;j<items.size();j++){
                SearchItem k = items.get(j);
                String ser = k.getString().toLowerCase();
                if (k.isStart() && k.isEnd()){
                    if (s.indexOf(ser) < 0){
                        bool = false;
                        break;
                    }
                }else if (k.isEnd()){
                    if (!s.endsWith(ser)){
                        bool = false;
                        break;
                    }
                }else if (k.isStart()){
                    if (!s.startsWith(ser)){
                        bool = false;
                        break;
                    }
                }else{
                    if (s.indexOf(ser) < 0){
                        bool = false;
                        break;
                    }
                }
            }
            if (bool){
                list.add(l.get(i));
            }
        }

        return list;
    }

    /**
     * Удаляет один символ слева.
     * @param str Строка
     * @return Изменённая строка.
     */
    public static String delleft(String str){
        String ret;
        ret = "";
        for (int i = 1;i<str.length();i++){
            ret = ret + str.charAt(i);
        }
        return ret;
    }

    //Перевод в системы счисления

    public static String tento(String str, int i, int u) {
        if (str != "Неверное число") {
            str = str.toUpperCase();
            String ret;
            double a, b, c;
            int y, z, x = 0;
            ret = "";
            if (i <= 1) {
                ret = "-1";
            } else {
                if (u < 1) {
                    u = 50;
                }
                a = Double.valueOf(str);
                b = Math.floor(a);
                c = Int(a);
                y = (int) b;
                ret = to(y, i);
                if (c != 0.0000000) {
                    ret = ret + ".";
                    while ((Int(c) != 0.00000) & (x < u)) {
                        c = c * i;
                        z = (int) Math.floor(c);
                        ret = ret + get16(z);
                        c = Int(c);
                        x = x + 1;
                    }
                }
            }


            return ret;
        } else{
            return "Неверное число";
        }
    }

    public static Double Int(double a) {
        return (a - Math.floor(a));
    }

    public static String to(int str, int i) {
        String ret;
        int a;
        ret = "";
        if (i <= 1) {

        } else {
            if (i == 2) {
                ret = Integer.toBinaryString(str);
            } else {
                a = str;
                while (a != 0){
                    ret = get16(String.valueOf(a % i)) + ret;
                    a = floorDiv(a, i);
                }

            }
        }
        return ret;
    }

    public static String get16(String str) {
        String ret;
        ret = "";
        if (Double.valueOf(str) >= 0) {
            if (Integer.valueOf(str) < 10) {
                ret = str;
            } else {
                switch (Integer.valueOf(str)) {
                    case 10:
                        ret = "A";
                        break;
                    case 11:
                        ret = "B";
                        break;
                    case 12:
                        ret = "C";
                        break;
                    case 13:
                        ret = "D";
                        break;
                    case 14:
                        ret = "E";
                        break;
                    case 15:
                        ret = "F";
                        break;
                    case 16:
                        ret = "G";
                        break;
                    case 17:
                        ret = "H";
                        break;
                    case 18:
                        ret = "I";
                        break;
                    case 19:
                        ret = "J";
                        break;
                    case 20:
                        ret = "K";
                        break;
                    case 21:
                        ret = "L";
                        break;
                    case 22:
                        ret = "M";
                        break;
                    case 23:
                        ret = "N";
                        break;
                    case 24:
                        ret = "O";
                        break;
                    case 25:
                        ret = "P";
                        break;
                    case 26:
                        ret = "Q";
                        break;
                    case 27:
                        ret = "R";
                        break;
                    case 28:
                        ret = "S";
                        break;
                    case 29:
                        ret = "T";
                        break;
                    case 30:
                        ret = "U";
                        break;
                    case 31:
                        ret = "V";
                        break;
                    case 32:
                        ret = "W";
                        break;
                    case 33:
                        ret = "X";
                        break;
                    case 34:
                        ret = "Y";
                        break;
                    case 35:
                        ret = "Z";
                        break;

                }
            }
        }

        return ret;
    }

    public static String get16(int str) {
        String ret;
        ret = "";
        if (str >= 0) {
            if (str < 10) {
                ret = String.valueOf(str);
            } else {
                switch (str) {
                    case 10:
                        ret = "A";
                        break;
                    case 11:
                        ret = "B";
                        break;
                    case 12:
                        ret = "C";
                        break;
                    case 13:
                        ret = "D";
                        break;
                    case 14:
                        ret = "E";
                        break;
                    case 15:
                        ret = "F";
                        break;
                    case 16:
                        ret = "G";
                        break;
                    case 17:
                        ret = "H";
                        break;
                    case 18:
                        ret = "I";
                        break;
                    case 19:
                        ret = "J";
                        break;
                    case 20:
                        ret = "K";
                        break;
                    case 21:
                        ret = "L";
                        break;
                    case 22:
                        ret = "M";
                        break;
                    case 23:
                        ret = "N";
                        break;
                    case 24:
                        ret = "O";
                        break;
                    case 25:
                        ret = "P";
                        break;
                    case 26:
                        ret = "Q";
                        break;
                    case 27:
                        ret = "R";
                        break;
                    case 28:
                        ret = "S";
                        break;
                    case 29:
                        ret = "T";
                        break;
                    case 30:
                        ret = "U";
                        break;
                    case 31:
                        ret = "V";
                        break;
                    case 32:
                        ret = "W";
                        break;
                    case 33:
                        ret = "X";
                        break;
                    case 34:
                        ret = "Y";
                        break;
                    case 35:
                        ret = "Z";
                        break;

                }
            }
        }

        return ret;
    }

    public static boolean isminus(String str){
        if (str.charAt(0) == '-' | str.charAt(0) == '-'){
            return true;
        }
        else{
            return false;
        }
    }

    public static String gototen(String str,int b){
        boolean isminus = false;
        while (isminus(str)){
            str = delleft(str);
            isminus = true;
        }
        String ret = "";
        if (findleft(str,'#') == 0){
            boolean bool = true;
            int a = 0;
            String[] r = new String[Math.round(str.length()/2)];
            r[0] = "";
            for (int i = 0;i <str.length();i++){
                if (str.charAt(i) == '#'){
                    if (i != 0){
                        a += 1;
                        r[a] = "";
                    }
                }
                else{
                    r[a] += str.charAt(i);
                }
            }
            r = delnull(r);
            MyBigInteger myint = new MyBigInteger("0");
            for (int i = 0;i < r.length;i++){
                try{
                    if (Integer.valueOf(r[i]) >= b | Integer.valueOf(r[i]) < 0){return "Неверное число. Неправильно указаны системы счисления";}
                    myint = myint.add(new MyBigInteger(String.valueOf(Integer.valueOf(r[i]))).multiply(powerBig(b,r.length-1-i)));
                }
                catch(Exception e){

                }
            }
            return myint.get();
        }else{
            int pos = findleft(str,'#');
            if (pos > -1){
                return gototen(Copy2(str,pos-1,str.length()),b);
            }else{
                if (isminus){
                    return toten("-" + str,b);
                }else {
                    return toten(str, b);
                }
            }
        }
    }

    public static String[] delnull(String[] str) {
        String[] ret = new String[schetnull(str)];
        int i2 = 0;
        for (int i = 0; i < str.length; i++) {
            if (str[i] != null) {
                ret[i2] = str[i];
                i2 = i2 + 1;
            }
        }

        return ret;
    }

    public static int schetnull(String[] str) {
        int i2 = 0;

        for (int i = 0; i < str.length; i++) {
            if (str[i] != null) {
                i2 = i2 + 1;
            }
        }
        return i2;
    }

    public static boolean isthis(String str,char t){
        boolean u;
        u = false;
        for (int i = 0;i<str.length();i++){
            if (str.charAt(i) == t){
                u = true;
            }
        }
        return u;

    }

    public static String toten(String str,int b){
        while(isminus(str)){
            str = delleft(str);
        }
        str = str.toUpperCase();
        int n= 0;
        double j = 0;
        String ret;
        ret = "";
        if (isthis(str,',')| isthis(str,'.')){
            if (isthis(str,',')){
                for (int i = findleft(str,',')-1;i > -1;i--){
                    if (Integer.valueOf(String.valueOf(str.charAt(i))) >= b){
                        return "Неверное число";
                    }
                    else{
                        n = n + (get10(String.valueOf(str.charAt(i))) * power(b,findleft(str,',')-i-1));
                    }
                }
                for (int i = findleft(str,',')+1;i < str.length();i++){
                    if (Integer.valueOf(String.valueOf(str.charAt(i))) >= b){
                        return "Неверное число";
                    }
                    else{
                        j = j + (get10(String.valueOf(str.charAt(i)))) * Math.pow(b,findleft(str,',')-i);
                    }}
            }else{
                str = str.replace('.', ',');
                ret = toten(str,b);
                return ret;
            }

        }
        else{
            MyBigInteger n2 = new MyBigInteger("0");
            for (int i = str.length()-1; i > -1;i--){
                if (str.charAt(i) != '0'){
                    if (get10(String.valueOf(str.charAt(i))) >= b){
                        return "Неверное число";
                    }
                    else{
                        n2 = n2.add(new MyBigInteger(String.valueOf(get10(String.valueOf(str.charAt(i))))).multiply(powerBig(b,str.length()-i-1)));
                    }}
            }
            return n2.get();
        }
        ret = String.valueOf(n + j);
        return ret;

    }
    public static MyBigInteger powerBig(int a,int b){
        MyBigInteger c = new MyBigInteger(String.valueOf(a));
        return c.pow(b);
    }

    public static int findleft(String str,char t){
        if (str == null){
            return -1;
        }else {
            int u;
            u = -8;
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) == t) {
                    return i;
                }
            }
            if (u == -8) {
                return -1;
            } else {
                return u;
            }
        }
    }

    public static int power(int a,int c){
        int ret;
        ret = 1;
        if (c == 0){
            ret = 1;
        }
        else{
            if (c == 1){
                ret = a;
            }
            else{
                for (int i = 0;i < c;i++){
                    ret = ret * a;
                }
            }
        }
        return ret;
    }

    public static String Copy2(String str,int on,int to){
        String ret;
        ret = "";
        if ((on > to)|(str.length() < to)){
            return "Error";
        }
        else{
            for (int i = on+1;i<to;i++){
                ret = ret + str.charAt(i);
            }
            return ret;
        }
    }

    public static int get10(String str) {
        int ret;
        ret = 0;
        boolean bool = false;
        try{
            Integer.valueOf(str);
            bool = true;
        }catch (Exception e){}
        if (bool) {
            try {
                ret = Integer.valueOf(str);
            }catch (Throwable e){
                return 0;
            }
        } else {
            switch (str) {
                case "A":
                    ret = 10;
                    break;
                case "B":
                    ret = 11;
                    break;
                case "C":
                    ret = 12;
                    break;
                case "D":
                    ret = 13;
                    break;
                case "E":
                    ret = 14;
                    break;
                case "F":
                    ret = 15;
                    break;
                case "G":
                    ret = 16;
                    break;
                case "H":
                    ret = 17;
                    break;
                case "I":
                    ret = 18;
                    break;
                case "J":
                    ret = 19;
                    break;
                case "K":
                    ret = 20;
                    break;
                case "L":
                    ret = 21;
                    break;
                case "M":
                    ret = 22;
                    break;
                case "N":
                    ret = 23;
                    break;
                case "O":
                    ret = 24;
                    break;
                case "P":
                    ret = 25;
                    break;
                case "Q":
                    ret = 26;
                    break;
                case "R":
                    ret = 27;
                    break;
                case "S":
                    ret = 28;
                    break;
                case "T":
                    ret = 29;
                    break;
                case "U":
                    ret = 30;
                    break;
                case "V":
                    ret = 31;
                    break;
                case "W":
                    ret = 32;
                    break;
                case "X":
                    ret = 33;
                    break;
                case "Y":
                    ret = 34;
                    break;
                case "Z":
                    ret = 35;
                    break;
            }
        }

        return ret;
    }



    public static Bitmap drawableToBitmap (Drawable drawable,int w,int h) {
        Bitmap bitmap = null;
        if (w < 1){
            w = 1;
        }
        if (h < 1){
            h = 1;
        }
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                Bitmap ret = Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(),w,h,true);
                return ret;
            }
        }
        bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static boolean isTooLarge (TextView text, String newText) {
        float textWidth = text.getPaint().measureText(newText);
        return (textWidth >= text.getMeasuredWidth ());
    }

    public static void getRealDrawable(Activity activity,int h,boolean bool1,boolean bool2){
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int w = displaymetrics.widthPixels;
        if (bool1) {
            try {
                PreferenceValue.fotoclickh = new BitmapDrawable(Bitmap.createScaledBitmap(S.drawableToBitmap(PreferenceValue.fotoclick, PreferenceValue.fotoclick.getIntrinsicWidth(), PreferenceValue.fotoclick.getIntrinsicHeight()), w, h, true));
            }catch (Exception e){
                Log.w("LIVe","It havent fotoclick");
            }
        }
        if (bool2) {
            try {
                PreferenceValue.fotobackh = new BitmapDrawable(Bitmap.createScaledBitmap(S.drawableToBitmap(PreferenceValue.fotoback, PreferenceValue.fotoback.getIntrinsicWidth(), PreferenceValue.fotoback.getIntrinsicHeight()), w, h, true));
            }catch (Exception e){
                Log.w("LIVe","It havent fotoback");
            }
        }
    }

    public static String getsctcin(String value) throws Exception{
        if (value.contains("(")){
            value = value.substring(0,value.indexOf("(")) + Calculator.calculate(value.substring(value.indexOf("("),value.lastIndexOf(")")))+value.substring(value.lastIndexOf(")")+1);
            value = value.replace(',', '.');
        }
        String str = "";
        if (value.startsWith("sin")){
            double sin = Double.valueOf(value.substring(3));
            double alpha = Math.asin(sin);
            double cos = Math.cos(alpha);
            double tg = sin/cos;
            double ctg = 1/tg;
            if (sin == 0){
                cos = 1;
                tg = 0;
                ctg = Double.POSITIVE_INFINITY;
            }
            if (sin == 1){
                cos = 0;
                tg = Double.POSITIVE_INFINITY;
                ctg = 0;
            }
            alpha = Math.toDegrees(alpha);
            str = "Угол = " + alpha + "\n" + "sin = " + sin + "\n" + "cos = "+ cos+"\n"+"tg = " + tg+"\n"+"ctg = "+ctg;
        }else if (value.startsWith("cos")){
            double alpha = Math.acos(Double.valueOf(value.substring(3)));
            double sin = Math.sin(alpha);
            double cos = Math.cos(alpha);
            double tg = sin/cos;
            double ctg = 1/tg;
            alpha = Math.toDegrees(alpha);
            str = "Угол = " + alpha + "\n" + "sin = " + sin + "\n" + "cos = "+ cos+"\n"+"tg = " + tg+"\n"+"ctg = "+ctg;
        }else if (value.startsWith("tg")){
            double alpha = Math.atan(Double.valueOf(value.substring(2)));
            double sin = Math.sin(alpha);
            double cos = Math.cos(alpha);
            double tg = sin/cos;
            double ctg = 1/tg;
            alpha = Math.toDegrees(alpha);
            str = "Угол = " + alpha + "\n" + "sin = " + sin + "\n" + "cos = "+ cos+"\n"+"tg = " + tg+"\n"+"ctg = "+ctg;
        }else if (value.startsWith("tan")){
            double alpha = Math.atan(Double.valueOf(value.substring(3)));
            double sin = Math.sin(alpha);
            double cos = Math.cos(alpha);
            double tg = sin/cos;
            double ctg = 1/tg;
            alpha = Math.toDegrees(alpha);
            str = "Угол = " + alpha + "\n" + "sin = " + sin + "\n" + "cos = "+ cos+"\n"+"tg = " + tg+"\n"+"ctg = "+ctg;
        }else if (value.startsWith("ctg")){
            double alpha = Math.atan(1/Double.valueOf(value.substring(3)));
            double sin = Math.sin(alpha);
            double cos = Math.cos(alpha);
            double tg = sin/cos;
            double ctg = 1/tg;
            alpha = Math.toDegrees(alpha);
            str = "Угол = " + alpha + "\n" + "sin = " + sin + "\n" + "cos = "+ cos+"\n"+"tg = " + tg+"\n"+"ctg = "+ctg;
        }else if (value.startsWith("ctan")){
            double alpha = Math.atan(1/Double.valueOf(value.substring(4)));
            double sin = Math.sin(alpha);
            double cos = Math.cos(alpha);
            double tg = sin/cos;
            double ctg = 1/tg;
            alpha = Math.toDegrees(alpha);
            str = "Угол = " + alpha + "\n" + "sin = " + sin + "\n" + "cos = "+ cos+"\n"+"tg = " + tg+"\n"+"ctg = "+ctg;
        }
        else if (value.endsWith("!")){
            double alpha = Math.toRadians(Double.valueOf(value.substring(0, value.length()-1)));
            double sin = Math.sin(alpha);
            double cos = Math.cos(alpha);
            double tg = sin/cos;
            double ctg = 1/tg;
            alpha = Math.toDegrees(alpha);
            str = "Угол = " + alpha + "\n" + "sin = " + sin + "\n" + "cos = "+ cos+"\n"+"tg = " + tg+"\n"+"ctg = "+ctg;
        }
        else{
            double alpha = Double.valueOf(value);
            double sin = Math.sin(alpha);
            double cos = Math.cos(alpha);
            double tg = sin/cos;
            double ctg = 1/tg;
            alpha = Math.toDegrees(alpha);
            str = "Угол = " + alpha + "\n" + "sin = " + sin + "\n" + "cos = "+ cos+"\n"+"tg = " + tg+"\n"+"ctg = "+ctg;
        }
        return str;
    }


    public static View getView(Activity activity,int id){
        return activity.getLayoutInflater().inflate(id,null,false);
    }

    


    public static String getRealPath(Context context, Uri uri){
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        int column_index
                = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static String getRealPathFromURI_BelowAPI11(Context context, Uri contentUri){
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        int column_index
                = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static void setCustomKeyBoard1(YEdit edit,Activity activity){
        final RelativeLayout layout = (RelativeLayout)edit.getKeyBoard(activity, R.layout.l_keyboard);
        layout.setVisibility(View.GONE);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        layout.setLayoutParams(layoutParams);
        layout.setGravity(Gravity.BOTTOM);
        InitView.add(layout);
        edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) layout.setVisibility(View.VISIBLE);
                else layout.setVisibility(View.GONE);
            }
        });
    }

    public static void setCustomKeyBoard2(YEdit edit,Activity activity){
        final RelativeLayout layout = (RelativeLayout)edit.getKeyBoard(activity, R.layout.l_keyboard_2);
        layout.setVisibility(View.GONE);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        layout.setLayoutParams(layoutParams);
        layout.setGravity(Gravity.BOTTOM);
        InitView.add(layout);
        edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) layout.setVisibility(View.VISIBLE);
                else layout.setVisibility(View.GONE);
            }
        });
    }


    private int getNumberOfCores() {
        if(Build.VERSION.SDK_INT >= 17) {
            return Runtime.getRuntime().availableProcessors();
        }
        else {
            // Use saurabh64's answer
            class CpuFilter implements FileFilter {
                @Override
                public boolean accept(File pathname) {
                    //Check if filename is "cpu", followed by a single digit number
                    if(Pattern.matches("cpu[0-9]+", pathname.getName())) {
                        return true;
                    }
                    return false;
                }
            }

            try {
                //Get directory containing CPU info
                File dir = new File("/sys/devices/system/cpu/");
                //Filter to only list the devices we care about
                File[] files = dir.listFiles(new CpuFilter());
                //Return the number of cores (virtual CPU devices)
                return files.length;
            } catch(Exception e) {
                //Default to return 1 core
                return 4;
            }
        }
    }

    public static void getPermission(Activity activity, int id){
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, id);
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
    }


}
