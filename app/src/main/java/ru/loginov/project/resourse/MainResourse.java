package ru.loginov.project.resourse;

/**
 * Created by Loginov Ilya on 01.10.2017.
 */

public final class MainResourse{
    //Теги для класса Log и его методов
    public static final String TAG = "LIVe";
    public static final String TAG_V = "LIVe:Verbose";
    public static final String TAG_D = "LIVe:Debug";
    public static final String TAG_I = "LIVe:Info";
    public static final String TAG_W = "LIVe:Warn";
    public static final String TAG_E = "LIVe:Error";
    public static final String TAG_A = "LIVe:Assert";
}
