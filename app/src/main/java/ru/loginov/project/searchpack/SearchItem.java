package ru.loginov.project.searchpack;

import ru.loginov.project.S;

/**
 * Created by user on 06.11.2016.
 */
public final class SearchItem {
    private String str = "";
    private boolean start = false;
    private boolean end = false;


    public SearchItem(String str){
        new SearchItem(S.getItem(str));

    }

    public SearchItem(SearchItem s){
        this.str = s.getString();
        this.start = s.isStart();
        this.end = s.end;
    }

    public SearchItem(String str,boolean a,boolean b){
        this.str = str;
        this.start = a;
        this.end = b;
    }

    public String getString(){
        return str;
    }

    public boolean isStart(){
        return start;
    }

    public boolean isEnd(){
        return end;
    }

    public void setString(String str){
        this.str = str;
    }

    public void setStart(boolean start){
        this.start =start;
    }

    public void setEnd(boolean end){
        this.end =end;
    }
}