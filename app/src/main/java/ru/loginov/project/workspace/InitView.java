package ru.loginov.project.workspace;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.loginov.project.R;
import ru.loginov.project.S;
import ru.loginov.project.menuclass.ButtonMenu;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.MenuAdapter;
import ru.loginov.project.menuclass.MenuView;
import ru.loginov.project.menuclass.OnClickMenu;
import ru.loginov.project.otherclass.MyToast;
import ru.loginov.project.otherclass.TouchImageView;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.preference.menu.MainMenu;

/**
 * Created by user on 18.10.2016.
 */
public final class InitView {
    private static Activity activity = null;
    private static RelativeLayout mainparent = null;
    private static List<MainView> history = new ArrayList<>();
    private static MainView now = null;

    public InitView(Activity activity){
        mainparent = (RelativeLayout) activity.findViewById(R.id.superparent);
        InitView.activity = activity;
    }

    public static void initMenu(Activity activity, Menu menu, MenuItem item){
        if (menu != null) {
            MainView mv = new MainView(menu.getView(activity), null, item);
            initView(activity, mv);
        }else{
            InitView.createSnackbarOnNow("NULL");
        }
    }

    public static void initView(Activity activity,MainView view,MenuItem item){
        if (view.getItem() == null){
            view.setItem(item);
        }
        initView(activity,view);
    }

    private static void initView(Activity activity,View view){
        initView(activity,new MainView(view));
    }

    private static void initView(Activity activity,MainView view){
        initDown(activity,view);
    }

    private static void initDown(Activity activity,MainView view) {
        if (now != null) {
            addToHistory();
            removeNow(activity);
            now = view;
            initNow(activity);
        } else {
            now = view;
            initNow(activity);
        }
    }

    public static void reloadAll(){
        for (int i = 0;i<history.size();i++){
            history.get(i).setNeedreload(true);
        }
    }

    public static void reloadNow(Activity activity, final int h){
        reloadNow(activity);
    }

    public static void reloadNow(Activity activity){
        if (now != null) {
            if (now.getOnReload() != null)
                if (now.getOnReload().preReload(activity)) return;
            removeNow(activity);
            if (now.getOnReload() != null) now.getOnReload().onReload(activity);
            initNow(activity);
            if (now.getOnReload() != null) now.getOnReload().postReload(activity);
        }else{
            initMenu(activity, new MainMenu().getMenu(activity),null);
        }
    }

    private static MainView getBackView(){
        if (history.size() > 0){
            return history.get(history.size()-1);
        }else{
            return null;
        }
    }

    private static void addToHistory(){
        history.add(now);
    }

    private static void removeToHistory(){
        history.remove(now);
    }

    private static void initNow(final Activity activity){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (now.getView() instanceof MenuView){
                    ((MenuView) now.getView()).setAdapter(new MenuAdapter(activity,((MenuView) now.getView()).getMenu(),PreferenceValue.heightadapter,PreferenceValue.htextadapter));
                    ((MenuView) now.getView()).setSelection(((MenuView) now.getView()).getH());
                    if (PreferenceValue.backdrawmenuboolean){
                        mainparent.setBackgroundDrawable(PreferenceValue.backdrawmenu);
                    }else{
                        mainparent.setBackgroundDrawable(S.getDrawable(PreferenceValue.backcolorm,PreferenceValue.getGradorientationmenu));
                    }
                    ((MenuView) now.getView()).setDivide(PreferenceValue.colormenudivide,PreferenceValue.hmenudivide);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT);
                    if (PreferenceValue.menutop > (getH()/2)) PreferenceValue.menutop = 0;
                    if (PreferenceValue.menubutton > getH()/2) PreferenceValue.menubutton=0;
                    if (PreferenceValue.menuleft>getW()/2) PreferenceValue.menuleft = 0;
                    if (PreferenceValue.menuright > getW()/2) PreferenceValue.menuright = 0;
                        layoutParams.setMargins(PreferenceValue.menuleft, PreferenceValue.menutop, PreferenceValue.menuright, PreferenceValue.menubutton);
                        now.getView().setLayoutParams(layoutParams);
                }
                if (now.getItem() != null){
                    now.getItem().setChecked(true);
                    activity.setTitle(now.getItem().getTitle());
                }else{
                    activity.setTitle("Помощник");
                }
                if (now.getView().getParent() != null){
                    ((RelativeLayout)now.getView().getParent()).removeAllViews();
                }
                if (now.getV() != null) {
                    View view = now.getV().getView(activity);
                    if (view != null){
                        mainparent.addView(view);
                        if (now.getRun() != null) {
                            now.getRun().onInit(activity, mainparent.getWidth(), mainparent.getHeight());
                        }
                        return;
                    }
                }
                if (now.getRun() != null) {
                    now.getRun().onInit(activity, mainparent.getWidth(), mainparent.getHeight());
                }
                mainparent.addView(now.getView());
                if (now.getRun() != null) {
                    now.getRun().onInit(activity, mainparent.getWidth(), mainparent.getHeight());
                }
            }
        });
    }

    private static void removeNow(Activity activity){
        if (now != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (now.getItem() != null) {
                        now.getItem().setChecked(false);
                    }
                    mainparent.removeAllViews();
                }
            });
        }

    }

    private static boolean initBackPress(Activity activity){
        MainView mainView = getBackView();
        if (mainView != null){
            removeNow(activity);
            now = mainView;
            removeToHistory();
            initNow(activity);
            if (now.getBackInit() != null){
                now.getBackInit().onInit(activity);
            }
            return true;
        }
        return false;
    }

    public static boolean onBackPress(Activity activity){
        if (now.getBack() != null){
            if (now.getBack().onBackPress(activity)){
                return false;
            }else{
                return initBackPress(activity);
            }
        }else{
            return initBackPress(activity);
        }
    }

    public static void onMenu(Activity activity){
        if (now != null){
            if (now.getOnMenu() != null) {
                now.getOnMenu().onOpenMenu(activity);
            }else{
                DrawerLayout drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
                if (!drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        }
    }

    public static void removeHistory(Activity activity){
        history = new ArrayList<>();
        removeNow(activity);
        now = null;
        S.initMainMenu(activity);
    }

    public static void removeH(Activity activity){
        history = new ArrayList<>();
        removeNow(activity);
        now = null;
    }

    public static void replaceNow(Activity activity,MainView view){
        removeNow(activity);
        now = view;
        initNow(activity);
    }


    public static RelativeLayout.LayoutParams setBounds(int top,int left,int right,int bottom){
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(left,top,right,bottom);
        return layoutParams;
    }


    public static MenuItem getNowMenuItem(){
        if (now != null) {
            return now.getItem();
        }
        return null;
    }

    public static int getW(){
        if (mainparent != null){
            return mainparent.getWidth();
        }else{
            return 0;
        }
    }
    public static int getH(){
        if (mainparent != null){
            return mainparent.getHeight();
        }
        return 0;
    }

    public static void setBackground(@NonNull final Drawable background){
        InitView.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mainparent.setBackgroundDrawable(background);
            }
        });

    }


    public static void createSnackbarOnNow(String text){
        try {
            S.createSnackbar(now.getView(), text);
        }catch (Exception e){
            MyToast.show(text);
        }
    }

    public static void createFormulView(final Activity activity, final String text, MenuItem item, final String text2){
        final TouchImageView tv = new TouchImageView(activity);
        if (text2!=null){
            tv.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                        if (!text2.equals("")) {
                            AlertDialog.Builder adb = new AlertDialog.Builder(activity);
                            adb.setTitle("Помощь");
                            LinearLayout ll = new LinearLayout(activity);
                            ll.setOrientation(LinearLayout.VERTICAL);
                            Button button = new Button(activity);
                            button.setBackgroundDrawable(S.getDrawable("#88FFFFFF", null));
                            button.setText("Закрыть");
                            TouchImageView touchImageView = new TouchImageView(activity);
                            touchImageView.setImageBitmap(S.getFormulBitmap(text2, -1, -1, Color.parseColor("#000000"), PreferenceValue.sizedialogformultext, false, null, null, null));
                            ll.addView(touchImageView);
                            ll.addView(button);
                            adb.setView(ll);
                            final Dialog d = adb.create();
                            button.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View view, MotionEvent motionEvent) {
                                    if (view != null) {
                                        if (motionEvent.getAction() == motionEvent.ACTION_DOWN) {
                                            view.setBackgroundDrawable(S.getDrawable("#660000ff", null));
                                        }
                                        if (motionEvent.getAction() == motionEvent.ACTION_UP)
                                            view.setBackgroundDrawable(S.getDrawable("#88FFFFFF", null));
                                    }

                                    return false;
                                }
                            });
                            button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    d.cancel();
                                    d.dismiss();
                                }
                            });
                            d.show();
                        }
                    return false;
                }
            });
        }
        InitView.initView(activity,new MainView(tv, new MainView.OnInit() {
            @Override
            public void onInit(final Activity ac, final int w, final int h) {
                final ProgressDialog progressDialog = new ProgressDialog(activity);
                progressDialog.setMessage("Загрузка");
                progressDialog.show();
                Thread th = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final Bitmap formul = S.getFormulBitmap(text,w,h);
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv.setImageBitmap(formul);
                                progressDialog.dismiss();
                            }
                        });
                    }
                });
                th.start();
            }
        }),item);
    }
    public static OnClickMenu createFormul(final Activity activity, final String text, final MenuItem item, final String text2){
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                createFormulView(activity,text,item,text2);
            }
        };
    }

    public static void initImage(Activity activity,Drawable drawable){
        initImage(activity,drawable,InitView.getNowMenuItem());
    }

    public static void initImage(Activity activity, final Drawable drawable,MenuItem item){
        final TouchImageView touchImageView = new TouchImageView(activity);
        MainView view = new MainView(touchImageView);
        view.setRun(new MainView.OnInit() {
            @Override
            public void onInit(Activity ac, int w, int h) {
                touchImageView.setImageDrawable(drawable);
            }
        });
        InitView.initView(activity,view,item);
    }

    public static void initImage(Activity activity,Bitmap bitmap){
        initImage(activity,bitmap,InitView.getNowMenuItem());
    }

    public static void initImage(Activity activity, final Bitmap bitmap, MenuItem item){
        final TouchImageView touchImageView = new TouchImageView(activity);
        MainView view = new MainView(touchImageView);
        view.setRun(new MainView.OnInit() {
            @Override
            public void onInit(Activity ac, int w, int h) {
                touchImageView.setImageBitmap(bitmap);
            }
        });
        InitView.initView(activity,view,item);
    }

    public static void initImage(final Activity activity, final int id){
        final TouchImageView touchImageView = new TouchImageView(activity);
        MainView view = new MainView(touchImageView);
        view.setRun(new MainView.OnInit() {
            @Override
            public void onInit(Activity ac, int w, int h) {
                try {
                    touchImageView.setImageResource(id);
                }catch (OutOfMemoryError e){
                    ac.onLowMemory();
                    try{
                        touchImageView.setImageResource(id);
                    }catch (Exception e2){
                        InitView.onBackPress(activity);
                        InitView.createSnackbarOnParent("Ошибка");
                    }
                }
            }
        });
        InitView.initView(activity,view,InitView.getNowMenuItem());
    }

    public static void initText(Activity activity,String text){
        ScrollView scrollView = new ScrollView(activity);
        final TextView tv = new TextView(activity);
        tv.setTextSize(PreferenceValue.textsizeformul);
        tv.setTextColor(Color.parseColor(PreferenceValue.colorformul));
        if (PreferenceValue.fotoformul){
            setBackground(PreferenceValue.backdrawformul);
        }else {
            setBackground(S.getDrawable(PreferenceValue.backcolorformul, PreferenceValue.gradformul));
        }
        tv.setText(text);
        scrollView.addView(tv);
        initView(activity,new MainView(scrollView),InitView.getNowMenuItem());
    }

    public static void createSnackbarOnParent(String text){
        S.createSnackbar(mainparent,text);
    }

    public static void reloadNavBar(final Activity activity){
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                final Drawable d1;
                if (PreferenceValue.havefotohead){
                    d1 = PreferenceValue.backfotoheadernavbar;
                }else {
                    d1 = S.getDrawable(PreferenceValue.backcolorheadernavbar, GradientDrawable.Orientation.TL_BR);
                }
                final Drawable d2;
                if (PreferenceValue.havefotomenu){
                    d2 = PreferenceValue.backfotonavmenu;
                }else {
                    d2 = S.getDrawable(PreferenceValue.backcolornavmenu, GradientDrawable.Orientation.TL_BR);
                }
                final NavigationView navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int[][] in = new int[][]{
                                new int[]{android.R.attr.state_checked},
                                new int[]{android.R.attr.state_enabled}

                        };
                        int[] colors = new int[]{
                                Color.parseColor(PreferenceValue.textcolornavbar),
                                Color.parseColor(PreferenceValue.textcolornavbar)
                        };
                        ColorStateList stateList = new ColorStateList(in,colors);
                        navigationView.setItemTextColor(stateList);
                    }
                });

                android.view.Menu m = navigationView.getMenu();
                /*for (int i = 0;i<m.size();i++){
                    for (int j = 0;j<m.getItem(i).getSubMenu().size();j++){
                        final TextView tv = (TextView) m.getItem(i).getSubMenu().getItem(j).getActionView();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    tv.setTextColor(Color.parseColor(PreferenceValue.textcolornavbar));
                                }catch (Exception e){

                                }
                            }
                        });

                    }
                }*/
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        navigationView.getHeaderView(0).setBackgroundDrawable(d1);
                        navigationView.setBackgroundDrawable(d2);
                    }
                });
            }
        });
        th.start();

    }

    public static MainView getNow(){
        return now;
    }

    public static ProgressDialog showLoadMessage(){
        ProgressDialog pd = new ProgressDialog(activity);
        pd.setMessage("Загрузка");
        pd.show();
        return pd;
    }

    public static void add(View view){mainparent.addView(view);}

}
