package ru.loginov.project.workspace;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.view.View;

/**
 * Created by user on 23.10.2016.
 */
public final class MainView {
    private View main = null;
    private OnInit run = null;
    private OnInitMenu menu = null;
    private OnBackPress back = null;
    private OnReload reload = null;
    private OnGetView getView = null;
    private onBackInit backInit = null;
    private MenuItem item = null;

    private boolean needreload = false;
    public MainView(@NonNull View view){
        main = view;
    }

    public MainView(@NonNull View view,OnInit onInit){
        main = view;
        run = onInit;
    }

    public MainView(@NonNull View view, OnInit onInit, MenuItem item){
        this.item = item;
        main = view;
        run = onInit;
    }

    public MainView (@NonNull View view,OnInit onInit,MenuItem item,OnInitMenu onInitMenu){
        this.main = view;
        this.run = onInit;
        this.item = item;
        this.menu = onInitMenu;
        this.back = null;
    }

    public MainView(@NonNull View view, OnInit onInit, MenuItem item,OnInitMenu onmenu,OnBackPress back){
        this.main = view;
        this.run = onInit;
        this.item = item;
        this.menu = onmenu;
        this.back = back;
    }

    public View getView(){
        return main;
    }

    public void onInit(Activity activity,int w,int h){
        if (run != null) {
            run.onInit(activity,w,h);
        }
    }

    public MainView setItem(MenuItem item){
        this.item = item;
        return this;
    }

    public MenuItem getItem(){
        return item;
    }

    public OnInit getRun(){
        return run;
    }

    public OnInitMenu getOnMenu(){
        return menu;
    }


    public void setOnMenu(OnInitMenu menu){
        this.menu = menu;
    }

    public void setView(@NonNull View view){
        main = view;
    }

    public void setRun(@NonNull OnInit init){
        run = init;
    }

    public void setBack(@NonNull OnBackPress back){
        this.back = back;
    }

    public OnGetView getV(){
        return getView;
    }

    public void setGetView(@NonNull OnGetView getView){
        this.getView = getView;
    }

    public OnReload getOnReload(){
        return reload;
    }

    public void setReload(@NonNull OnReload reload){
        this.reload = reload;
    }

    public OnBackPress getBack(){
        return back;
    }

    public void setBackInit(@NonNull onBackInit init){this.backInit = init;}

    public onBackInit getBackInit(){return this.backInit;}

    public interface OnInit{
        void onInit(Activity ac, int w, int h);
    }

    public interface onBackInit{
        void onInit(Activity activity);
    }

    public interface OnInitMenu{
        void onOpenMenu(Activity ac);
    }

    public interface OnBackPress{
        boolean onBackPress(Activity activity);
    }

    public interface OnReload{
        boolean preReload(Activity activity);

        void onReload(Activity activity);

        void postReload(Activity activity);
    }

    public interface OnGetView{
        View getView(Activity activity);
    }

    public void setNeedreload(boolean needreload1){
        this.needreload = needreload1;
    }

    public boolean isneedreload(){
        return needreload;
    }

}
