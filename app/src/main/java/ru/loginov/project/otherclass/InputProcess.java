package ru.loginov.project.otherclass;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.loginov.project.R;
import ru.loginov.project.S;
import ru.loginov.project.mainmenu.mathmenu.YEdit;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;
import ru.loginov.project.workspace.MainView;

/**
 * Created by user on 01.11.2016.
 */
public class InputProcess {
    static boolean visible = false;
    List<EditText> editTextList = new ArrayList<>();
    View output = null;
    OnClear onClear = null;
    OnExecute onExecute = null;

    public InputProcess(@NonNull EditText[] editTexts, View mainview, OnClear onClear, OnExecute execute){
        this.editTextList = Arrays.asList(editTexts);
        this.output = mainview;
        this.onClear = onClear;
        this.onExecute = execute;
    }

    public void setOnExecute(@NonNull OnExecute execute){
        this.onExecute = execute;
    }

    public MainView initView(Activity activity){
        ScrollView scrollView = new ScrollView(activity);
        LinearLayout ll = new LinearLayout(activity);
        ll.setOrientation(ll.VERTICAL);
        if (output.getParent() != null) ((LinearLayout)output.getParent()).removeAllViews();
        ll.addView(output);
        for (int i = 0;i<editTextList.size();i++){
            editTextList.get(i).setHintTextColor(Color.parseColor(PreferenceValue.hintcolorinput));
            editTextList.get(i).setTextColor(Color.parseColor(PreferenceValue.edittextcolotinput));
            ll.addView(editTextList.get(i));
        }
        RelativeLayout relativeLayout = new RelativeLayout(activity);
        Button button = new Button(activity);
        button.setText("Сброс");
        if (onClear != null) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClear.onClear(view);
                }
            });
        }
        Button button1 = new Button(activity);
        button1.setText("Выполнить");
        if (onExecute != null){
            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<String> l = new ArrayList<String>();
                    for (int i =0;i<editTextList.size();i++){
                        l.add(editTextList.get(i).getText().toString());
                    }
                    new Task().execute(l.toArray(new String[l.size()]));
                }
            });
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        button.setLayoutParams(layoutParams);
        button.setTextColor(Color.parseColor(PreferenceValue.textcolorbuttominput));
        button.setBackgroundDrawable(S.getDrawable(PreferenceValue.backcolotbuttominput,PreferenceValue.gradbackinput));
        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        button1.setLayoutParams(layoutParams1);
        button1.setTextColor(Color.parseColor(PreferenceValue.textcolorbuttominput));
        button1.setBackgroundDrawable(S.getDrawable(PreferenceValue.backcolotbuttominput,PreferenceValue.gradbackinput));
        relativeLayout.addView(button);
        relativeLayout.addView(button1);
        ll.addView(relativeLayout);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        scrollView.setLayoutParams(layoutParams3);
        scrollView.addView(ll);
        visible = true;
        final MainView mainView = new MainView(scrollView);
        mainView.setRun(new MainView.OnInit() {
            @Override
            public void onInit(Activity ac, int w, int h) {
                if (PreferenceValue.havebackfotoinput){
                    InitView.setBackground(PreferenceValue.backfotoinput);
                }else{
                    InitView.setBackground(S.getDrawable(PreferenceValue.backcolorinput,PreferenceValue.gradbackinput));
                }
            }
        });
        mainView.setBack(new MainView.OnBackPress() {
            @Override
            public boolean onBackPress(Activity activity) {
                visible = false;
                return false;
            }
        });
        mainView.setReload(new MainView.OnReload() {
            @Override
            public boolean preReload(Activity activity) {
                InitView.replaceNow(activity,mainView);
                return true;
            }

            @Override
            public void onReload(Activity activity) {

            }

            @Override
            public void postReload(Activity activity) {

            }
        });
        mainView.setGetView(new MainView.OnGetView() {
            @Override
            public View getView(Activity activity) {
                return initView(activity).getView();
            }
        });
        return mainView;
    }

    public void reloadProcess(Activity activity){
        if (visible){
            InitView.replaceNow(activity,initView(activity));
        }else{
            InitView.initView(activity,initView(activity),InitView.getNowMenuItem());
        }
    }

    public interface OnClear{
        void onClear(View view);
    }

    public interface OnExecute{
        void onExecute(String... data);
    }

    private class Task extends AsyncTask<String,Void,Void>{
        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            pd = InitView.showLoadMessage();
        }

        @Override
        protected Void doInBackground(String... strings) {
            onExecute.onExecute(strings);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            pd.dismiss();
        }
    }

}
