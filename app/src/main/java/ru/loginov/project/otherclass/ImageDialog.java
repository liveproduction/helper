package ru.loginov.project.otherclass;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * Created by user on 22.10.2016.
 */
public class ImageDialog {
    String title = null;
    String buttomtext = null;
    Bitmap image = null;
    Runnable run = null;
    Runnable postrun = null;
    public ImageDialog(@NonNull String title,@NonNull String buttomText,@NonNull Bitmap image){
        this.title = title;
        this.image = image;
        this.buttomtext = buttomText;
    }

    public void setRun(Runnable run){
        this.run = run;
    }

    public void setPostrun(Runnable postrun){
        this.postrun = postrun;
    }

    public Dialog getDialog(Activity activity){
        AlertDialog.Builder adb = new AlertDialog.Builder(activity);
        adb.setTitle(title);
        final TouchImageView iv = new TouchImageView(activity);
        final LinearLayout ll = new LinearLayout(activity);
        iv.setImageBitmap(image);
        final Button button = new Button(activity);
        ll.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        ll.setLayoutParams(lp);
        iv.setLayoutParams(lp);
        button.setLayoutParams(lp);
        button.setText(buttomtext);
        iv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (run != null){
                    run.run();
                    return true;
                }
                return false;
            }
        });
        ll.addView(iv);
        ll.addView(button);
        adb.setView(ll);
        ll.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                iv.setMaxWidth(ll.getWidth());
                button.setWidth(ll.getWidth());
            }
        });
        final Dialog d = adb.create();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               d.dismiss();
                if (postrun != null){
                    postrun.run();
                }
            }
        });
        return d;
    }
}
