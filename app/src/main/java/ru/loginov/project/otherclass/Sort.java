package ru.loginov.project.otherclass;

/**
 * Created by user on 25.10.2016.
 */
public class Sort {

    private static boolean isnumber(char a){
        switch (a){
            case '1':
                return true;
            case '2':
                return true;
            case '3':
                return true;
            case '4':
                return true;
            case '5':
                return true;
            case '6':
                return true;
            case '7':
                return true;
            case '8':
                return true;
            case '9':
                return true;
            case '0':
                return true;
            case '.':
                return true;
        }
        return false;
    }

    public static int sort(String one,String two){
        String number1 = "";
        String number2 = "";
        for (int i = 0;i<one.length();i++){
            if (isnumber(one.charAt(i))){
                number1+=one.charAt(i);
            }else{
                break;
            }
        }
        for (int i = 0;i<two.length();i++){
            if (isnumber(two.charAt(i))){
                number2+=two.charAt(i);
            }else{break;}}
        double d = Double.valueOf(number1) - Double.valueOf(number2);
        if (d == 0){
            return 0;
        }
        if (d > 0){
            return 1;
        }else{
            return  -1;
        }
    }
}