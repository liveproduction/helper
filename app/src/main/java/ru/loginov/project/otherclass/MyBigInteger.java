package ru.loginov.project.otherclass;

public class MyBigInteger {
	private String number = "-1";
	private boolean isminus = false;
	
	public MyBigInteger(){
		number = null;
	}
	
	public MyBigInteger(int a){
		if (a < 0){
			isminus = true;
		}
		number = String.valueOf(a);
	}
	
	public MyBigInteger(String str){
		
		if (isnumber(str)){
			if (isminus(str)){
				isminus = true;
			}
			number = str;
		}
		
	}
	
	
	
	public String get(){
		return delnol();
	}
	
	public MyBigInteger abs(){
		
		if (isminus){
			return new MyBigInteger(delleft(number));
		}else{
			return new MyBigInteger(number);
		}
		
	}
	
	
	/**
     * Returns a BigInteger whose value is {@code (this + b)}.
     *
     * @param  val value to be added to this BigInteger.
     * @return {@code this + b}
     */
	public MyBigInteger add(MyBigInteger b) {
		return add(b.get());
	}
	
	
	/**
     * Returns a BigInteger whose value is {@code (this + str)}.
     *
     * @param  val value to be added to this BigInteger.
     * @return {@code this + str}
     */
	public MyBigInteger add(String str){
		if (findleft(number,'-') == -1 | findleft(number,'-') == -1){
		if (findleft(str,'-') == -1 | findleft(str,'-') == -1){
		int x = 0;
		String ret = "";
		if (str.length() < number.length()){
			int i2 = number.length() - 1;
		for (int i = str.length()-1;i > -1;i--){
			int a = Integer.valueOf(String.valueOf(str.charAt(i))) + Integer.valueOf(String.valueOf(number.charAt(i2)));
			if (x != 0){
				a = a + x;
				x = 0;
			}
			if (a > 9){
				x = x + 1;
				a = a - 10;
			}
			ret = String.valueOf(a) + ret;
			i2 = i2 - 1;
		}
		}
		else if (number.length() < str.length()){
			int i2 = str.length() - 1;
			for (int i = number.length() - 1; i > -1; i--){
				int a = Integer.valueOf(String.valueOf(str.charAt(i2)));
				int b = Integer.valueOf(String.valueOf(number.charAt(i)));
				int c = a + b;
				if (x != 0){
					c = c + x;
					x = 0;
				}
				while (c > 9){
					x = x + 1;
					c = c - 10;
				}
				ret = String.valueOf(c) + ret;
				i2 = i2 - 1;
			}
			
			
		}
		else if (number.length() == str.length()){
			
			for (int i = number.length() - 1; i > -1; i--){
				int a = Integer.valueOf(String.valueOf(str.charAt(i)));
				int b = Integer.valueOf(String.valueOf(number.charAt(i)));
				int c = a + b;
				if (x != 0){
					c = c + x;
					x = 0;
				}
				while (c > 9){
					x = x + 1;
					c = c - 10;
				}
				ret = String.valueOf(c) + ret;
			}
			
		}
		if (str.length() < number.length()){
			if (x == 0){
			ret = String.valueOf(copy(number,0-1,number.length()-str.length())) + ret;
			}
			else{
				ret = new MyBigInteger(copy(number,0-1,number.length()-str.length())).add(String.valueOf(x)).get() + ret;
				x = 0;
			}
		}
		else if (number.length() < str.length()){
			if (x==0){
			ret = String.valueOf(copy(str,0-1,str.length()-number.length())) + ret;
			}
			else{
			ret = new MyBigInteger(copy(str,0-1,str.length()-number.length())).add(String.valueOf(x)).get() + ret;
			x = 0;
			}
		}
		
		
		if (x != 0){
			ret = String.valueOf(x) + ret ;
		}
		return new MyBigInteger(ret);
		}
		else{
			return minus(delleft(str),false);
		}
		}
		else{
			
			if (findleft(str,'-') == -1 | findleft(str,'-') == -1){
				return new MyBigInteger(str).minus(delleft(number));
			}
			else{
				MyBigInteger a = new MyBigInteger(delleft(number)).add(delleft(str));
				return new MyBigInteger('-' + a.get());
			}
			
		}
	}
	
	public int toint(){
		try{
			int a = Integer.valueOf(number);
			if (String.valueOf(a).equals(number)){
				return a;
			}
			else{
				if (isminus){
					return a = -2147483648;
				}else{
					return a = 2147483647;
				}
			}
		}
		 catch (Exception e) {
			 if (isminus){
					return -2147483648;
				}else{
					return 2147483647;
				}
		 }
	}
	
	public MyBigInteger pow(MyBigInteger a){
		MyBigInteger b = new MyBigInteger(number);
		if (isminus(a.get())){
			return new MyBigInteger("-1");
		}else{
			for (int i = 1;i < a.toint();i++){
				b = b.multiply(b);
			}
			return b;
		}
	}
	
	public MyBigInteger pow(int b){
		MyBigInteger a = this;
		if (b < 0){
			return new MyBigInteger("-1");
		}else if (b == 0){
			return new MyBigInteger("1");
		}
		else if (b > 0){
			for (int i = 1; i < b;i++){
				a = a.multiply(this);
			}
		}
		return a;
	}
	
	public MyBigInteger divide(MyBigInteger a){
		return divide(a.get());
	}
	
	public MyBigInteger divide(String a){
		if (a.length() < number.length()){
		if (isminus){
			if (isminus(a)){
				return new MyBigInteger(delleft(number)).divide(delleft(a));	
			}else{
				return new MyBigInteger("-" + new MyBigInteger(new MyBigInteger(delleft(number)).divide(a).get()));
			}
		}
		else{
			if (isminus(a)){
				return new MyBigInteger("-" + new MyBigInteger(new MyBigInteger(number).divide(delleft(a)).get()));
			}else{
				if (new MyBigInteger(a).equals(new MyBigInteger("0"))){
					return new MyBigInteger("0");
				}
				else{
					if (number.equals(a)){
						return new MyBigInteger("1");
						
					}else{
						MyBigInteger k = new MyBigInteger(number);
						MyBigInteger m = new MyBigInteger(a).multiply(new MyBigInteger("10"));
						MyBigInteger i = new MyBigInteger("0");
						if(k.max(m).equals(k)){
						while(k.max(m).equals(k)){
							k = k.minus(m);
							i = i.add(new MyBigInteger("10"));
						}
						m = new MyBigInteger(a);
						while(k.max(m).equals(k)){
							k = k.minus(m);
							i = i.add(new MyBigInteger("1"));
						}
						}
						else{
							m = new MyBigInteger(a);
							while(k.max(m).equals(k)){
								k = k.minus(m);
								i = i.add(new MyBigInteger("1"));
							}
						}
						return i;
					}
				}
			}
		}
		}
		else{
			return new MyBigInteger(0);
		}
		
		
	}
	
	public MyBigInteger max(MyBigInteger b) {
		return max(new MyBigInteger(number),b);
	}
	
	
	public MyBigInteger minus(MyBigInteger a){
		return minus(a.get());
	}
	
	public int getnumber(int i){
		if (number != null & i > -1){
			return Integer.valueOf(String.valueOf(number.charAt(i)));
		}
		else{
			return -1;
		}
	}
	
	public MyBigInteger min(MyBigInteger a,MyBigInteger b){
		MyBigInteger c = max(a,b);
		if (c.equals(a)){
		return b;
		}
		else if (c.equals(b)){
			return a;
		}
		else{
			return c;
		}
	}
	
	
	public MyBigInteger max(MyBigInteger a,MyBigInteger b){
		MyBigInteger ret = new MyBigInteger("�����",null);
		String a1 = a.get();
		String b1 = b.get();
		if (isminus(a1)){
			if (isminus(b1)){
				a1 = delleft(a1);
				b1 = delleft(b1);
				MyBigInteger c = min(new MyBigInteger(a1),new MyBigInteger(b1));
				if (c.equals(new MyBigInteger(a1))){
				return a;
				}
				else if (c.equals(new MyBigInteger(b1))){
					return b;
				}else{
					return c;
				}
			}
			else{
				return b;
			}
		}
		else{
			if (isminus(b1)){
				return a;
			}
			else{
				if (a.length(false) > b.length(false)){
					return a;
				}
				else if (a.length(false) < b.length(false)){
					return b;
				}
				else if (a.length(false) == b.length(false)){
					for (int i = a1.length()-1; i > -1; i--){
						if (Integer.valueOf(a1.charAt(i)) > Integer.valueOf(b1.charAt(i))){
							ret = a;
						}
						else if (Integer.valueOf(a1.charAt(i)) < Integer.valueOf(b1.charAt(i))){
							ret = b;
						}
					}
				}
			}
		}
		return ret;
	}
	
	public int length(boolean minus){
		
		if (minus){
			return number.length();
		}else{
			if (isminus){
				return delleft(number).length();
			}
			else{
				return number.length();
			}
		}
		
	}
	
	
	public boolean equals(MyBigInteger a){
		if (number.equals(a.get())){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean equals(Object obj){
		if (number.equals(obj)){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	public MyBigInteger multiply(MyBigInteger mybigint){
		if (isminus){
			if (isminus(mybigint.get())){
				return new MyBigInteger(delleft(number)).multiply(new MyBigInteger(delleft(mybigint.get())));
			}else{
				return new MyBigInteger('-' + new MyBigInteger(delleft(number)).multiply(mybigint).get());
			}
		}else{
			if (isminus(mybigint.get())){
				return new MyBigInteger('-' + new MyBigInteger(number).multiply(new MyBigInteger(delleft(mybigint.get()))).get());
			}else{
				MyBigInteger y = new MyBigInteger("0");
				MyBigInteger ret = new MyBigInteger("0");
				String myb = mybigint.get();
				String ystr = "";
				int x = 0;
				for (int i = myb.length()-1;i > -1;i--){
					for (int i2 = number.length() - 1;i2 > -1; i2--){
						int a = Integer.valueOf(String.valueOf(myb.charAt(i)));
						int b = Integer.valueOf(String.valueOf(number.charAt(i2)));
						int c = a * b;
						ystr = String.valueOf(c);
						
						for (int i3 = 1;i3 < number.length() - i2;i3++){
							ystr = ystr + "0";
							
						}
						y = y.add(ystr);
						
					}
					
					String y2 = y.get();
					y = new MyBigInteger("0");
					if (x != 0){
						y2 = String.valueOf(x) + y2;
						x = 0;
					}
					for (int i4 = 1;i4 < myb.length() - i;i4++){
						y2 = y2 + "0";
					}
					ret = ret.add(y2);
				}
				return ret;
			}
		}
		
		
	}
	
	
	public boolean iszero(){
		return iszero(number);
	}
	
	
	//��������� ������
	
	protected MyBigInteger divide2(String a){
		if (a.length() < number.length()){
		if (isminus){
			if (isminus(a)){
				return new MyBigInteger(delleft(number)).divide2(delleft(a));	
			}else{
				return new MyBigInteger("-" + new MyBigInteger(new MyBigInteger(delleft(number)).divide2(a).get()));
			}
		}
		else{
			if (isminus(a)){
				return new MyBigInteger("-" + new MyBigInteger(new MyBigInteger(number).divide2(delleft(a)).get()));
			}else{
				if (new MyBigInteger(a).equals(new MyBigInteger("0"))){
					return new MyBigInteger("0");
				}
				else{
					if (number.equals(a)){
						return new MyBigInteger("1");
						
					}else{
						MyBigInteger k = new MyBigInteger(number);
						MyBigInteger m = new MyBigInteger(a).multiply2(new MyBigInteger("10"));
						MyBigInteger i = new MyBigInteger("0");
						if(k.max(m).equals(k)){
						while(k.max(m).equals(k)){
							k = k.add2("-" + m.get2());
							i = i.add2("10");
						}
						m = new MyBigInteger(a);
						while(k.max(m).equals(k)){
							k = k.add2("-" + m.get2());
							i = i.add2("1");
						}
						}
						else{
							m = new MyBigInteger(a);
							while(k.max(m).equals(k)){
								k = k.add2("-" + m.get2());
								i = i.add2("1");
							}
						}
						return i.add2("1");
					}
				}
			}
		}
		}
		else{
			return new MyBigInteger(0);
		}
		
		
	}
	
	protected MyBigInteger multiply2(MyBigInteger mybigint){
		if (isminus){
			if (isminus(mybigint.get2())){
				return new MyBigInteger(delleft(number)).multiply(new MyBigInteger(delleft(mybigint.get2())));
			}else{
				return new MyBigInteger('-' + new MyBigInteger(delleft(number)).multiply(mybigint).get2());
			}
		}else{
			if (isminus(mybigint.get2())){
				return new MyBigInteger('-' + new MyBigInteger(number).multiply(new MyBigInteger(delleft(mybigint.get2()))).get2());
			}else{
				MyBigInteger y = new MyBigInteger("0");
				MyBigInteger ret = new MyBigInteger("0");
				String myb = mybigint.get2();
				String ystr = "";
				int x = 0;
				for (int i = myb.length()-1;i > -1;i--){
					for (int i2 = number.length() - 1;i2 > -1; i2--){
						int a = Integer.valueOf(String.valueOf(myb.charAt(i)));
						int b = Integer.valueOf(String.valueOf(number.charAt(i2)));
						int c = a * b;
						ystr = String.valueOf(c);
						
						for (int i3 = 1;i3 < number.length() - i2;i3++){
							ystr = ystr + "0";
							
						}
						y = y.add2(ystr);
						
					}
					
					String y2 = y.get();
					y = new MyBigInteger("0");
					if (x != 0){
						y2 = String.valueOf(x) + y2;
						x = 0;
					}
					for (int i4 = 1;i4 < myb.length() - i;i4++){
						y2 = y2 + "0";
					}
					ret = ret.add2(y2);
				}
				return ret;
			}
		}
		
		
	}
	
	protected MyBigInteger add2(String str){
		if (findleft(number,'-') == -1 | findleft(number,'-') == -1){
		if (findleft(str,'-') == -1 | findleft(str,'-') == -1){
		int x = 0;
		String ret = "";
		if (str.length() < number.length()){
			int i2 = number.length() - 1;
		for (int i = str.length()-1;i > -1;i--){
			int a = Integer.valueOf(String.valueOf(str.charAt(i))) + Integer.valueOf(String.valueOf(number.charAt(i2)));
			if (x != 0){
				a = a + x;
			}
			if (a > 9){
				x = x + 1;
				a = a - 10;
			}
			ret = String.valueOf(a) + ret;
			i2 = i2 - 1;
		}
		}
		else if (number.length() < str.length()){
			int i2 = str.length() - 1;
			for (int i = number.length() - 1; i > -1; i--){
				int a = Integer.valueOf(String.valueOf(str.charAt(i2)));
				int b = Integer.valueOf(String.valueOf(number.charAt(i)));
				int c = a + b;
				if (x != 0){
					c = c + x;
					x = 0;
				}
				while (c > 9){
					x = x + 1;
					c = c - 10;
				}
				ret = String.valueOf(c) + ret;
				i2 = i2 - 1;
			}
			
			
		}
		else if (number.length() == str.length()){
			
			for (int i = number.length() - 1; i > -1; i--){
				int a = Integer.valueOf(String.valueOf(str.charAt(i)));
				int b = Integer.valueOf(String.valueOf(number.charAt(i)));
				int c = a + b;
				if (x != 0){
					c = c + x;
					x = 0;
				}
				while (c > 9){
					x = x + 1;
					c = c - 10;
				}
				ret = String.valueOf(c) + ret;
			}
			
		}
		if (str.length() < number.length()){
			if (x == 0){
			ret = String.valueOf(copy(number,0-1,number.length()-str.length())) + ret;
			}
			else{
				ret = new MyBigInteger(copy(number,0-1,number.length()-str.length())).add(String.valueOf(x)).get2() + ret;
				x = 0;
			}
		}
		else if (number.length() < str.length()){
			if (x==0){
			ret = String.valueOf(copy(str,0-1,str.length()-number.length())) + ret;
			}
			else{
			ret = new MyBigInteger(copy(str,0-1,str.length()-number.length())).add(String.valueOf(x)).get2() + ret;
			x = 0;
			}
		}
		
		
		if (x != 0){
			ret = String.valueOf(x) + ret ;
		}
		return new MyBigInteger(ret);
		}
		else{
			return minus(delleft(str),false);
		}
		}
		else{
			
			if (findleft(str,'-') == -1 | findleft(str,'-') == -1){
				return new MyBigInteger(str).minus(delleft(number));
			}
			else{
				MyBigInteger a = new MyBigInteger(delleft(number)).add2(delleft(str));
				return new MyBigInteger('-' + a.get2());
			}
			
		}
	}
	
	protected String get(boolean bool){
		if (bool){
			return get();
		}
		else{
			return number;
		}
	}
	
	private MyBigInteger(String str, Void v){
		number = str;
	}
	
	private boolean iszero(String str){
		if (str == null){
			return true;
		}else
		{
			str = get(false);
			if (str.length() > 1){
				return false;
			}
			else{
				if (str.length() == 1){
					if (str == "0"){
						return true;
					}
					else{
						if (str.charAt(0) == '0'){
							return true;
						}
						else{
							return false;
						}
					}
				}else{
					return false;
				}
			}
		}
	}
	
	private MyBigInteger minus(String str){
		return minus(str,false);
	}
	
	private MyBigInteger minus(String str,boolean minus){
		if (!iszero(str) & !iszero(number)){
		if ((!isminus) & (!isminus(str))){
		if (str != null & str != ""){
		if (minus){
			
			if (number.length() < str.length()){
				System.out.println("Error: numbers may be not inisialised");
				return new MyBigInteger("-" + new MyBigInteger(str).minus(number).get());
			}
			else if (number.length() == str.length()){
				MyBigInteger a = new MyBigInteger(number);
				MyBigInteger b = new MyBigInteger(str);
				MyBigInteger c = max(a,b);
				if (c == a){
					String ret = "";
					int x = 0;
					for (int i = number.length()-1;i > -1;i--){
						int a1 = Integer.valueOf(String.valueOf(number.charAt(i)));
						int b1 = Integer.valueOf(String.valueOf(str.charAt(i)));
						int c1 = a1-b1;
						if (x != 0){
							c1 = c1 - x;
							x = 0;
						}
						while (c1 < 0){
							x = x + 1;
							c1 = c1 + 10;
						}
						ret = String.valueOf(c1) + ret;
					}
					ret = '-' + ret;
					return new MyBigInteger(ret);
				}
				else{
					return a.minus(str,false);
				}
			}
			else if (number.length() > str.length()){
				String ret = "";
				int x = 0;
				int i;
				int i2 = str.length()-1;
				for (i = number.length()-1;i > -1;i--){
					int a1 = Integer.valueOf(String.valueOf(number.charAt(i)));
					int b1;
					try{
					 b1 = Integer.valueOf(String.valueOf(str.charAt(i2)));
					}
					catch(Exception e){
						b1 = 0;
					}
					int c1 = a1-b1;
					if (x != 0){
						c1 = c1 - x;
						x = 0;
					}
					while (c1 < 0){
						x = x + 1;
						c1 = c1 + 10;
					}
					ret = String.valueOf(c1) + ret;
					i2 = i2 - 1;
				}
				while (x != 0){
					int a1 = Integer.valueOf(String.valueOf(number.charAt(i)));
					int c1 = a1 - x;
					x = 0;
					while (c1 < 0){
						x = x + 1;
						c1 = c1 + 10;
					}
					i = i + 1;
					ret = String.valueOf(c1) + ret;
				}
				try{
					ret = copy(number,-1,i) + ret;
				}
				catch(Exception e){
					
				}
				ret = '-' + ret;
				return new MyBigInteger(ret);
			}
			
		}
		else{
		if (number.length() < str.length()){
			return new MyBigInteger(str).minus(number, true);
		}
		else if(number.length() > str.length()){
			String ret = "";
			int x = 0;
			int i;
			int i2 = str.length()-1;
			for (i = number.length()-1;i > -1;i--){
				int a1 = Integer.valueOf(String.valueOf(number.charAt(i)));
				int b1;
				try{
				 b1 = Integer.valueOf(String.valueOf(str.charAt(i2)));
				}
				catch(Exception e){
					b1 = 0;
				}
				int c1 = a1-b1;
				if (x != 0){
					c1 = c1 - x;
					x = 0;
				}
				while (c1 < 0){
					x = x + 1;
					c1 = c1 + 10;
				}
				ret = String.valueOf(c1) + ret;
				i2 = i2 - 1;
			}
			while (x != 0){
				int a1 = Integer.valueOf(String.valueOf(number.charAt(i)));
				int c1 = a1 - x;
				x = 0;
				while (c1 < 0){
					x = x + 1;
					c1 = c1 + 10;
				}
				i = i + 1;
				ret = String.valueOf(c1) + ret;
			}
			try{
				ret = copy(number,-1,i) + ret;
			}
			catch(Exception e){
				
			}
			return new MyBigInteger(ret);
		}
		else if(number.length() == str.length()){
			MyBigInteger a = new MyBigInteger(number);
			MyBigInteger b = new MyBigInteger(str);
			MyBigInteger c = max(a,b);
			if (a.equals(b)){
				return new MyBigInteger("0");
			}
			if (c.equals(a)){
				String ret = "";
				int x = 0;
				for (int i = number.length()-1;i > -1;i--){
					int a1 = Integer.valueOf(String.valueOf(number.charAt(i)));
					int b1 = Integer.valueOf(String.valueOf(str.charAt(i)));
					int c1 = a1-b1;
					if (x != 0){
						c1 = c1 - x;
						x = 0;
					}
					while (c1 < 0){
						x = x + 1;
						c1 = c1 + 10;
					}
					ret = String.valueOf(c1) + ret;
				}
				return new MyBigInteger(ret);
			}
			else if (c.equals(b)){
				return new MyBigInteger("-" + b.minus(a).get());
			}else{
				return new MyBigInteger("������",null);
			}
		}
		
		
		
		}
		}
		else{
			return new MyBigInteger("0");
		}
		return null;
		}
		else{
			if (isminus){
				if (isminus(str)){
					MyBigInteger c = new MyBigInteger(delleft(number)).minus(delleft(str));
					if (isminus(c.get())){
						return new MyBigInteger(delleft(c.get()));
					}
					else{
						return new MyBigInteger('-' + c.get());
					}
					
				}
				else{
					MyBigInteger c = new MyBigInteger(delleft(number)).add(str);
					return new MyBigInteger('-' + c.get());
				}
			}
			else{
				if (isminus(str)){
					return new MyBigInteger(number).add(delleft(str));
				}
				else{
					return null;
				}
			}
		}
		
	}
		else{
			return new MyBigInteger("0");
		}
		
		
	}
	
	private String delleft(String str){
		String ret;
		ret = "";
		for (int i = 1;i<str.length();i++){
			ret = ret + str.charAt(i);
		}
		return ret;
	}
	
	private boolean isminus(String str){
		if (str.charAt(0) == '-' | str.charAt(0) == '-'){
			return true;
		}
		else{
			return false;
		}
	}
	
	private String copy(String str,int on,int to){
		String ret;
		ret = "";
		if ((on > to)|(str.length() < to)){
			return "Error";
		}
		else{
		for (int i = on+1;i<to;i++){
			ret = ret + str.charAt(i);
		}
		return ret;
		}
	}
	
	
	private String delprob(String str){
		String ret;
		ret = "";
		for (int i = 0;i<str.length();i++){
			if (str.charAt(i) != ' '){
				ret = ret + str.charAt(i);
			}
		}
		return ret;
	}
	
	private int findleft(String str,char t){
		int u;
		u = -8;
		for (int i = 0;i<str.length();i++){
			if (str.charAt(i) == t){
				u = i;
			}
		}
		if (u == -8){
			return -1;
		}
			else{
				return u;
			}
	}
	
	private int findrigth(String str,char t){
		int u;
		u = -8;
		for (int i = str.length()-1;i > -1; i--){
			if (str.charAt(i) == t){
				u = i;
			}
		}
		if (u == -8){
			return -1;
		}
		else{
			return u;
		}
	}
	
	 private boolean isnumber(String str) {
	        boolean bool;
	        char[] c = str.toCharArray();
	        bool = false;
	        for (int i = 0; i < c.length; i++) {
	            if ((c[i] == '1') | (c[i] == '2') | (c[i] == '3') | (c[i] == '4') | (c[i] == '5') | (c[i] == '6') | (c[i] == '7') | (c[i] == '8') | (c[i] == '9') | (c[i] == '0') | (c[i] == '.') | (c[i] == ',') | (c[i] == '-') | (c[i] == '-')) {
	                if(findleft(str,',') == findrigth(str,',')){
	                    if (findleft(str,'.') == findrigth(str,'.')){
	                        if (findleft(str,'-') == findrigth(str,'-')){//����
	                        	if (findleft(str,'-') != -1){
	                        		if (findleft(str,'-') == 0){
	                        			bool = true;
	                        		}
	                        		else{
	                        			return false;
	                        		}
	                        	}else{
	                        		if (findleft(str,'-') == findrigth(str,'-')){ //�����
	                        			if (findleft(str,'-') != -1){
	                        				if (findleft(str,'-') == 0){
	                        					bool = true;
	                        				}
	                        				else{
	                        					return false;
	                        				}
	                        			}else{
	                        				bool = true;
	                        			}
	                            	}
	                        	}
	                        }
	                    }
	                }
	            }
	        }
	        return bool;
	    }
	 
	 private String invers(String str){
	    	String ret = "";
	    	for (int i = 0; i < str.length();i++){
	    		ret = str.charAt(i) + ret;
	    	}
	    	return ret;
	    }
	 
	 private String delnol(){
		if (isminus){
			String str = delleft(number);
			number = "-" + new MyBigInteger(str).delnol();
			return number;
		}else{
			while (number.charAt(0) == '0' & number.length() > 1){
				number = delleft(number);
			}
			return number;
		}
	 }

	public String get2() {
		return number;
	}
	 

}
