package ru.loginov.project.otherclass;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.widget.TextView;
import android.widget.Toast;

import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 03.11.2016.
 */
public class MyToast extends Toast{
    public static Context context = null;

    public static void init(Context context){
        MyToast.context = context;
    }

    public MyToast(Context context) {
        super(context);
    }


    public static Toast makeText(String text){
        Toast toast = new Toast(context);
        Spannable text2 = new SpannableString(text);
        text2.setSpan(new StyleSpan(2),0,text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        text2.setSpan(new ForegroundColorSpan(Color.parseColor(PreferenceValue.colortexttoast)),0,text.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        /*ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(S.getFormulBitmap(text,-1,-1,Color.parseColor("#FF0000"),20,false,null,null,null));*/
        TextView tv = new TextView(context);
        tv.setText(text2);
        toast.setView(tv);
        toast.setDuration(Toast.LENGTH_SHORT);
        return toast;
    }

    public static void show(String text) {
        InitView.createSnackbarOnParent(text);
    }
}
