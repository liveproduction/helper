package ru.loginov.project.otherclass;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import ru.loginov.project.S;
import ru.loginov.project.menuclass.Menu;
import ru.loginov.project.menuclass.MenuAdapter;
import ru.loginov.project.preference.PreferenceValue;

/**
 * Created by user on 20.10.2016.
 */
public class ListDialog {
    View v = null;
    String title = null;
    Menu menu = null;
    boolean dis = true;
    public ListDialog(String title,Menu menu){
        this.title = title;
        this.menu = menu;
    }

    public void setDis(boolean dis){
        this.dis = dis;
    }

    public Dialog getDialog(Activity activity){
        AlertDialog.Builder adb = new AlertDialog.Builder(activity);
        adb.setTitle(title);
        final ListView lv = new ListView(activity);
        MenuAdapter adapter = new MenuAdapter(activity,menu, PreferenceValue.heightadapter,15);
        adapter.setTextSetting("#000000","#FFFFFF",null);
        adapter.setOnTouchListnerRl(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == motionEvent.ACTION_DOWN)
                if (view != null){
                    v = view;
                    view.setBackgroundDrawable(S.getDrawable("#660000bb",null));
                }
                return false;
            }
        });
        lv.setAdapter(adapter);
        adb.setView(lv);
        final Dialog d = adb.create();
        lv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    return true;
                }
                if (motionEvent.getAction() == motionEvent.ACTION_UP){
                    if(v!= null){
                        v.setBackgroundDrawable(S.getDrawable("#FFFFFF",null));
                    }
                }
                return false;
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                menu.getButton(i).getRunnable().run(adapterView,view,i,l,menu.getButton(i));
                if (dis){
                    d.cancel();
                    d.dismiss();
                }else {
                    d.hide();
                }
            }
        });
        return d;
    }
}
