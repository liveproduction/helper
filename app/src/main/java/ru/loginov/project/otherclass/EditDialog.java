package ru.loginov.project.otherclass;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.loginov.project.S;
import ru.loginov.project.preference.PreferenceValue;

/**
 * Created by user on 18.10.2016.
 */
public class EditDialog {
    String title = null;
    String buttomtext = null;
    String edittexthint = null;
    String edittext = null;
    public EditDialog(String title, String buttomtext, String edittexthint,String edittext){
        this.title = title;
        this.buttomtext = buttomtext;
        this.edittexthint = edittexthint;
        this.edittext = edittext;
    };

    public onEnded runnable = null;
    public String output =null;
    public Dialog getDialog(final Activity context){
        final AlertDialog.Builder adb = new AlertDialog.Builder(context);
        final LinearLayout rl = new LinearLayout(context);
        rl.setOrientation(LinearLayout.VERTICAL);
        final EditText et = new EditText(context);
        final Button bt = new Button(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        rl.setLayoutParams(lp);
        rl.addView(et);
        rl.addView(bt);
        et.setLayoutParams(lp);
        bt.setLayoutParams(lp);
        bt.setText(buttomtext);
        bt.setBackgroundDrawable(S.getDrawable(PreferenceValue.coloredittextbuttom, GradientDrawable.Orientation.TL_BR));
        et.setHint(edittexthint);
        if (edittext != null){
            et.setText(edittext);
        }
        adb.setView(rl);
        adb.setTitle(title);
        rl.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                et.setWidth(rl.getWidth());
                bt.setWidth(rl.getWidth());
            }
        });

        final Dialog d = adb.create();
        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView et, int i, KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    hideKeyboard(context);
                    et.setSelected(false);
                    if (!et.getText().toString().equals("") & et.getText() != null){
                        output = et.getText().toString();
                        d.dismiss();
                        if (runnable != null){
                            runnable.run(context);
                        }
                    }else {
                        d.dismiss();
                    }
                    return true;
                } else {
                    Log.w("ru.loginov.project", keyEvent.toString());
                    return false;
                }
            }
        });
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(context);
                et.setSelected(false);
                if (!et.getText().toString().equals("") & et.getText() != null){
                    output = et.getText().toString();
                    d.dismiss();
                    if (runnable != null){
                        runnable.run(context);
                    }
                }else {
                    d.dismiss();
                }
            }
        });
        return d;
    }

    public void setRunnable(onEnded runnable){
        this.runnable = runnable;
    }

    public String getOutput(){
        return output;
    }

    private void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
    }

    public interface onEnded{
        public void run(Activity activity);
    }
}
