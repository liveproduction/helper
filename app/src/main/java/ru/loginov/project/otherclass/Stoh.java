package ru.loginov.project.otherclass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Stoh {
    private static boolean prime(int a){
        int n_0 = (int)Math.sqrt(a);
        for (int i = 2; i <= n_0; ++i){
            if (a % i == 0) return false;
        }
        return true;
    }

    private static int gcd(int a, int b){
        if (b == 0) return a;
        return gcd(b, a % b);
    }


    public static double func(int n){
        if (prime(n)){
            ArrayList<Integer> h = new ArrayList<Integer>();
            h.add(1);
            while (h.size() < n-1){
                int local = 2 * h.get(h.size() - 1) % n;
                if (h.indexOf(local) < 0 && local < n){
                    h.add(local);
                }else break;
            }

            Integer[] tmp = h.toArray(new Integer[h.size()]);
            Arrays.sort(tmp);
            h = new ArrayList<Integer>(Arrays.asList(tmp));
            double R = 0;
            for (int i = 1; i < h.size(); i++) {
                R += Math.pow(h.get(i) - h.get(i - 1), 2);
            }
            if (h.get(h.size() - 1) == n - 2 && h.get(0) == 1) {
                R += 1;
            } else {
                int local = n - 1 - h.get(h.size() - 1) + h.get(0);
                R += Math.pow(local == 0 ? n-1 : local, 2);
            }
            return R / (n-1) / (n-1) * h.size();

        }else {
            ArrayList<Integer> v = new ArrayList<Integer>();
            for (int i = 1; i < n; i++) {
                if (gcd(n, i) == 1) v.add(i);
            }

            ArrayList<Integer> h = new ArrayList<Integer>();
            h.add(v.get(0));
            while (h.size() < n - 1) {
                int local = 2 * h.get(h.size() - 1) % n;
                if (h.indexOf(local) < 0 && v.indexOf(local) > -1) {
                    h.add(local);
                } else break;
            }

            /*Integer[] tmp = h.toArray(new Integer[h.size()]);
            Arrays.sort(tmp);
            h = new ArrayList<Integer>(Arrays.asList(tmp));*/
            Collections.sort(h);

            double R = 0;
            for (int i = 1; i < h.size(); i++) {
                R += Math.pow(v.indexOf(h.get(i)) - v.indexOf(h.get(i - 1)), 2);
            }
            if (v.indexOf(h.get(h.size() - 1)) == v.size() - 1 && v.indexOf(h.get(0)) == 0) {
                R += 1;
            } else {
                int local = v.size() - v.indexOf(h.get(h.size() - 1)) + v.indexOf(h.get(0));
                R += Math.pow(local == 0 ? v.size() : local, 2);
            }
            return R / v.size() / v.size() * h.size();
        }
    }
}
