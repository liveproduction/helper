package ru.loginov.project.menuclass;

import android.view.View;
import android.widget.AdapterView;

/**
 * Created by user on 18.10.2016.
 */
public interface OnClickMenu {

    void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu);

}
