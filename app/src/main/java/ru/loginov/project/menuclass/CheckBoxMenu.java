package ru.loginov.project.menuclass;

/**
 * Created by user on 20.10.2016.
 */
public class CheckBoxMenu extends ButtonMenu {
    private boolean isChecked = false;
    public CheckBoxMenu(String name, OnClickMenu runnable,boolean isOn) {
        super(name, runnable);
        this.setChecked(isOn);
    }

    public boolean isChecked(){
        return isChecked;
    }

    public void setChecked(boolean checked){
        isChecked = checked;
    }

    @Override
    public boolean isCheckBox() {
        return true;
    }
}
