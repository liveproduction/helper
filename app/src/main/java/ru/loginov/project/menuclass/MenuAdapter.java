package ru.loginov.project.menuclass;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import ru.loginov.project.S;
import ru.loginov.project.preference.PreferenceValue;

/**
 * Created by user on 19.10.2016.
 */
public class MenuAdapter extends BaseAdapter {
    static int h = 1;
    int textsize = 20;
    Menu menu = null;
    List<String> l =new ArrayList<>();
    LayoutInflater lInflater;
    Context context;
    String colortext = PreferenceValue.colormenutext;
    GradientDrawable.Orientation orientationtext = PreferenceValue.gradorientationadapter;
    String backgroudcolor = PreferenceValue.backcolormenu;
    View.OnTouchListener listener = null;


    public MenuAdapter(Context context,Menu m,int h,int textsize) {
        menu = m;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        l = menu.getNames();
        this.context = context;
        this.h = h;
        this.textsize = textsize;
    }

    public void setTextSetting(String colortext, String backcolor, GradientDrawable.Orientation orientation){
        this.colortext = colortext;
        this.backgroudcolor = backcolor;
        this.orientationtext = orientation;
    }

    public void setOnTouchListnerRl(View.OnTouchListener listnerRl){
        this.listener = listnerRl;
    }

    public void setTextSize(int textH){
        this.textsize = textH;
    }

    @Override
    public int getCount() {
        return l.size();
    }

    @Override
    public Object getItem(int i) {
        return l.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, final ViewGroup viewGroup) {
        RelativeLayout rl = new RelativeLayout(context);
        rl.setMinimumHeight(h);
        if (!(menu.getButton(i) instanceof CheckBoxMenu)) {
            MenuTextView tv = new MenuTextView(context);
            tv.setText(l.get(i));
            tv.setTextSize(textsize);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            layoutParams.setMargins(40,0,0,0);
            tv.setLayoutParams(layoutParams);
            tv.setTextColor(Color.parseColor(colortext));
            if (!PreferenceValue.fotoonback) {
                if (menu.getButton(i).isEnabled()) {
                    rl.setBackgroundDrawable(S.getDrawable(backgroudcolor, orientationtext));
                } else {
                    rl.setBackgroundDrawable(S.getDrawable("#88222222", null));
                }
            }else{
                try {
                    if (menu.getButton(i).isEnabled()) {
                        rl.setBackgroundDrawable(PreferenceValue.fotobackh);
                    }else{
                        rl.setBackgroundDrawable(S.getDrawable("#88222222", null));
                    }
                }catch (Exception e){
                    if (menu.getButton(i).isEnabled()) {
                        rl.setBackgroundDrawable(S.getDrawable(backgroudcolor, orientationtext));
                    } else {
                        rl.setBackgroundDrawable(S.getDrawable("#88222222", null));
                    }
                }
            }
            //tv.setMinHeight(10);
            //tv.setHeight(hend);
            rl.addView(tv);
        }else{
            CheckBox checkBox = new CheckBox(context);
            checkBox.setEnabled(false);
            checkBox.setText(l.get(i));
            checkBox.setTextColor(Color.parseColor(colortext));
            checkBox.setTextSize(textsize);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            layoutParams.setMargins(40,0,0,0);

            checkBox.setLayoutParams(layoutParams);
            if (!PreferenceValue.fotoonback) {
                rl.setBackgroundDrawable(S.getDrawable(backgroudcolor, orientationtext));
            }else{
                try {
                    rl.setBackgroundDrawable(PreferenceValue.fotobackh);
                }catch (Exception e){
                    rl.setBackgroundDrawable(S.getDrawable(backgroudcolor, orientationtext));
                }
            }
            checkBox.setChecked(((CheckBoxMenu) menu.getButton(i)).isChecked());
            checkBox.setMinHeight(10);
            //checkBox.setHeight(h);
            rl.addView(checkBox);

        }
        if (listener != null) rl.setOnTouchListener(listener);
        return rl;
    }
}
