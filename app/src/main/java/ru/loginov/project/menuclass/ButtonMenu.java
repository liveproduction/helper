package ru.loginov.project.menuclass;

/**
 * Created by user on 18.10.2016.
 */
public class ButtonMenu {
    private OnClickMenu runnable;
    private String name;
    private boolean isEnabled = true;
    public ButtonMenu(String name,OnClickMenu runnable){
        this.name = name;
        this.runnable = runnable;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public OnClickMenu getRunnable(){
        return runnable;
    }

    public boolean isEnabled(){
        return isEnabled;
    }

    public void setEnabled(boolean enable){
        isEnabled = enable;
    }

    public void setRunnable(OnClickMenu runnable){
        this.runnable = runnable;
    }

    public boolean isCheckBox(){
        return false;
    }
}
