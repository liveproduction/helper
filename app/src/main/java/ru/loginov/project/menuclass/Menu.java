package ru.loginov.project.menuclass;


import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;

/**
 * Created by user on 18.10.2016.
 */
public class Menu {
    private List<ButtonMenu> buttons = new ArrayList<>();
    private List<OnClickMenu> dom = new ArrayList<>();
    private String toolbartitle = null;

    public Menu(@NonNull List<ButtonMenu> buttonMenus){
        buttons = buttonMenus;
    }

    public Menu(@NonNull ButtonMenu[] buttonMenus){
        buttons = Arrays.asList(buttonMenus);
    }

    public List<ButtonMenu> getButtoms(){
        return buttons;
    }

    public void addButto(int index,ButtonMenu buttonMenu){
        if (index > -1){
            buttons.add(index,buttonMenu);
        }else{
            buttons.add(buttonMenu);
        }
    }

    public void addButtom(ButtonMenu buttonMenu){
        addButto(-1,buttonMenu);
    }

    public ButtonMenu getButton(int index){
        return buttons.get(index);
    }

    public void onClick(AdapterView<?> adapterView, View view, int i, long l){
        if (buttons.get(i).isEnabled()) {
            if (buttons.get(i).getRunnable() != null) {
                buttons.get(i).getRunnable().run(adapterView, view, i, l, buttons.get(i));
            }else{
                InitView.createSnackbarOnParent("NULL ITEM");
            }
            if (dom.size()>0){
                for (int j = 0;j<dom.size();j++){
                    dom.get(j).run(adapterView,view,i,l,buttons.get(i));
                }
            }
        }
    }



    public void addrun(OnClickMenu a){
        dom.add(a);
    }

    public void removerun(OnClickMenu a){
        dom.remove(a);
    }

    public void removerun(int a){
        dom.remove(a);
    }

    public List<String> getNames(){
        List<String> ret = new ArrayList<>();
        for (int i = 0;i<buttons.size();i++){
            ret.add(buttons.get(i).getName());
        }
        return ret;
    }

    public Menu setTitle(String title){
        this.toolbartitle = title;
        return this;
    }

    public String getToolbarTitle(){
        return toolbartitle;
    }

    public View getView(Activity activity){
        MenuView view = new MenuView(activity,this);
        view.setAdapter(new MenuAdapter(activity,this, PreferenceValue.heightadapter, PreferenceValue.htextadapter));
        return view;
    }

    public OnClickMenu tothis(final Activity activity) {
        final Menu menu = this;
        return new OnClickMenu() {
            @Override
            public void run(AdapterView<?> adapterView, View view, int i, long l, ButtonMenu buttonMenu) {
                InitView.initMenu(activity,menu,InitView.getNowMenuItem());
            }
        };

    }
}
