package ru.loginov.project.menuclass;

import android.content.Context;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;

import ru.loginov.project.S;
import ru.loginov.project.preference.PreferenceValue;

/**
 * Created by user on 19.10.2016.
 */
public class MenuView extends ListView {
    private static float y1,y2;
    public int h = 0;
    public static View v = null;
    public static ButtonMenu now = null;
    private Menu menu = null;
    public MenuView(Context context,Menu menu) {
        super(context);
        setCacheColorHint(0);
        this.menu = menu;
    }

    public void setDivide(String color,int h){
        this.setDivider(S.getDrawable(color,PreferenceValue.gradordivide));
        this.setDividerHeight(h);
    }

    public int getH(){
        return h;
    }

    public Menu getMenu(){
        return menu;
    }

    @Override
    protected void drawableStateChanged() {
        return;
    }

    @Override
    public boolean performItemClick(View view, int position, long id) {
        menu.onClick(this,view,position,id);
        return super.performItemClick(view, position, id);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == ev.ACTION_DOWN){
            View view = getV(ev);
            y1 = ev.getRawY();
            if (view != null){
                now = menu.getButton(this.getPositionForView(view));
                v = view;
                if (!PreferenceValue.fotoonclick) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        view.setBackground(S.getDrawable(PreferenceValue.colorontouchmenu, PreferenceValue.gradonclickadapter));
                    }else {
                        view.setBackgroundDrawable(S.getDrawable(PreferenceValue.colorontouchmenu, PreferenceValue.gradonclickadapter));
                    }
                }else{
                    try{
                        view.setBackgroundDrawable(PreferenceValue.fotoclickh);
                    }catch (Exception e){
                        view.setBackgroundDrawable(S.getDrawable(PreferenceValue.colorontouchmenu, PreferenceValue.gradonclickadapter));
                    }
                }
            }

        }else
        if (ev.getAction() == ev.ACTION_UP){
            h = this.getFirstVisiblePosition();
            y2 = ev.getRawY();
            if (v != null){
                if (!PreferenceValue.fotoonback) {
                    if (now.isEnabled()) {
                        v.setBackgroundDrawable(S.getDrawable(PreferenceValue.backcolormenu, PreferenceValue.gradorientationadapter));
                    } else {
                        v.setBackgroundDrawable(S.getDrawable("#88222222", null));
                    }
                }else{
                    try {
                        if (now.isEnabled()) {
                            v.setBackgroundDrawable(PreferenceValue.fotobackh);
                        }else{
                            v.setBackgroundDrawable(S.getDrawable("#88222222", null));
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                        if (now.isEnabled()) {
                            v.setBackgroundDrawable(S.getDrawable(PreferenceValue.backcolormenu, PreferenceValue.gradorientationadapter));
                        } else {
                            v.setBackgroundDrawable(S.getDrawable("#88222222", null));
                        }
                    }
                }
                View view = getV(ev);
                if (v.equals(view)){
                    if (Math.abs(y2-y1) < 20) {
                        if (menu.getButton(this.getPositionForView(v)).isCheckBox()) {
                            menu.onClick(this, view, this.getPositionForView(view), view.getId());
                        }
                    }
                    v = null;
                }
            }
           // View c = this.getChildAt(0);
            //h = -c.getTop() + this.getFirstVisiblePosition() * c.getHeight();

        }
        return super.dispatchTouchEvent(ev);
    }

    public View getV(MotionEvent ev){
        View d = null;
        int childCount = this.getChildCount();
        float x = ev.getRawX();
        float y = ev.getRawY();
        View child;
        for (int i = 0;i<childCount;i++) {
            child = this.getChildAt(i);
            if (isPointInsideView(x,y,child)){
                d = child;
                break;
            }
        }
        return d;
    }

    private static boolean isPointInsideView(float x, float y, View view){
        int location[] = new int[2];
        view.getLocationOnScreen(location);
        int viewX = location[0];
        int viewY = location[1];

        //point is inside view bounds
        if(( x > viewX && x < (viewX + view.getWidth())) &&
                ( y > viewY && y < (viewY + view.getHeight()))){
            return true;
        } else {
            return false;
        }
    }

}
