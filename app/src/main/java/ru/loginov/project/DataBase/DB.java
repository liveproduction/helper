package ru.loginov.project.DataBase;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Loginov Ilya on 21.09.2017.
 * Класс для обработки запросов к базе данных
 */

public class DB {
    private static Activity activity = null;

    //Инициализация Activity для выполнения некоторых команд в главном потоке
    public static void setActivity(Activity activity){
        DB.activity = activity;
    }

    /**
     * Создание базы данных
     * Unusing method
     */
    public static void createDB(){
        if (activity != null){
            final DBHelper dbHelper = new DBHelper(activity);
            final SQLiteDatabase db = dbHelper.getWritableDatabase();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dbHelper.onCreate(db);
                    db.close();
                }
            });
        }
    }

    /**
     * Обновление базы данных
     * Unusing method
     */
    public static void updateDB(){
        if(activity != null){
            final DBHelper dbHelper = new DBHelper(activity);
            final SQLiteDatabase db = dbHelper.getWritableDatabase();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dbHelper.onUpgrade(db,0,0);
                    db.close();
                }
            });
        }
    }

    /**
     * Извлечь строку из базы данных
     * @param table Таблица
     * @param key Ключ
     * @return Возвращает null если нету такого ключа, иначе вовращает значение.
     */
    public static String getString(String table, String key){
        DBHelper dbHelper = new DBHelper(activity);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(table, null, null, null, null, null, null);
        if (c.moveToFirst()) {
            if (c.getColumnIndex(key)<0){
                c.close();
                db.close();
                return null;
            }
            String ret = c.getString(c.getColumnIndex(key));
            c.close();
            db.close();
            return ret;
        }
        c.close();
        db.close();
        return null;
    }

    /**
     * Извлекает значение float из базы данных
     * @param table Таблица
     * @param key Ключ
     * @return Возвращает значение или -1F
     */
    public static float getFloat(String table,String key){
        DBHelper dbHelper = new DBHelper(activity);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(table, null, null, null, null, null, null);
        if (c.moveToFirst()){
            if (c.getColumnIndex(key) > -1){
                float ret = c.getFloat(c.getColumnIndex(key));
                c.close();
                db.close();
                return ret;
            }else{
                c.close();
                db.close();
                return -1F;
            }
        }
        c.close();
        db.close();
        return -1F;
    }

    /**
     * Помещает значение float в базу данных
     * @param table Таблица
     * @param key Ключ
     * @param value Значение
     * @return True если успешно записано, иначе false
     */
    public static boolean setFloat(final String table, final String key, final float value){
        final SQLiteDatabase db = new DBHelper(activity).getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(key,value);
        final Cursor c = db.query(table, null, null, null, null, null, null);
        if (c.moveToFirst()){
            if (c.getColumnIndex(key) >-1){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        db.execSQL("update " + table + " set " + key + " = " + value + ";");
                        c.close();
                        db.close();
                    }
                });
                return true;
            }else{
                c.close();
                addKey(table,key,"float");
                return setFloat(table,key,value);
            }
        }else{
            c.close();
            addValue(db,table,key,value);
        }
        return false;
    }

    /**
     * Помещает строку в базу данных
     * @param table Таблица
     * @param key Ключ
     * @param string Значение
     * @return True если успешно записано, иначе false
     */
    public static boolean setString(final String table, final String key, final String string){
        DBHelper dbHelper = new DBHelper(activity);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(key,string);
        final Cursor c = db.query(table, null, null, null, null, null, null);
        if (c.moveToFirst()) {
            if (c.getColumnIndex(key) > -1) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        db.execSQL("update " + table + " set " + key + " = \"" + string + "\";");
                        c.close();
                        db.close();
                    }
                });
                return true;
            }else{
                c.close();
                addKey(table,key,"String");
                return setString(table,key,string);
            }
        }else{
            c.close();
            addValue(db,table,key,string);
        }
        return false;
    }

    /**
     * Извлечение integer из базы данных
     * @param table Таблица
     * @param key Ключ
     * @return Либо значение, либо -1
     */
    public static int getInteger(String table,String key){
        DBHelper dbHelper = new DBHelper(activity);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(table, null, null, null, null, null, null);
        if (c.moveToFirst()) {
            if (c.getColumnIndex(key) < 0){
                addKey(table,key,"int");
                c.close();
                db.close();
                return -1;
            }else {
                int ret = c.getInt(c.getColumnIndex(key));
                c.close();
                db.close();
                return ret;
            }
        }
        c.close();
        db.close();
        return -1;
    }

    /**
     * Извлекает boolean из базы данных
     * @param table Таблица
     * @param key Ключ
     * @return Либо значение, либо false
     */
    public static boolean getBoolean(String table,String key){
        DBHelper dbHelper = new DBHelper(activity);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(table, null, null, null, null, null, null);
        if (c.moveToFirst()){
            if (c.getColumnIndex(key) < 0){
                addKey(table,key,"boolean");
                c.close();
                db.close();
                return false;
            }else{
                boolean ret = Boolean.valueOf(c.getString(c.getColumnIndex(key)));
                c.close();
                db.close();
                return ret;
            }

        }
        c.close();
        db.close();
        return false;
    }

    /**
     * Помещает значение boolean в базу данных, оборачивая в String
     * @param table Таблица
     * @param key Ключ
     * @param value Значение
     */
    public static void setBooolean(String table,String key,boolean value){
        setString(table,key,String.valueOf(value));
    }

    /**
     * Помещает значение int в базу данных.
     * @param table Таблица
     * @param key Ключ
     * @param value Значение
     * @return True если успешно записано, иначе false
     */
    public static boolean setInteger(final String table, final String key, final int value){
        if (activity != null) {
            DBHelper dbHelper = new DBHelper(activity);
            final SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(key, value);
            final Cursor c = db.query(table, null, null, null, null, null, null);
            if (c.moveToFirst()) {//
                if (c.getColumnIndex(key) > -1) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            db.execSQL("update " + table + " set " + key + " = " + value + ";");
                            c.close();
                            db.close();

                        }
                    });
                    return true;
                } else {
                    c.close();
                    addKey(table, key, value);
                    return true;
                }
            } else {
                c.close();
                addValue(db, table, key, value);
            }
        }
        return false;
    }

    /**
     * Присвоение значения ячейки
     * @param database db
     * @param table Таблица
     * @param key Ключ
     * @param value Значение
     */
    private static void addValue(final SQLiteDatabase database, final String table, final String key, final String value){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                database.execSQL("insert into " + table +" ("+key + ") values (\"" + value + "\");");
                database.close();
            }
        });
    }

    /**
     * Присвоение значения ячейки
     * @param database db
     * @param table Таблица
     * @param key Ключ
     * @param value Значение
     */
    private static void addValue(final SQLiteDatabase database, final String table, final String key, final int value){
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    database.execSQL("insert into " + table + " (" + key + ") values (" + value + ");");
                    database.close();
                }
            });
        }
    }

    /**
     * Присвоение значения ячейки
     * @param database db
     * @param table Таблица
     * @param key Ключ
     * @param value Значение
     */
    private static void addValue(final SQLiteDatabase database, final String table, final String key, final float value){
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    database.execSQL("insert into " + table + " (" + key + ") values (" + value + ");");
                    database.close();
                }
            });
        }
    }

    /**
     * Создание ключа
     * @param table Таблица
     * @param key Ключ
     * @param clas Тип ключа
     * @return True если создан новый ключ, иначе false
     */
    public static boolean addKey(final String table, final String key, final String clas){
        if (activity != null) {
            DBHelper dbHelper = new DBHelper(activity);
            final SQLiteDatabase db = dbHelper.getWritableDatabase();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        db.execSQL("alter table " + table + " add " + key + " " + clas + ";");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    db.close();
                }
            });
        }
        return true;
    }

    public static boolean addKey(final String table, final String key, final int value){
        if (activity != null) {
            DBHelper dbHelper = new DBHelper(activity);
            final SQLiteDatabase db = dbHelper.getWritableDatabase();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        db.execSQL("alter table " + table + " add " + key + " " + "int" + ";");
                        db.execSQL("insert into " + table + " (" + key + ") values (" + value + ");");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    db.close();
                }
            });
        }
        return true;
    }

}
