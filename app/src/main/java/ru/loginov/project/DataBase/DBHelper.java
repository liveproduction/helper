package ru.loginov.project.DataBase;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ru.loginov.project.preference.PreferenceValue;

/**
 * Created by Loginov Ilya on 21.09.2017.
 * Класс для подключения и работы с базой данных
 */

public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(Activity context) {
        super(context, "LIVe", null, 1);
    }

    //Создание базы данных
    @Override
    public void onCreate(SQLiteDatabase db) {
        // создаем таблицу с полями
        db.execSQL("create table IF NOT EXISTS "+ PreferenceValue.dbt+" (id integer,colormenuadapter text,backcolormenuadapter text,colormenutext text,colormenudivide text,hmenudivide integer,editdialogbuttomcolor text,backdrawfotomenu text, backrawfotomenuboolean text, heightmenuadapter int,orientationone text,orientationtwo text,orientationthree text,orientationfore text,heighttextadapter int,orientationfive text);");
        db.execSQL("delete from "+ PreferenceValue.dbt+" where id > 1");
    }

    //Обновление базы данных
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }

}
