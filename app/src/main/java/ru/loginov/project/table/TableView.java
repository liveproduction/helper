package ru.loginov.project.table;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import ru.loginov.project.S;
import ru.loginov.project.otherclass.Sort;
import ru.loginov.project.preference.PreferenceValue;
import ru.loginov.project.workspace.InitView;
import ru.loginov.project.workspace.MainView;

/**
 * Created by user on 24.10.2016.
 */
public final class TableView{
    RelativeLayout relativeLayout = null;
    public static boolean visible = false;
    String name = null;
    String value = null;
    List<TableItem> items = new ArrayList<>();
    MainView.OnInitMenu onmenu = null;

    public TableView(String name,String value,TableItem[] tableItems){
        this.name = name;
        this.value = value;
        this.items = Arrays.asList(tableItems);

    }

    public TableView(String name, String value,TableItem[] tableItems, MainView.OnInitMenu menu){
            this.name = name;
        this.value = value;
        this.items = Arrays.asList(tableItems);
        this.onmenu = menu;
    }

    public void setOnmenu(MainView.OnInitMenu menu){
        this.onmenu = menu;
    }

    public void addItem(TableItem item){
        items.add(item);
    }

    public void removeItem(TableItem item){
        items.remove(item);
    }

    public void initTable(Activity activity){
        relativeLayout = new RelativeLayout(activity);
    }

    public void visibleTable(final Activity activity, final MenuItem item){
        MainView mainView = new MainView(visibleTableNow(activity),null,null,onmenu,null);
        mainView.setBackInit(new MainView.onBackInit() {
            @Override
            public void onInit(Activity activity) {
                reloadTable(activity,item);
            }
        });
        InitView.initView(activity,mainView,item);
    }

    private RelativeLayout visibleTableNow(Activity activity){
        if (relativeLayout == null) initTable(activity);
        TextView tv = new TextView(activity);
        TextView tv2 = new TextView(activity);
        ListView lv = new ListView(activity);
        lv.setAdapter(new TableAdapter(activity,items));
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        tv.setLayoutParams(lp);
        tv.setText(name);
        tv.setId(5555);
        tv.setTextColor(Color.parseColor(PreferenceValue.tablecolortextname));
        tv.setTextSize(PreferenceValue.tableheighttext);
        RelativeLayout.LayoutParams lp2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp2.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        lp2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        tv2.setLayoutParams(lp2);
        tv2.setText(value);
        tv2.setTextColor(Color.parseColor(PreferenceValue.tablecolortextvalue));
        tv2.setTextSize(PreferenceValue.tableheighttext);
        lv.setDivider(S.getDrawable(PreferenceValue.tablecolordivide, GradientDrawable.Orientation.BL_TR));
        lv.setDividerHeight(1);
        RelativeLayout.LayoutParams lp3 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp3.addRule(RelativeLayout.BELOW,5555);
        lv.setLayoutParams(lp3);
        relativeLayout.addView(tv);
        relativeLayout.addView(tv2);
        relativeLayout.addView(lv);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            relativeLayout.setBackground(S.getDrawable(PreferenceValue.tablebackcolorall, PreferenceValue.tablegradleback));
        }else relativeLayout.setBackgroundDrawable(S.getDrawable(PreferenceValue.tablebackcolorall, PreferenceValue.tablegradleback));
        //InitView.setBackground(S.getDrawable(PreferenceValue.tablebackcolorall, GradientDrawable.Orientation.TL_BR));
        visible = true;
        return relativeLayout;
    }

    public void reloadTable(Activity activity, MenuItem item){
        if (visible) {
            relativeLayout = null;
            InitView.replaceNow(activity,new MainView(visibleTableNow(activity),null,null,onmenu,null));
        }else{
            visibleTable(activity,item);
        }
    }

    public TableView sortNameDown(){
        TableItem[] items = this.items.toArray(new TableItem[this.items.size()]);
        Arrays.sort(items, new Comparator<TableItem>() {
            @Override
            public int compare(TableItem item, TableItem t1) {
                return item.getName().compareTo(t1.getName());
            }
        });
        this.items = Arrays.asList(items);
        return this;
    }

    public TableView sortNameUp(){
        TableItem[] items = this.items.toArray(new TableItem[this.items.size()]);
        Arrays.sort(items, new Comparator<TableItem>() {
            @Override
            public int compare(TableItem item, TableItem t1) {
                return - (item.getName().compareTo(t1.getName()));
            }
        });
        this.items = Arrays.asList(items);
        return this;
    }

    public TableView sortValueDown(){
        TableItem[] items = this.items.toArray(new TableItem[this.items.size()]);
        Arrays.sort(items, new Comparator<TableItem>() {
            @Override
            public int compare(TableItem item, TableItem t1) {
                return Sort.sort(item.getValue(),t1.getValue());
            }
        });
        this.items = Arrays.asList(items);
        return this;
    }

    public TableView sortValueUp(){
        TableItem[] items = this.items.toArray(new TableItem[this.items.size()]);
        Arrays.sort(items, new Comparator<TableItem>() {
            @Override
            public int compare(TableItem item, TableItem t1) {
                return -Sort.sort(item.getValue(),t1.getValue());
            }
        });
        this.items = Arrays.asList(items);
        return this;
    }

    public TableView search(String str){
        this.items = S.prosearch(str,this.items);
        return this;
    }

    public TableView setItems(List<TableItem> items){
        this.items = items;
        return this;
    }

    public TableView setItems(TableItem[] items){
        this.items = Arrays.asList(items);
        return this;
    }

}
