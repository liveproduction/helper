package ru.loginov.project.table;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.loginov.project.S;
import ru.loginov.project.preference.PreferenceValue;

/**
 * Created by user on 25.10.2016.
 */
public final class TableAdapter extends BaseAdapter {
    List<TableItem> list = new ArrayList<>();
    Context context = null;
    public TableAdapter(@NonNull Context context,@NonNull List<TableItem> list){
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        RelativeLayout rl = new RelativeLayout(context);
        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.WRAP_CONTENT);
        rl.setLayoutParams(lp);
        TextView tv = new TextView(context);
        TextView tv2 = new TextView(context);
        tv.setText(list.get(i).getName());
        tv2.setText(list.get(i).getValue());

        tv.setLayoutParams(getParams(RelativeLayout.ALIGN_PARENT_LEFT,RelativeLayout.ALIGN_PARENT_TOP));
        tv2.setLayoutParams(getParams(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.ALIGN_PARENT_TOP));

        tv.setTextColor(Color.parseColor(PreferenceValue.tablecolortextname));
        tv2.setTextColor(Color.parseColor(PreferenceValue.tablecolortextvalue));

        tv.setTextSize(PreferenceValue.tableheighttext);
        tv2.setTextSize(PreferenceValue.tableheighttext);

        rl.addView(tv);
        rl.addView(tv2);
        rl.setBackgroundDrawable(S.getDrawable(PreferenceValue.tablebackcoloritems,PreferenceValue.tablegradleitemback));
        return rl;
    }

    public RelativeLayout.LayoutParams getParams(int... rules){
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        for (int i = 0;i<rules.length;i++){
            params.addRule(rules[i]);
        }
        return params;
    }
}
