package ru.loginov.project.table;

/**
 * Created by user on 24.10.2016.
 */
public final class TableItem {
    private String name = null;
    private String value = null;
    public TableItem(String name,String value){
        this.name = name;
        this.value = value;
    }

    public String getName(){
        return name;
    }

    public String getValue(){
        return value;
    }
}
